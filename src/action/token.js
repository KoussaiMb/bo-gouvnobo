import { TOKEN } from './actionList';
import { API_NOBO_URL } from "../utils/globalVariable";

export function tokenError(err) {
  return {
    type: TOKEN.ERROR,
    error: err
  };
}

export function tokenReset() {
  return {
    type: TOKEN.RESET,
    tokenData: null
  };
}

export function tokenLoading(bool) {
  return {
    type: TOKEN.LOADING,
    isLoading: bool
  };
}

export function tokenSuccess(tokenData) {
  return {
    type: TOKEN.SUCCESS,
    tokenData
  }
}

export function checkToken(token) {
  return (dispatch) => {
    dispatch(tokenLoading(true));
    fetch(API_NOBO_URL + "/auth/isValid/apiToken", {
      method: 'get',
      headers: new Headers({
        'content-type': 'application/json',
        'apiToken': token
      })
    }).then(response => {
      dispatch(tokenLoading(false));
      let status = response.status;
      return response.json().then((json) => {
        if (status !== 200)
          throw json;
        return (json);
      })
    }).then(tokenData => dispatch(tokenSuccess(tokenData))
    ).catch(err => {
      dispatch(tokenError(err.message));
    });
  }
}
