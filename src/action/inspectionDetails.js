import {INSPDETAILS} from './actionList';
import {API_NOBO_URL} from  '../utils/globalVariable';

export function getInspDetailsError(err) {
  return {
    type:INSPDETAILS.ERROR,
    error: err
  };
}

export function getInspDetailsLoading(bool) {
  return {
    type: INSPDETAILS.LOADING,
    isLoading: bool
  };
}

export function getInspDetailsSuccess(inspDetailsData) {
  return {
    type: INSPDETAILS.SUCCESS,
    inspDetailsData
  };
}

export function getInspDetails(apiToken, inspId) {
  return (dispatch) => {
    dispatch(getInspDetailsError(""));
    dispatch(getInspDetailsLoading(true));
    fetch(API_NOBO_URL + '/governess/inspection/' + inspId,
        {
          method: 'get',
          headers: new Headers({
            'content-type': 'application/json',
            'apiToken': apiToken
          })
        }).then((response) => {
      dispatch(getInspDetailsLoading(false));
      let status = response.status;
      return response.json().then((json) => {
        if (status !== 200)
          throw json.message;
        return (json.data);
      })
    }).then((inspDetailsData) => {
      dispatch(getInspDetailsSuccess(inspDetailsData));
    }).catch(err => {
      dispatch(getInspDetailsError(err));
    });
  };
}