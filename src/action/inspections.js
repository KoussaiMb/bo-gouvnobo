import {INSPECTIONS} from './actionList';
import {API_NOBO_URL} from  '../utils/globalVariable';

export function getInspectionsError(err) {
  return {
    type:INSPECTIONS.ERROR,
    error: err
  };
}

export function getInspectionsLoading(bool) {
  return {
    type: INSPECTIONS.LOADING,
    isLoading: bool
  };
}

export function getInspectionsSuccess(inspectionsData) {
  return {
    type: INSPECTIONS.SUCCESS,
    inspectionsData
  };
}

export function getInspections(apiToken, page) {
  return (dispatch, getState) => {
    dispatch(getInspectionsLoading(true));
    let curState = getState();
    fetch(API_NOBO_URL + '/governess/inspection/page/' + page,
        {
          method: 'get',
          headers: new Headers({
            'content-type': 'application/json',
            'apiToken': apiToken
          })
        }).then((response) => {
      dispatch(getInspectionsLoading(false));
      let status = response.status;
      return response.json().then((json) => {
        if (status !== 200)
          throw json.message;
        return (json);
      })
    }).then((inspectionsData) => {
      if (page <= 1)
        dispatch(getInspectionsSuccess(inspectionsData));
      else {
        let curData = curState.inspectionsReducer.inspectionsData.data;
        inspectionsData.data = curData.concat(inspectionsData.data);
        dispatch(getInspectionsSuccess(inspectionsData));
      }
    }).catch(err => {
      dispatch(getInspectionsError(err));
    });
  };
}

function appendPercent(field) {
  return "%".concat(field.concat("%"));
}

export function searchInspections(apiToken, clientName, providerName, date) {
  return (dispatch) => {
    let headers = new Headers({'content-type': 'application/json',
      'apiToken': apiToken});
    if (clientName)
      headers.set('clientName', appendPercent(clientName));
    if (providerName)
      headers.set('providerName', appendPercent(providerName));
    if (date)
      headers.set('inspectionDate', date);
    dispatch(getInspectionsLoading(true));
    fetch(API_NOBO_URL + '/governess/inspection/search',
        {
          method: 'get',
          headers
        }).then((response) => {
      dispatch(getInspectionsLoading(false));
      let status = response.status;
      return response.json().then((json) => {
        if (status !== 200)
          throw json.message;
        return (json);
      })
    }).then((inspectionsData) => {
      dispatch(getInspectionsSuccess(inspectionsData));
    }).catch(err => {
      dispatch(getInspectionsError(err));
    });
  };
}