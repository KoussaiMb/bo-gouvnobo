import {MPCARDEX} from './actionList';
import {API_NOBO_URL} from  '../utils/globalVariable';

export function ResetgetMpCardex() {
    return dispatch => dispatch(getMpCardexResetv2())
}

export function getMpCardexErrorv2(error) {
  return {
    type:MPCARDEX.ERROR,
    error
  };
}

 function getMpCardexLoadingv2(isLoading) {
  return {
    type: MPCARDEX.LOADING,
    isLoading
  };
}

export function getMpCardexSuccessv2(maintenanceData) {
  return {
    type: MPCARDEX.SUCCESS,
    maintenanceData
  };
}
export function getMpCardexResetv2() {
  return {
    type: MPCARDEX.RESET
  };
}

export function getMpCardex(apiToken, id) {
  return (dispatch) => {
    dispatch(getMpCardexLoadingv2(true));
    fetch(API_NOBO_URL + '/governess/address/mp_cardex/' + id,
      {
        method: 'get',
        headers: new Headers({
          'content-type': 'application/json',
          'apiToken': apiToken
        })
      }).then((response) => {
      dispatch(getMpCardexLoadingv2(false));
      let status = response.status;
      return response.json().then((json) => {
        if (status !== 200)
          throw json.message;
        return (json);
      })
    })
      .then((maintenanceData) => {
        const {data} = maintenanceData
        dispatch(getMpCardexSuccessv2(data));
      })
      .catch(err => {
        dispatch(getMpCardexErrorv2(err));
      });
  };
}