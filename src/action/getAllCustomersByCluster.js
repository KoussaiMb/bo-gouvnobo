import {CUSTOMERBYCLUSTER} from './actionList';
import {API_NOBO_URL} from  '../utils/globalVariable';

export function getAllCustomerByClusterError(error) {
    return {
        type:CUSTOMERBYCLUSTER.ERROR,
        error
    };
}

export function getAllCustomerByClusterLoading(isLoading) {
    return {
        type: CUSTOMERBYCLUSTER.LOADING,
        isLoading
    };
}

export function getAllCustomerByClusterSuccess(list) {
    return {
        type: CUSTOMERBYCLUSTER.SUCCESS,
        list
    };
}

export function getAllCustomersByCluster(apiToken) {
    return (dispatch) => {
        dispatch(getAllCustomerByClusterLoading(true));
        fetch(API_NOBO_URL + '/governess/customer/customersAddresses',
            {
                method: 'get',
                headers: new Headers({
                    'content-type': 'application/json',
                    'apiToken': apiToken
                })
            }).then((response) => {
            dispatch(getAllCustomerByClusterLoading(false));
            let status = response.status;
            return response.json().then((json) => {
                if (status !== 200)
                    throw json.message;
                return (json);
            })
        })
            .then( listCustomers => {
                const {data} =  listCustomers
                dispatch(getAllCustomerByClusterSuccess(data));
            })
            .catch(err => {
                dispatch(getAllCustomerByClusterError(err));
            });
    };
}