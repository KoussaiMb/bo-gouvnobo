import {AGENDAENTRY} from './actionList';
import {API_NOBO_URL} from  '../utils/globalVariable';

export function getAgendaEntryError(err) {
  return {
    type:AGENDAENTRY.ERROR,
    error: err
  };
}

export function getAgendaEntryLoading(bool) {
  return {
    type: AGENDAENTRY.LOADING,
    isLoading: bool
  };
}

export function getAgendaEntrySuccess(agendaEntryData) {
  return {
    type: AGENDAENTRY.SUCCESS,
    agendaEntryData
  };
}

export function getAgendaEntry(apiToken, id) {
  return (dispatch) => {
    dispatch(getAgendaEntryLoading(true));
    fetch(API_NOBO_URL + '/governess/agenda/' + id,
      {
        method: 'get',
        headers: new Headers({
          'content-type': 'application/json',
          'apiToken': apiToken
        })
      }).then((response) => {
      dispatch(getAgendaEntryLoading(false));
      let status = response.status;
      return response.json().then((json) => {
        if (status !== 200)
          throw json.message;
        return (json);
      })
    })
      .then((missionsData) => {
        dispatch(getAgendaEntrySuccess(missionsData));
      })
      .catch(err => {
        dispatch(getAgendaEntryError(err));
      });
  };
}