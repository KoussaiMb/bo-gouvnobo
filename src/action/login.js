import { LOGIN } from './actionList';
import { API_NOBO_URL } from "../utils/globalVariable";

export function loginError(err) {
  return {
    type: LOGIN.ERROR,
    error: err
  };
}

export function loginReset() {
  return {
    type: LOGIN.RESET,
    loginData: null
  };
}

export function loginLoading(bool) {
  return {
    type: LOGIN.LOADING,
    isLoading: bool
  };
}

export function loginSuccess(loginData) {
  return {
    type: LOGIN.SUCCESS,
    loginData
  }
}

export function authenticate(mail, password) {
  return (dispatch) => {
    dispatch(loginLoading(true));
    fetch(API_NOBO_URL + "/auth/authenticate", {
      method: 'post',
      headers: new Headers({
        'content-type': 'application/json'
      }),
      body: JSON.stringify({
        email: mail,
        password: password
      })
    }).then(response => {
      dispatch(loginLoading(false));
      let status = response.status;
      return response.json().then((json) => {
        if (status !== 200)
          throw json;
        return (json);
      })
    }).then(loginData => dispatch(loginSuccess(loginData))
    ).catch(err => {
      dispatch(loginError(err.message));
    });
  }
}