import {TODAYMISSIONS} from './actionList';
import {API_NOBO_URL} from  '../utils/globalVariable';

export function getTodayMissionsError(err) {
  return {
    type:TODAYMISSIONS.ERROR,
    error: err
  };
}

export function getTodayMissionsLoading(bool) {
  return {
    type: TODAYMISSIONS.LOADING,
    isLoading: bool
  };
}

export function getTodayMissionsSuccess(todayMissionsData) {
  return {
    type: TODAYMISSIONS.SUCCESS,
    todayMissionsData
  };
}

export function getTodayMissions(apiToken) {
  return (dispatch) => {
    dispatch(getTodayMissionsLoading(true));
    fetch(API_NOBO_URL + '/governess/agenda/today',
      {
        method: 'get',
        headers: new Headers({
          'content-type': 'application/json',
          'apiToken': apiToken,
          'day': new Date().getDay()
        })
      }).then((response) => {
      dispatch(getTodayMissionsLoading(false));
      let status = response.status;
      return response.json().then((json) => {
        if (status !== 200)
          throw json.message;
        return (json);
      })
    })
      .then((missionsData) => {
        dispatch(getTodayMissionsSuccess(missionsData));
      })
      .catch(err => {
        dispatch(getTodayMissionsError(err));
      });
  };
}