import {LocalizationLockboxTrash} from './actionList';
import {API_NOBO_URL} from  '../utils/globalVariable';

export function getLocalizationLockboxTrashError(err) {
    return {
        type:LocalizationLockboxTrash.ERROR,
        error: err
    };
}

export function getLocalizationLockboxTrashLoading(bool) {
    return {
        type: LocalizationLockboxTrash.LOADING,
        isLoading: bool
    };
}

export function getLocalizationLockboxTrashSuccess(LocalizationLockboxTrashData) {
    return {
        type: LocalizationLockboxTrash.SUCCESS,
        LocalizationLockboxTrashData
    };
}

export function getLocalizationLockboxTrash(apiToken, id) {
    return (dispatch) => {
        dispatch(getLocalizationLockboxTrashLoading(true));
        fetch(API_NOBO_URL + '/governess/address/localizationLockboxTrash/' + id,
            {
                method: 'get',
                headers: new Headers({
                    'content-type': 'application/json',
                    'apiToken': apiToken
                })
            }).then((response) => {
            dispatch(getLocalizationLockboxTrashLoading(false));
            let status = response.status;
            return response.json().then((json) => {
                if (status !== 200)
                    throw json.message;
                return (json);
            })
        })
            .then((LocalizationLockboxTrashData) => {
                const {data} = LocalizationLockboxTrashData
                dispatch(getLocalizationLockboxTrashSuccess(data));
            })
            .catch(err => {
                dispatch(getLocalizationLockboxTrashError(err));
            });
    };
}