import {ALLCUSTOMERS} from './actionList';
import {API_NOBO_URL} from  '../utils/globalVariable';

export function getAllCustomersError(error) {
  return {
    type:ALLCUSTOMERS.ERROR,
    error
  };
}

export function getAllCustomersLoading(isLoading) {
  return {
    type: ALLCUSTOMERS.LOADING,
    isLoading
  };
}

export function getAllCustomersSuccess(list) {
  return {
    type: ALLCUSTOMERS.SUCCESS,
    list
  };
}

export function getAllCustomers(apiToken) {
  return (dispatch) => {
    dispatch(getAllCustomersLoading(true));
    fetch(API_NOBO_URL + '/governess/customer/customers',
      {
        method: 'get',
        headers: new Headers({
          'content-type': 'application/json',
          'apiToken': apiToken
        })
      }).then((response) => {
      dispatch(getAllCustomersLoading(false));
      let status = response.status;
      return response.json().then((json) => {
        if (status !== 200)
          throw json.message;
        return (json);
      })
    })
      .then( listCustomers => {
        const {data} =  listCustomers
        dispatch(getAllCustomersSuccess(data));
      })
      .catch(err => {
        dispatch(getAllCustomersError(err));
      });
  };
}