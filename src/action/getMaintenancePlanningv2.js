import {MAINTENANCEPLANNINGV2} from './actionList';
import {API_NOBO_URL} from  '../utils/globalVariable';

export function ResetgetMaintenancePlanningv2() {
    return dispatch => dispatch(getMaintenancePlanningResetv2())
}

export function getMaintenancePlanningErrorv2(error) {
  return {
    type:MAINTENANCEPLANNINGV2.ERROR,
    error
  };
}

 function getMaintenancePlanningLoadingv2(isLoading) {
  return {
    type: MAINTENANCEPLANNINGV2.LOADING,
    isLoading
  };
}

export function getMaintenancePlanningSuccessv2(maintenanceData) {
  return {
    type: MAINTENANCEPLANNINGV2.SUCCESS,
    maintenanceData
  };
}
export function getMaintenancePlanningResetv2() {
  return {
    type: MAINTENANCEPLANNINGV2.RESET
  };
}

export function getMaintenancePlanningv2(apiToken, id) {
  return (dispatch) => {
    dispatch(getMaintenancePlanningLoadingv2(true));
    fetch(API_NOBO_URL + '/governess/address/planningv2/' + id,
      {
        method: 'get',
        headers: new Headers({
          'content-type': 'application/json',
          'apiToken': apiToken
        })
      }).then((response) => {
      dispatch(getMaintenancePlanningLoadingv2(false));
      let status = response.status;
      return response.json().then((json) => {
        if (status !== 200)
          throw json.message;
        return (json);
      })
    })
      .then((maintenanceData) => {
        const {data} = maintenanceData
        dispatch(getMaintenancePlanningSuccessv2(data));
      })
      .catch(err => {
        dispatch(getMaintenancePlanningErrorv2(err));
      });
  };
}