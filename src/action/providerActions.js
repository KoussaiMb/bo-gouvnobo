import {SELECTEDPROVIDER,WEEKMISSIONS,PROVIDERLIST,TODAYMISSIONSPROVIDER, MONTHMISSIONSPROVIDER, SELECTEDEVENT, SIDESTATUS, SELECTEDDATE, EVENTSBYTYPE, DELETEEVENTMODAL, DELETEEVENT, INFOEVENTMODAL, UPDATEEVENT, USERSLIST, CREATEEVENT, UPDATEEVENTUSER} from './actionList'
import {API_NOBO_URL} from  '../utils/globalVariable'
//TODO: split into several files && delete those that are not used

//getProviderList actions
const getProviderListError = error => {
    return {
        type: PROVIDERLIST.ERROR,
        error
    };
}

const getProviderListReset = () => {
    return {
        type: PROVIDERLIST.RESET,
        providerList: null
    }
}

const getProviderListLoading = bool => {
    return {
        type: PROVIDERLIST.LOADING,
        Loading: bool
    };
}

const getProviderListSuccess = providerList => {
    return {
        type: PROVIDERLIST.SUCCESS,
        providerList
    };
}

export const getProviderList = apiToken => {
    return dispatch => {
        dispatch(getProviderListLoading(true))
        fetch(API_NOBO_URL + '/governess/provider/',
            {
                method: 'get',
                headers: new Headers({
                    'content-type': 'application/json',
                    'apiToken': apiToken
                })
            })
            .then( response => {
                dispatch(getProviderListLoading(false))
                let status = response.status
                return response.json()
                    .then( json => {
                        if (status !== 200)
                            throw json.message[0]
                        return json
                    })
            })
            .then( providerList => {
                dispatch(getProviderListSuccess(providerList))
            })
            .catch( error => {
                dispatch(getProviderListReset())
                dispatch(getProviderListError(error))
            })
    }
}

//getSelectedProvider actions
const getSelectedProviderLoading = bool => {
    return {
        type: SELECTEDPROVIDER.LOADING,
        Loading: bool
    }
}

const getSelectedProviderSuccess = provider => {
    return {
        type: SELECTEDPROVIDER.SUCCESS,
        provider
    }
}

export const getSelectedProvider = provider => {
    return dispatch => {
        dispatch(getSelectedProviderLoading(true))
        dispatch(getSelectedProviderLoading(false))
        dispatch(getSelectedProviderSuccess(provider))
    }
}

//get TodayMissionsProvider
const getTodayMissionsProviderError = error => {
    return {
        type:TODAYMISSIONSPROVIDER.ERROR,
        error
    };
}

const getTodayMissionsProviderLoading = Loading => {
    return {
        type: TODAYMISSIONSPROVIDER.LOADING,
        Loading
    };
}

const getTodayMissionsProviderSuccess = missions => {
    return {
        type: TODAYMISSIONSPROVIDER.SUCCESS,
        missions
    };
}

export function getTodayMissionsProvider(apiToken,providerId) {
    return (dispatch) => {
        dispatch(getTodayMissionsProviderLoading(true));
        fetch(API_NOBO_URL + `/governess/agenda/today/${providerId}`,
            {
                method: 'get',
                headers: new Headers({
                    'content-type': 'application/json',
                    'apiToken': apiToken
                })
            }).then((response) => {
                dispatch(getTodayMissionsProviderLoading(false));
                let status = response.status;
                return response.json().then((json) => {
                    if (status !== 200)
                        throw json.message;
                    return (json);
                })
            })
            .then((missionsData) => {
                const {data} = missionsData
                dispatch(getTodayMissionsProviderSuccess(data));
            })
            .catch(err => {
                dispatch(getTodayMissionsProviderError(err));
            });
    };
}

//getWeekMissions actions
const getWeekMissionsError = err => {
    return {
        type:WEEKMISSIONS.ERROR,
        error: err
    }
}

const getWeekMissionsLoading = bool => {
    return {
        type: WEEKMISSIONS.LOADING,
        Loading: bool
    }
}

const getWeekMissionsSuccess = weekMissions => {
    return {
        type: WEEKMISSIONS.SUCCESS,
        weekMissions
    }
}

export const getWeekMissions = (apiToken, provider_id) => {
    return (dispatch) => {
        dispatch(getWeekMissionsLoading(true))
        fetch(API_NOBO_URL + `/governess/agenda/week/current/${provider_id}`,
            {
                method: 'get',
                headers: new Headers({
                    'content-type': 'application/json',
                    'apiToken': apiToken
                })
            }).then( response => {
                dispatch(getWeekMissionsLoading(false))
                let status = response.status
                return response.json().then((json) => {
                    if (status !== 200)
                        throw json.message
                    return (json)
                })
            })
            .then( missions => {
                dispatch(getWeekMissionsSuccess(missions))
            })
            .catch(err => {
                dispatch(getWeekMissionsError(err))
            })
    }
}

//getMonthMissionsProvider
const getMonthMissionsProviderError = error => {
    return {
        type:MONTHMISSIONSPROVIDER.ERROR,
        error
    };
}

const getMonthMissionsProviderLoading = Loading => {
    return {
        type: MONTHMISSIONSPROVIDER.LOADING,
        Loading
    };
}

const getMonthMissionsProviderSuccess = missions => {
    return {
        type: MONTHMISSIONSPROVIDER.SUCCESS,
        missions
    };
}

export function getMonthMissionsProvider(apiToken,month) {
    return (dispatch) => {
        dispatch(getMonthMissionsProviderLoading(true));
        fetch(API_NOBO_URL + `/governess/agenda/month`,
            {
                method: 'get',
                headers: new Headers({
                    'content-type': 'application/json',
                    'apiToken': apiToken,
                    'month' : month
                })
            }).then((response) => {
                dispatch(getMonthMissionsProviderLoading(false));
                let status = response.status;
                return response.json().then((json) => {
                    if (status !== 200)
                        throw json.message;
                    return (json);
                })
            })
            .then((missionsData) => {
                const {data} = missionsData
                dispatch(getMonthMissionsProviderSuccess(data));
            })
            .catch(err => {
                dispatch(getMonthMissionsProviderError(err));
            });
    };
}

//getSideStatus actions
const getSideStatusSuccess = open => {
    return {
        type: SIDESTATUS.SUCCESS,
        open
    }
}

export const getSideStatus = open => {
    return dispatch => {
        dispatch(getSideStatusSuccess(open))
    }
}

//getSelectedDate actions
const getSelectedDateSuccess = (date) => {
    return {
        type: SELECTEDDATE.SUCCESS,
        date
    }
}

export function getSelectedDate(date){
    return dispatch => {
        dispatch(getSelectedDateSuccess(date))
    }
}


//getEventsByType actions
const getEventsByTypeError = error => {
    return {
        type:EVENTSBYTYPE.ERROR,
        error
    };
}

const getEventsByTypeLoading = Loading => {
    return {
        type: EVENTSBYTYPE.LOADING,
        Loading
    };
}

const getEventsByTypeSuccess = events => {
    return {
        type: EVENTSBYTYPE.SUCCESS,
        events
    };
}


const getEventsByTypeReset = events => {
    return {
        type: EVENTSBYTYPE.RESET,
        events
    }
}

export function getEventsByType(apiToken, startDate, endDate, types, users) {
    return (dispatch) => {
        dispatch(getEventsByTypeLoading(true))
        dispatch(getEventsByTypeReset(null));
        fetch(API_NOBO_URL + `/governess/event?${types}&${users}`,
            {
                method: 'get',
                headers: new Headers({
                    'content-type'  :   'application/json',
                    'apiToken'      :   apiToken,
                    'startDate'     :   startDate,
                    'endDate'       :   endDate
                })
            }).then((response) => {
                dispatch(getEventsByTypeLoading(false));
                let status = response.status;
                return response.json().then( json => {
                    if (status !== 200)
                        throw json.message;
                    return (json);
                })
            })
            .then( events => {
                let data = events.data
                if(data === undefined)
                    data = []
                dispatch(getEventsByTypeSuccess(data));
            })
            .catch(error => {
                dispatch(getEventsByTypeError(error));
            });
    };
}

/*
export function getEventsByType(apiToken, startDate, endDate, types) {
    return (dispatch) => {
        dispatch(getEventsByTypeLoading(true))
        dispatch(getEventsByTypeReset(null));
        fetch(API_NOBO_URL + `/governess/event?${types}`,
            {
                method: 'get',
                headers: new Headers({
                    'content-type'  :   'application/json',
                    'apiToken'      :   apiToken,
                    'startDate'     :   startDate,
                    'endDate'       :   endDate
                })
            }).then((response) => {
                dispatch(getEventsByTypeLoading(false));
                let status = response.status;
                return response.json().then( json => {
                    if (status !== 200)
                        throw json.message;
                    return (json);
                })
            })
            .then( events => {
                let data = events.data
                if(data === undefined)
                    data = []
                dispatch(getEventsByTypeSuccess(data));
            })
            .catch(error => {
                dispatch(getEventsByTypeError(error));
            });
    };
}*/

//getSelectedEvent actions
const getSelectedEventSuccess = event => {
    return {
        type: SELECTEDEVENT.SUCCESS,
        event
    }
}

export const getSelectedEvent = event => {
    return dispatch => dispatch(getSelectedEventSuccess(event))

}

//getDeleteEventModal actions
const getDeleteEventModalSuccess = open => {
    return {
        type: DELETEEVENTMODAL.SUCCESS,
        open
    }
}

export const getDeleteEventModal = open => {
    return dispatch => dispatch(getDeleteEventModalSuccess(open))

}

//getInfoEventModal actions
const getInfoEventModalSuccess = open => {
    return {
        type: INFOEVENTMODAL.SUCCESS,
        open
    }
}

export const getInfoEventModal = open => {
    return dispatch => dispatch(getInfoEventModalSuccess(open))

}


//getResetDeleteEventStatus actions
export const getResetDeleteEventStatus = () => {
    return dispatch => dispatch(getDeleteEventReset())

}

//getDeleteEvent actions
const getDeleteEventReset = () => {
    return {
        type: DELETEEVENT.RESET
    }
}

const getDeleteEventError = error => {
    return {
        type:DELETEEVENT.ERROR,
        error
    };
}

const getDeleteEventLoading = Loading => {
    return {
        type: DELETEEVENT.LOADING,
        Loading
    };
}

const getDeleteEventSuccess = message => {
    return {
        type: DELETEEVENT.SUCCESS,
        message
    };
}

export function getDeleteEvent(apiToken, oldSubEvent, op, id) {
    return (dispatch) => {
        dispatch(getDeleteEventLoading(true))
        fetch(API_NOBO_URL + `/governess/event/${id}`,
            {
                method: 'delete',
                headers: new Headers({
                    'content-type'  :   'application/json',
                    'apiToken'      :   apiToken
                }),
                body : JSON.stringify({
                    "op": op,
                    "oldSubEvent": oldSubEvent
                })
            }).then( response => {
                dispatch(getDeleteEventLoading(false));
                let status = response.status;
                return response.json().then( json => {
                    if (status !== 200)
                        throw json.message;
                    return (json);
                })
            })
            .then( response => {
                const {message} = response
                dispatch(getDeleteEventSuccess(message));
            })
            .catch(error => {
                dispatch(getDeleteEventError(error));
            });
    };
}


//getResetUpdateEventStatus
export const getResetUpdateEventStatus = () => {
    return dispatch => dispatch(getUpdateEventReset())

}

//getUpdateEvent actions
const getUpdateEventReset = () => {
    return {
        type: UPDATEEVENT.RESET
    }
}

const getUpdateEventError = error => {
    return {
        type:UPDATEEVENT.ERROR,
        error
    };
}

const getUpdateEventLoading = Loading => {
    return {
        type: UPDATEEVENT.LOADING,
        Loading
    };
}

const getUpdateEventSuccess = message => {
    return {
        type: UPDATEEVENT.SUCCESS,
        message
    };
}

export function getUpdateEvent(apiToken, oldSubEvent,eventToUpdate, op, id) {
    return (dispatch) => {
        dispatch(getUpdateEventLoading(true))
        fetch(API_NOBO_URL + `/governess/event/${id}`,
            {
                method: 'put',
                headers: new Headers({
                    'content-type'  :   'application/json',
                    'apiToken'      :   apiToken
                }),
                body : JSON.stringify({
                    "op": op,
                    "oldSubEvent"   : oldSubEvent,
                    "event"         : eventToUpdate
                })
            }).then( response => {
                dispatch(getUpdateEventLoading(false));
                let status = response.status;
                return response.json().then( json => {
                    if (status !== 200)
                        throw json.message;
                    return (json);
                })
            })
            .then( response => {
                const {message} = response
                dispatch(getUpdateEventSuccess(message));
            })
            .catch(error => {
                dispatch(getUpdateEventError(error));
            });
    };
}



//getUsersList actions
const getUsersListError = error => {
    return {
        type: USERSLIST.ERROR,
        error
    };
}

const getUsersListLoading = bool => {
    return {
        type: USERSLIST.LOADING,
        Loading: bool
    };
}

const getUsersListSuccess = list => {
    return {
        type: USERSLIST.SUCCESS,
        list
    };
}

export const getUsersList = (apiToken, who) => {
    return dispatch => {
        dispatch(getUsersListLoading(true))
        fetch(API_NOBO_URL + `/governess/user?${who}`,
            {
                method: 'get',
                headers: new Headers({
                    'content-type': 'application/json',
                    'apiToken': apiToken
                })
            })
            .then(response => {
                dispatch(getUsersListLoading(false))
                let status = response.status
                return response.json()
                    .then(json => {
                        if (status !== 200)
                            throw json.message[0]
                        return json
                    })
            })
            .then(usersList => {
                let data = usersList.data
                if (data === undefined)
                    data = []
                dispatch(getUsersListSuccess(data))
            })
            .catch(error => {
                dispatch(getUsersListError(error))
            })
    }
}


//getResetCreateEventStatus
export const getResetCreateEventStatus = () => {
    return dispatch => dispatch(createEventReset())

}

const createEventReset = () => {
    return {
        type: CREATEEVENT.RESET
    }
}

const createEventError = error => {
  return {
    type:CREATEEVENT.ERROR,
    error
  };
}

const createEventLoading = Loading => {
  return {
    type: CREATEEVENT.LOADING,
    Loading
  };
}

const createEventSuccess = message => {
  return {
    type: CREATEEVENT.SUCCESS,
    message
  };
}

export function createEvent(apiToken, eventToCreate, users) {
  return (dispatch) => {
    dispatch(createEventLoading(true))
    fetch(API_NOBO_URL + '/governess/event/',
      {
        method: 'post',
        headers: new Headers({
          'content-type'  :   'application/json',
          'apiToken'      :   apiToken
        }),
        body: JSON.stringify({
            "event" : eventToCreate,
            "users" : users
        })
      }).then((response) => {
      dispatch(createEventLoading(false))
      let status = response.status
      return response.json().then( json => {
        if (status !== 200)
          throw json.message;
        return (json);
      })
    })
      .then( response => {
        const {message} = response
        dispatch(createEventSuccess(message))
      })
      .catch(error => {
        dispatch(createEventError(error))
      });
  };
}



//resetUpdateEventUserStatus
export const resetUpdateEventUserStatus = () => {
    return dispatch => dispatch(updateEventUserReset())

}

const updateEventUserReset = () => {
    return {
        type: UPDATEEVENTUSER.RESET
    }
}

const updateEventUserError = error => {
    return {
        type:UPDATEEVENTUSER.ERROR,
        error
    };
}

const updateEventUserLoading = Loading => {
    return {
        type: UPDATEEVENTUSER.LOADING,
        Loading
    };
}

const updateEventUserSuccess = message => {
    return {
        type: UPDATEEVENTUSER.SUCCESS,
        message
    };
}

export function getUpdateEventUser(apiToken, eventId, eventUsers) {
    return (dispatch) => {
        dispatch(updateEventUserLoading(true))
        fetch(API_NOBO_URL + `/governess/event_user/${eventId}`,
            {
                method: 'put',
                headers: new Headers({
                    'content-type'  :   'application/json',
                    'apiToken'      :   apiToken
                }),
                body: JSON.stringify({
                    "eventUsers" : eventUsers
                })
            }).then((response) => {
                dispatch(updateEventUserLoading(false))
                let status = response.status
                return response.json().then( json => {
                    if (status !== 200)
                        throw json.message;
                    return (json);
                })
            })
            .then( response => {
                const {message} = response
                console.log(message)
                dispatch(updateEventUserSuccess(message))
            })
            .catch(error => {
                dispatch(updateEventUserError(error))
            });
    };
}
