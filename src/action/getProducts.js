import {PRODUCTS} from './actionList';
import {API_NOBO_URL} from  '../utils/globalVariable';

export function getProductsError(err) {
  return {
    type:PRODUCTS.ERROR,
    error: err
  };
}

export function getProductsLoading(bool) {
  return {
    type: PRODUCTS.LOADING,
    isLoading: bool
  };
}

export function getProductsSuccess(productsData) {
  return {
    type: PRODUCTS.SUCCESS,
    productsData
  };
}

export function getProducts(apiToken, id) {
  return (dispatch) => {
    dispatch(getProductsLoading(true));
    fetch(API_NOBO_URL + '/governess/address/product/' + id,
      {
        method: 'get',
        headers: new Headers({
          'content-type': 'application/json',
          'apiToken': apiToken
        })
      }).then((response) => {
      dispatch(getProductsLoading(false));
      let status = response.status;
      return response.json().then((json) => {
        console.log(json);
        if (status !== 200)
          throw json.message;
        return (json);
      })
    })
      .then((productsData) => {
        dispatch(getProductsSuccess(productsData));
      })
      .catch(err => {
        dispatch(getProductsError(err));
      });
  };
}