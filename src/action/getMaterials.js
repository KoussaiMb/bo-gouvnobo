import {Materials} from './actionList';
import {API_NOBO_URL} from  '../utils/globalVariable';

export function getMaterialsError(err) {
  return {
    type:Materials.ERROR,
    error: err
  };
}

export function getMaterialsLoading(bool) {
  return {
    type: Materials.LOADING,
    isLoading: bool
  };
}

export function getMaterialsSuccess(materialsData) {
  return {
    type: Materials.SUCCESS,
      materialsData
  };
}

export function getMaterials(apiToken, id) {
  return (dispatch) => {
    dispatch(getMaterialsLoading(true));
    fetch(API_NOBO_URL + '/governess/address/material/' + id,
      {
        method: 'get',
        headers: new Headers({
          'content-type': 'application/json',
          'apiToken': apiToken
        })
      }).then((response) => {
      dispatch(getMaterialsLoading(false));
      let status = response.status;
      return response.json().then((json) => {
        if (status !== 200)
          throw json.message;
        return (json);
      })
    })
      .then((materialsData) => {
        const {data} = materialsData
        dispatch(getMaterialsSuccess(data));
      })
      .catch(err => {
        dispatch(getMaterialsError(err));
      });
  };
}