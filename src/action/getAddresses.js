import {ADDRESSES} from './actionList';
import {API_NOBO_URL} from  '../utils/globalVariable';

export function getAddressesError(error) {
  return {
    type:ADDRESSES.ERROR,
    error
  };
}

export function getAddressesLoading(isLoading) {
  return {
    type: ADDRESSES.LOADING,
    isLoading
  };
}

export function getAddressesSuccess(list) {
  return {
    type: ADDRESSES.SUCCESS,
    list
  };
}

export function getAddresses(apiToken) {
  return (dispatch) => {
    dispatch(getAddressesLoading(true));
    fetch(API_NOBO_URL + '/governess/address/byCluster',
      {
        method: 'get',
        headers: new Headers({
          'content-type': 'application/json',
          'apiToken': apiToken
        })
      }).then((response) => {
      dispatch(getAddressesLoading(false));
      let status = response.status;
      return response.json().then((json) => {
        if (status !== 200)
          throw json.message;
        return (json);
      })
    }).then( listAddresses => {
        const {data} =  listAddresses
        dispatch(getAddressesSuccess(data));
      }).catch(err => {
        dispatch(getAddressesError(err));
      });
  };
}