//getOnGoingMissions reducer actions
export const ONGOINGMISSIONS = {
    ERROR         : 'ONGOINGMISSIONS_CALL_ERROR',
    SUCCESS       : 'ONGOINGMISSIONS_CALL_SUCCESS',
    LOADING       : 'ONGOINGMISSIONS_CALL_LOADING',
    RESET         : 'ONGOINGMISSIONS_CALL_RESET'
};
//login Reducer actions
export const LOGIN = {
    ERROR         : 'LOGIN_CALL_ERROR',
    SUCCESS       : 'LOGIN_CALL_SUCCESS',
    LOADING       : 'LOGIN_CALL_LOADING',
    RESET         : 'LOGIN_CALL_RESET'
};
//getTodayMissions reducer actions
export const TODAYMISSIONS = {
    ERROR         : 'TODAYMISSIONS_CALL_ERROR',
    SUCCESS       : 'TODAYMISSIONS_CALL_SUCCESS',
    LOADING       : 'TODAYMISSIONS_CALL_LOADING',
    RESET         : 'TODAYMISSIONS_CALL_RESET'
};
//getInspections reducer actions
export const INSPECTIONS = {
    ERROR         : 'INSPECTIONS_CALL_ERROR',
    SUCCESS       : 'INSPECTIONS_CALL_SUCCESS',
    LOADING       : 'INSPECTIONS_CALL_LOADING',
    RESET         : 'INSPECTIONS_CALL_RESET'
};
//getAgendaEntry reducer actions
export const AGENDAENTRY = {
    ERROR         : 'AGENDAENTRY_CALL_ERROR',
    SUCCESS       : 'AGENDAENTRY_CALL_SUCCESS',
    LOADING       : 'AGENDAENTRY_CALL_LOADING',
    RESET         : 'AGENDAENTRY_CALL_RESET'
};
//token Reducer actions
export const TOKEN = {
    ERROR: 'TOKEN_CALL_ERROR',
    SUCCESS: 'TOKEN_CALL_SUCCESS',
    LOADING: 'TOKEN_CALL_LOADING',
    RESET: 'TOKEN_CALL_RESET'
};
//inspdetails Reducer actions
export const INSPDETAILS = {
    ERROR         : 'INSPDETAILS_CALL_ERROR',
    SUCCESS       : 'INSPDETAILS_CALL_SUCCESS',
    LOADING       : 'INSPDETAILS_CALL_LOADING',
    RESET         : 'INSPDETAILS_CALL_RESET'
};
//allCustomersReducer actions
export const ALLCUSTOMERS = {
    ERROR         : 'ALLCUSTOMERS_CALL_ERROR',
    SUCCESS       : 'ALLCUSTOMERS_CALL_SUCCESS',
    LOADING       : 'ALLCUSTOMERS_CALL_LOADING'
};
//getMaintenancePlanning reducer action
export const MAINTENANCEPLANNING = {
    ERROR         : 'MAINTENANCEPLANNING_CALL_ERROR',
    SUCCESS       : 'MAINTENANCEPLANNING_CALL_SUCCESS',
    LOADING       : 'MAINTENANCEPLANNING_CALL_LOADING'
};

//PROVIDERLIST  actions
export const PROVIDERLIST = {
    ERROR       : 'PROVIDERLIST_CALL_ERROR',
    SUCCESS     : 'PROVIDERLIST_CALL_SUCCESS',
    LOADING     : 'PROVIDERLIST_CALL_LOADING',
    RESET       : 'PROVIDERLIST_CALL_RESET'

}

//getWeekMission actions
export const WEEKMISSIONS = {
    ERROR       : 'WEEKMISSIONS_CALL_ERROR',
    SUCCESS     : 'WEEKMISSIONS_CALL_SUCCESS',
    LOADING     : 'WEEKMISSIONS_CALL_LOADING',
    RESET       : 'WEEKMISSIONS_CALL_RESET'
}

//getTodayMissionsProvider actions
export const TODAYMISSIONSPROVIDER = {
    ERROR       : 'TODAYMISSIONSPROVIDER_CALL_ERROR',
    SUCCESS     : 'TODAYMISSIONSPROVIDER_CALL_SUCCESS',
    LOADING     : 'TODAYMISSIONSPROVIDER_CALL_LOADING',
    RESET       : 'TODAYMISSIONSPROVIDER_CALL_RESET'
}
//getMonthMissionsProvider actions
export const MONTHMISSIONSPROVIDER = {
    ERROR       : 'MONTHMISSIONSPROVIDER_CALL_ERROR',
    SUCCESS     : 'MONTHMISSIONSPROVIDER_CALL_SUCCESS',
    LOADING     : 'MONTHMISSIONSPROVIDER_CALL_LOADING',
    RESET       : 'MONTHMISSIONSPROVIDER_CALL_RESET'
}
//getAllEventsByGoverness actions
export const EVENTSBYTYPE = {
    ERROR       : 'EVENTSBYTYPE_CALL_ERROR',
    SUCCESS     : 'EVENTSBYTYPE_CALL_SUCCESS',
    LOADING     : 'EVENTSBYTYPE_CALL_LOADING',
    RESET       : 'EVENTSBYTYPE_CALL_RESET'
}

//getSelectedProvider actions
export const SELECTEDPROVIDER = {
    ERROR       : 'SELECTEDPROVIDER_CALL_ERROR',
    SUCCESS     : 'SELECTEDPROVIDER_CALL_SUCCESS',
    LOADING     : 'SELECTEDPROVIDER_CALL_LOADING',
    RESET       : 'SELECTEDPROVIDER_CALL_RESET'
}

//getSelectedEvent actions
export const SELECTEDEVENT = {
    ERROR       : 'SELECTEDEVENT_CALL_ERROR',
    SUCCESS     : 'SELECTEDEVENT_CALL_SUCCESS',
    LOADING     : 'SELECTEDEVENT_CALL_LOADING',
    RESET       : 'SELECTEDEVENT_CALL_RESET'
}

//getSideStatus actions
export const SIDESTATUS = {
    SUCCESS     : 'SIDESTATUS_CALL_SUCCESS'
}

//getDeleteEvent actions
export const DELETEEVENTMODAL = {
    SUCCESS     : 'DELETEEVENTMODAL_CALL_SUCCESS'
}

//getSelectedDate actions
export const SELECTEDDATE = {
    SUCCESS     : 'SELECTEDDATE_CALL_SUCCESS'
}

//getProducts reducer action
export const PRODUCTS = {
    ERROR: 'PRODUCTS_CALL_ERROR',
    SUCCESS: 'PRODUCTS_CALL_SUCCESS',
    LOADING: 'PRODUCTS_CALL_LOADING'
};

//getAddressInfo reducer action
export const ADDRESSINFO = {
    ERROR  : 'ADDRESSINFO_CALL_ERROR',
    SUCCESS: 'ADDRESSINFO_CALL_SUCCESS',
    LOADING: 'ADDRESSINFO_CALL_LOADING'
};

export const ADDRESSES = {
    ERROR  : 'ADDRESSES_CALL_ERROR',
    SUCCESS: 'ADDRESSES_CALL_SUCCESS',
    LOADING: 'ADDRESSES_CALL_LOADING'
};

//getDeleteEvent reducer action
export const DELETEEVENT = {
    ERROR  : 'DELETEEVENT_CALL_ERROR',
    SUCCESS: 'DELETEEVENT_CALL_SUCCESS',
    LOADING: 'DELETEEVENT_CALL_LOADING',
    RESET  : 'DELETEEVENT_CALL_RESET'
};

//getUpdateEvent reducer action
export const UPDATEEVENT = {
    ERROR  : 'UPDATEEVENT_CALL_ERROR',
    SUCCESS: 'UPDATEEVENT_CALL_SUCCESS',
    LOADING: 'UPDATEEVENT_CALL_LOADING',
    RESET  : 'UPDATEEVENT_CALL_RESET'
};

//getInfoEventModal reducer action
export const INFOEVENTMODAL = {
    SUCCESS: 'INFOEVENTMODAL_CALL_SUCCESS'
};

//userList  actions
export const USERSLIST = {
    ERROR: 'USERSLIST_CALL_ERROR',
    SUCCESS: 'USERSLIST_CALL_SUCCESS',
    LOADING: 'USERSLIST_CALL_LOADING',
    RESET: 'USERSLIST_CALL_RESET'
}

//createEvent actions
export const CREATEEVENT = {
    ERROR       : 'CREATEEVENT_CALL_ERROR',
    SUCCESS     : 'CREEATEEVENT_CALL_SUCCESS',
    LOADING     : 'CREATEEVENT_CALL_LOADING',
    RESET       : 'CREATEEVENT_CALL_RESET'
}

//UpdateEventUser actions
export const UPDATEEVENTUSER = {
    ERROR       : 'UPDATEEVENTUSER_CALL_ERROR',
    SUCCESS     : 'UPDATEEVENTUSER_CALL_SUCCESS',
    LOADING     : 'UPDATEEVENTUSER_CALL_LOADING',
    RESET       : 'UPDATEEVENTUSER_CALL_RESET'
}
//getMaintenancePlanningv2 reducer action
export const MAINTENANCEPLANNINGV2 = {
    ERROR         : 'MAINTENANCEPLANNINGV2_CALL_ERROR',
    SUCCESS       : 'MAINTENANCEPLANNINGV2_CALL_SUCCESS',
    LOADING       : 'MAINTENANCEPLANNINGV2_CALL_LOADING',
    RESET         : 'MAINTENANCEPLANNINGV2_CALL_RESET'
};
//getMpCardex reducer action
export const MPCARDEX = {
    ERROR         : 'MPCARDEX_CALL_ERROR',
    SUCCESS       : 'MPCARDEX_CALL_SUCCESS',
    LOADING       : 'MPCARDEX_CALL_LOADING',
    RESET         : 'MPCARDEX_CALL_RESET'
};
//getMaterials reducer action
export const Materials = {
    ERROR         : 'Materials_CALL_ERROR',
    SUCCESS       : 'Materials_CALL_SUCCESS',
    LOADING       : 'Materials_CALL_LOADING'
};
//getLocalizationLockboxTrash reducer action
export const LocalizationLockboxTrash = {
    ERROR         : 'LocalizationLockboxTrash_CALL_ERROR',
    SUCCESS       : 'LocalizationLockboxTrash_CALL_SUCCESS',
    LOADING       : 'LocalizationLockboxTrash_CALL_LOADING'
};
//getCustomersByCluster reducer action
export const CUSTOMERBYCLUSTER = {
    ERROR         : 'CustomersByCluster_CALL_ERROR',
    SUCCESS       : 'CustomersByCluster_CALL_SUCCESS',
    LOADING       : 'CustomersByCluster_CALL_LOADING'
};

//createMission actions
export const CREATEMISSION = {
  ERROR       : 'CREATEMISSION_CALL_ERROR',
  SUCCESS     : 'CREATEMISSION_CALL_SUCCESS',
  LOADING     : 'CREATEMISSION_CALL_LOADING',
  RESET       : 'CREATEMISSION_CALL_RESET'
}
