import {MAINTENANCEPLANNING} from './actionList';
import {API_NOBO_URL} from  '../utils/globalVariable';

export function getMaintenancePlanningError(err) {
  return {
    type:MAINTENANCEPLANNING.ERROR,
    error: err
  };
}

export function getMaintenancePlanningLoading(bool) {
  return {
    type: MAINTENANCEPLANNING.LOADING,
    isLoading: bool
  };
}

export function getMaintenancePlanningSuccess(maintenanceData) {
  return {
    type: MAINTENANCEPLANNING.SUCCESS,
    maintenanceData
  };
}

export function getMaintenancePlanning(apiToken, id) {
  return (dispatch) => {
    dispatch(getMaintenancePlanningLoading(true));
    fetch(API_NOBO_URL + '/governess/address/planning/' + id,
      {
        method: 'get',
        headers: new Headers({
          'content-type': 'application/json',
          'apiToken': apiToken
        })
      }).then((response) => {
      dispatch(getMaintenancePlanningLoading(false));
      let status = response.status;
      return response.json().then((json) => {
        console.log(json);
        if (status !== 200)
          throw json.message;
        return (json);
      })
    })
      .then((maintenanceData) => {
        dispatch(getMaintenancePlanningSuccess(maintenanceData));
      })
      .catch(err => {
        dispatch(getMaintenancePlanningError(err));
      });
  };
}