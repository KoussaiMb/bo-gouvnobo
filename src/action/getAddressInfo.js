import {ADDRESSINFO} from './actionList';
import {API_NOBO_URL} from  '../utils/globalVariable';

export function getAddressInfoError(err) {
  return {
    type:ADDRESSINFO.ERROR,
    error: err
  };
}

export function getAddressInfoLoading(bool) {
  return {
    type: ADDRESSINFO.LOADING,
    isLoading: bool
  };
}

export function getAddressInfoSuccess(addressData) {
  return {
    type: ADDRESSINFO.SUCCESS,
    addressData
  };
}

export function getAddressInfo(apiToken, id) {
  return (dispatch) => {
    dispatch(getAddressInfoLoading(true));
    fetch(API_NOBO_URL + '/governess/address/' + id,
      {
        method: 'get',
        headers: new Headers({
          'content-type': 'application/json',
          'apiToken': apiToken
        })
      }).then((response) => {
      dispatch(getAddressInfoLoading(false));
      let status = response.status;
      return response.json().then((json) => {
        if (status !== 200)
          throw json.message;
        return (json);
      })
    })
      .then((addressData) => {
        dispatch(getAddressInfoSuccess(addressData));
      })
      .catch(err => {
        dispatch(getAddressInfoError(err));
      });
  };
}