import {CREATEMISSION} from '../actionList'
import {API_NOBO_URL} from  '../../utils/globalVariable'

//resetCreateMissionStatus
export const resetCreateMissionStatus = () => {
    return dispatch => dispatch(createMissionReset())

}

const createMissionReset = () => {
    return {
        type: CREATEMISSION.RESET
    }
}

const createMissionError = error => {
    return {
        type:CREATEMISSION.ERROR,
        error
    };
}

const createMissionLoading = Loading => {
    return {
        type: CREATEMISSION.LOADING,
        Loading
    };
}

const createMissionSuccess = message => {
    return {
        type: CREATEMISSION.SUCCESS,
        message
    };
}

export function createMission(apiToken, mission) {
    return (dispatch) => {
        dispatch(createMissionLoading(true))
        fetch(API_NOBO_URL + `/governess/mission`,
            {
                method: 'post',
                headers: new Headers({
                    'content-type'  :   'application/json',
                    'apiToken'      :   apiToken
                }),
                body: JSON.stringify({
                    "mission" : mission
                })
            }).then((response) => {
                dispatch(createMissionLoading(false))
                let status = response.status
                return response.json().then( json => {
                    if (status !== 200)
                        throw json.message;
                    return (json);
                })
            })
            .then( response => {
                const {message} = response
                dispatch(createMissionSuccess(message))
            })
            .catch(error => {
                dispatch(createMissionError(error))
            });
    };
}