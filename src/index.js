import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Provider } from 'react-redux';
import { Router, Route, Switch } from "react-router-dom";
import store from './store';
import moment from "moment";
import localization from 'moment/locale/fr';

import "./assets/css/material-dashboard-react.css";
import "./assets/css/fullcalendar.css";
import indexRoutes from "./routes/index";
const hist = createBrowserHistory();
moment.locale('fr');

ReactDOM.render(
  <Router history={hist}>
      <Provider store={store}>
          <Switch>
            {indexRoutes.map((prop, key) => {return <Route path={prop.path} component={prop.component} key={key} />;})}
          </Switch>
      </Provider>
  </Router>,
  document.getElementById("root")
);