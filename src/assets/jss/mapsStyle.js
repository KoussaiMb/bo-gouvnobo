import {
  defaultFont,
  mainFont
} from "./mainStyle.js";

const mapsStyle = theme => ({
  titleCard: {
    ...defaultFont,
    lineHeight: "30px",
    fontSize: "18px",
    color: mainFont,
    marginBottom: 16
  }
});

export default mapsStyle;