import React, { Component } from 'react';
import {
    CardContent,
    Typography,
    CircularProgress,
    Button,
    Grid,
    Card
    } from '@material-ui/core'
import {
    Mail,
    Lock
    } from '@material-ui/icons'
import {authenticate, loginReset } from "../action/login";
import LoginInput from "../components/CustomInput/LoginInput";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";
import cookies from "js-cookie";
import logo from "../assets/img/logo.png";

const mapStateToProps = (state) => {
    return {
        loginReducer: state.loginReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        authenticate: (mail, password) =>dispatch(authenticate(mail, password)),
        resetLogin  : () => dispatch(loginReset())
    };
};

class Login extends Component {
    constructor(props) {
        super(props);

        this.loginData = [];
        this.loginData['mail'] = null;
        this.loginData['password'] = null;
    }

    state={
        logged : false
    }

    handleChange(event, field) {
        this.loginData[field] = event.target.value;
    }

    authenticate(event) {
        this.props.authenticate(this.loginData['mail'], this.loginData['password']);
        event.preventDefault();
    }

    componentWillReceiveProps(nextProps){
        const {loginData} = nextProps.loginReducer
        if(loginData && loginData.status === 200){
            this.setState({
                logged : true
            })
        }
    }

    render() {

        const { logged } = this.state
        if(logged){
            const { from } = this.props.location.state || { from: { pathname: "/" } }
            return (<Redirect to={from} />)
        }
        else
            return (
                <Grid style={{height: "100vh"}} container direction="row" alignItems="center" justify="center">
                    <Card>
                        <form onSubmit={this.authenticate.bind(this)}>
                            <CardContent>
                                <Grid container justify="center">
                                    <Grid item>
                                        <Typography variant="title">
                                        Connexion
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <LoginInput onChangeCallback={this.handleChange.bind(this)}
                                fieldType="email" autofocus={true} iconComponent={<Mail/>}
                                dataName="mail" label="E-mail"/>
                                <LoginInput onChangeCallback={this.handleChange.bind(this)}
                                fieldType="password" autofocus={false} iconComponent={<Lock/>}
                                dataName="password" label="Mot de passe"/>
                                <Grid container direction="column" justify="center" alignItems="center">
                                    <Grid item>
                                        <Button type="submit" variant="contained" color="primary" style={{marginTop: "1rem"}}>
                                        { !this.props.loginReducer.isLoading && "Se connecter" }
                                        { this.props.loginReducer.isLoading && <CircularProgress color="secondary" /> }
                                        </Button>
                                    </Grid>
                                    <Grid item style={{paddingTop: "1rem"}}>
                                        <a href="/login">Mot de passe oublié ?</a>
                                    </Grid>
                                    { this.props.loginReducer.error &&
                                      <Grid item style={{paddingTop: "1rem"}}>
                                          <span style={{color: 'red'}}>Mail ou mot de passe invalide</span>
                                      </Grid>
                                    }
                                </Grid>
                            </CardContent>
                        </form>
                    </Card>
                </Grid>)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);