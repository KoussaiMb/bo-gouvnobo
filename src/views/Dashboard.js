import React from "react";
import {Map, TimeLine} from "../components/index.js";
import {Grid} from '@material-ui/core';

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      timeLineSize: 6,
      mapSize: 6
    }
  }

  expandTimeline() {
    this.state.timeLineSize === 6 ? this.setState({timeLineSize: 12}) : this.setState({timeLineSize: 6});
  }

  expandMap() {
    this.state.mapSize === 6 ? this.setState({mapSize: 12}) : this.setState({mapSize: 6});
  }

  render() {
        return (
            <div>
                <Grid container spacing={24} alignItems="flex-start">
          {this.state.mapSize === 12 && <Grid item xs={12} lg={this.state.mapSize}>
              <Map expandMap={this.expandMap.bind(this)} size={this.state.mapSize}/>
          </Grid> }
                    <Grid item xs={12} md={12} lg={this.state.timeLineSize}>
                        <TimeLine expandTimeline={this.expandTimeline.bind(this)} size={this.state.timeLineSize}/>
                    </Grid>
          {this.state.mapSize === 6 && <Grid item xs={12} lg={this.state.mapSize}>
              <Map expandMap={this.expandMap.bind(this)} size={this.state.mapSize}/>
          </Grid> }
                </Grid>
            </div>
        )
  }
}

export default (Dashboard);