import React, { Component } from "react";
import {InspectionList, InspectionDetails, InspectionSearchBar} from "../components/index.js";
import {Grid} from '@material-ui/core';

export default class Inspection extends Component {
  shouldComponentUpdate() {
    return false;
  }

  render() {
    return (
        <Grid container spacing={16} direction="column">
          <Grid item style={{paddingTop: "1rem"}} xs={12} xl={12}>
            <InspectionDetails/>
          </Grid>
          <Grid item style={{paddingTop: "1rem"}}>
            <InspectionSearchBar/>
          </Grid>
          <Grid item style={{paddingTop: "1rem"}}>
            <InspectionList/>
          </Grid>
        </Grid>)
  }
}