import React from "react";
import {Grid} from '@material-ui/core';
import {Provider} from '../components/'


class ProviderView extends React.Component {

    render() {
        return (
            <div>
                <Grid container spacing={24}>
                    <Grid item xs={12} lg={12} style={{paddingTop: "1rem"}}>
                        <Provider />
                    </Grid>
                </Grid>
            </div>
        )
    }

}

export default ProviderView;