import moment from 'moment'

moment.prototype.isCustomAfter = function(event ){
    let repeatType = event.repeat_type,
        week = ''
    if (['monthDay', 'monthDate'].includes(repeatType))
        repeatType = 'month'
    if(repeatType === 'week')
        this.day(event.day)
    if(event.repeat_type === 'monthDay')
        return this.add(event.repeat_rec, 'months').weekOccurrencesPerMonth(event.nday, event.day).isAfter(event.end_rec, 'day')
    return this.add(event.repeat_rec, repeatType.concat('s')).isAfter(event.end_rec, 'day')
}


moment.prototype.weekOccurrencesPerMonth = function(weekNumberOnMonth, dayOnWeek){
    const endMonth      = moment(this).endOf('month')
    let   startMonth    = moment(this).startOf('month'),
          _occurrence   = 0,
          _this         = moment(this),
          matched       = false

    while(startMonth.isBetween(startMonth, endMonth, 'days', '[]') && !matched){
        if(startMonth.day() === dayOnWeek) {
            _occurrence += 1;
            _this = moment(startMonth)
        }
        if(_occurrence === weekNumberOnMonth){
            matched = true
        }
        startMonth = moment(startMonth).add(1, 'days')
    }
    return _this
}

export default moment