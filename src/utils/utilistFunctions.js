import moment from 'moment'

export const getHours = function(time) {
  return time.substr(0, 2);
};

export const getMinutes = function(time) {
  return time.substr(3, 2);
};

export const getSeconds = function(time) {
  return time.substr(6, 2);
};

export const cutSeconds = function(time) {
  return time.substr(0, 5);
};

export const searchInArrayById = function(array, key) {
  let found = false;
  for(let i = 0; i < array.length; i++) {
    if (array[i].id === key) {
      found = true;
      break;
    }
  }
  return (found);
};

export const getInArrayById = function(array, id) {
  for(let i = 0; i < array.length; i++) {
    if (array[i].id === id) {
      return(array[i]);
    }
  }
  return (null);
};

export const cutLongString = function(string, size) {
  if (string.length > size)
    return (string.substr(0, size) + '...');
  else
    return (string);
};





