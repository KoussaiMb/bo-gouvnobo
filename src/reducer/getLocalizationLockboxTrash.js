import { LocalizationLockboxTrash } from '../action/actionList';

const initialState = {
    LocalizationLockboxTrashData: null,
    error: "",
    isLoading: false,
};

export default function getLocalizationLockboxTrash(state = initialState, action) {
  switch (action.type) {
    case LocalizationLockboxTrash.SUCCESS:
      return {
        ...state,
          LocalizationLockboxTrashData: action.LocalizationLockboxTrashData
      };
    case LocalizationLockboxTrash.ERROR:
      return {
        ...state,
        error: action.error
      };
    case LocalizationLockboxTrash.LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      };
    default:
      return state;
  }
};