import { PRODUCTS } from '../action/actionList';

const initialState = {
  productsData: null,
  error: "",
  isLoading: false,
};

export default function getProducts(state = initialState, action) {
  switch (action.type) {
    case PRODUCTS.SUCCESS:
      return {
        ...state,
        productsData: action.productsData
      };
    case PRODUCTS.ERROR:
      return {
        ...state,
        error: action.error
      };
    case PRODUCTS.LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      };
    default:
      return state;
  }
};