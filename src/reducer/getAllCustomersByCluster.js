import { CUSTOMERBYCLUSTER } from '../action/actionList';

export default function getAllCustomersByCluster(state = {list : null, error : null, isLoading : false}, action={}) {
    switch (action.type) {
        case CUSTOMERBYCLUSTER.SUCCESS:
            return state = Object.assign({}, state, { list : action.list})
        case CUSTOMERBYCLUSTER.ERROR:
            return state = Object.assign({}, state, { error : action.error})
        case CUSTOMERBYCLUSTER.LOADING:
            return state = Object.assign({}, state, { isLoading : action.isLoading})
        default:
            return state;
    }
};