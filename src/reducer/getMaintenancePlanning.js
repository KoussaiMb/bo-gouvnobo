import { MAINTENANCEPLANNING } from '../action/actionList';

const initialState = {
  maintenanceData: null,
  error: "",
  isLoading: false,
};

export default function getMaintenancePlanning(state = initialState, action) {
  switch (action.type) {
    case MAINTENANCEPLANNING.SUCCESS:
      return {
        ...state,
        maintenanceData: action.maintenanceData
      };
    case MAINTENANCEPLANNING.ERROR:
      return {
        ...state,
        error: action.error
      };
    case MAINTENANCEPLANNING.LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      };
    default:
      return state;
  }
};