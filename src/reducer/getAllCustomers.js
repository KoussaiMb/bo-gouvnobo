import { ALLCUSTOMERS } from '../action/actionList';

export default function getCustomers(state = {list : null, error : null, isLoading : false}, action={}) {
  switch (action.type) {
    case ALLCUSTOMERS.SUCCESS:
        return state = Object.assign({}, state, { list : action.list})
    case ALLCUSTOMERS.ERROR:
        return state = Object.assign({}, state, { error : action.error})
    case ALLCUSTOMERS.LOADING:
        return state = Object.assign({}, state, { isLoading : action.isLoading})
    default:
      return state;
  }
};