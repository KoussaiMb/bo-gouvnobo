import {CREATEMISSION} from './../../action/actionList'



//createMissionReducer
export const createMissionReducer = (state = {message: null, status: null, error: null, Loading: false}, action = {}) => {
    switch (action.type) {
        case CREATEMISSION.SUCCESS:
            return state = Object.assign({}, state, { message : action.message , status : 200})
        case CREATEMISSION.ERROR:
            return state = Object.assign({}, state, { error : action.error , status : 400})
        case CREATEMISSION.LOADING:
            return state = Object.assign({}, state, { Loading : action.Loading})
        case CREATEMISSION.RESET:
            return state = Object.assign({}, state, { message : null , status : null})
        default:
            return state
    }
}