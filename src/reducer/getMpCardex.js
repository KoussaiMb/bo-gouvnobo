import { MPCARDEX } from '../action/actionList';

const initialState = {
    maintenanceData: null,
    error: "",
    isLoading: false,
};

export default function getMpCardexV2(state = initialState, action) {
    switch (action.type) {
        case MPCARDEX.SUCCESS:
            return {
                ...state,
                maintenanceData: action.maintenanceData
            };
        case MPCARDEX.ERROR:
            return {
                ...state,
                error: action.error
            };
        case MPCARDEX.LOADING:
            return {
                ...state,
                isLoading: action.isLoading
            };
        case MPCARDEX.RESET:
            return {
                ...state,
                maintenanceData: null
            };
        default:
            return state;
    }
};