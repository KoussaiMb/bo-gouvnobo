import { INSPECTIONS } from '../action/actionList';

const initialState = {
  inspectionsData: null,
  error: "",
  isLoading: false,
};

export default function getInspectionsReducer(state = initialState, action) {
  switch (action.type) {
    case INSPECTIONS.SUCCESS:
      return {
        ...state,
        error: "",
        inspectionsData: action.inspectionsData
      };
    case INSPECTIONS.ERROR:
      return {
        ...state,
        error: action.error
      };
    case INSPECTIONS.LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      };
    case INSPECTIONS.RESET:
      return {
        ...state,
        isLoading: false,
        error: "",
        inspectionsData: null
      };
    default:
      return state;
  }
};