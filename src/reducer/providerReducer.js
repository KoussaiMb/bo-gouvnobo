import {PROVIDERLIST, WEEKMISSIONS, SELECTEDPROVIDER, TODAYMISSIONSPROVIDER, MONTHMISSIONSPROVIDER, SELECTEDEVENT, SIDESTATUS, SELECTEDDATE, EVENTSBYTYPE, DELETEEVENTMODAL, DELETEEVENT, INFOEVENTMODAL, UPDATEEVENT, USERSLIST, CREATEEVENT, UPDATEEVENTUSER} from '../action/actionList'
//TODO: split into several files && delete those that are not used

//getProviderListReducer
export const getProviderListReducer = (state = {providerList: null,error: "",Loading: false}, action = {}) => {
    switch (action.type) {
        case PROVIDERLIST.SUCCESS:
            return state = Object.assign({}, state, { providerList : action.providerList})
        case PROVIDERLIST.ERROR:
            return state = Object.assign({}, state, { error : action.error})
        case PROVIDERLIST.LOADING:
            return state = Object.assign({}, state, { Loading : action.Loading})
        case PROVIDERLIST.RESET:
            return state = Object.assign({}, state, { providerList : action.providerList})
        default:
            return state
    }
}

//getWeekMissionReducer
export const getWeekMissionReducer = (state = {weekMissions: null,error: null,Loading: false}, action = {}) => {
    switch (action.type) {
        case WEEKMISSIONS.SUCCESS:
            return state = Object.assign({}, state, { weekMissions : action.weekMissions})
        case WEEKMISSIONS.ERROR:
            return state = Object.assign({}, state, { error : action.error})
        case WEEKMISSIONS.LOADING:
            return state = Object.assign({}, state, { Loading : action.Loading})
        default:
            return state
    }
}

//getSelectedProviderReducer
export const getSelectedProviderReducer = (state = {provider: null, Loading: false}, action = {}) => {
    switch (action.type) {
        case SELECTEDPROVIDER.SUCCESS:
            return state = Object.assign({}, state, { provider : action.provider})
        case SELECTEDPROVIDER.LOADING:
            return state = Object.assign({}, state, { Loading : action.Loading})
        default:
            return state
    }
}

//getTodayMissionsProviderReducer
export const getTodayMissionsProviderReducer = (state = {missions: null, error:null, Loading: false}, action = {}) => {
    switch (action.type) {
        case TODAYMISSIONSPROVIDER.SUCCESS:
            return state = Object.assign({}, state, { missions : action.missions})
        case TODAYMISSIONSPROVIDER.ERROR:
            return state = Object.assign({}, state, { error : action.error})
        case TODAYMISSIONSPROVIDER.LOADING:
            return state = Object.assign({}, state, { Loading : action.Loading})
        default:
            return state
    }
}

//getMonthMissionsProviderReducer
export const getMonthMissionsProviderReducer = (state = {missions: null, error:null, Loading: false}, action = {}) => {
    switch (action.type) {
        case MONTHMISSIONSPROVIDER.SUCCESS:
            return state = Object.assign({}, state, { missions : action.missions})
        case MONTHMISSIONSPROVIDER.ERROR:
            return state = Object.assign({}, state, { error : action.error})
        case MONTHMISSIONSPROVIDER.LOADING:
            return state = Object.assign({}, state, { Loading : action.Loading})
        default:
            return state
    }
}

//getSideStatusReducer
export const getSideStatusReducer = (state = {open:null}, action = {}) => {
    switch (action.type) {
        case SIDESTATUS.SUCCESS:
            return state = Object.assign({}, state, { open : action.open})
        default:
            return state
    }
}

//getSelectedDateReducer
export const getSelectedDateReducer = (state = {date: false}, action = {}) => {
    switch (action.type) {
        case SELECTEDDATE.SUCCESS:
            return state = Object.assign({}, state, { date : action.date})
        default:
            return state
    }
}

//getEventsByTypeReducer
export const getEventsByTypeReducer = (state = {events: null, error:null, Loading: false}, action = {}) => {
    switch (action.type) {
        case EVENTSBYTYPE.SUCCESS:
            return state = Object.assign({}, state, { events : action.events})
        case EVENTSBYTYPE.ERROR:
            return state = Object.assign({}, state, { error : action.error})
        case EVENTSBYTYPE.LOADING:
            return state = Object.assign({}, state, { Loading : action.Loading})
        case EVENTSBYTYPE.RESET:
            return state = Object.assign({}, state, { events : action.events})
        default:
            return state
    }
}


//getSelectedEventReducer
export const getSelectedEventReducer = (state = {event: null}, action = {}) => {
    switch (action.type) {
        case SELECTEDEVENT.SUCCESS:
            return state = Object.assign({}, state, { event : action.event})
        default:
            return state
    }
}


//getDeleteEventModalReducer
export const getDeleteEventModalReducer = (state = {open: false}, action = {}) => {
    switch (action.type) {
        case DELETEEVENTMODAL.SUCCESS:
            return state = Object.assign({}, state, { open : action.open})
        default:
            return state
    }
}


//getInfoEventModalReducer
export const getInfoEventModalReducer = (state = {open: null}, action = {}) => {
    switch (action.type) {
        case INFOEVENTMODAL.SUCCESS:
            return state = Object.assign({}, state, { open : action.open})
        default:
            return state
    }
}


//getDeleteEventReducer
export const getDeleteEventReducer = (state = {message: null, status: null, error:null, Loading: false}, action = {}) => {
    switch (action.type) {
        case DELETEEVENT.SUCCESS:
            return state = Object.assign({}, state, { message : action.message , status : 200})
        case DELETEEVENT.ERROR:
            return state = Object.assign({}, state, { error : action.error , status: 400})
        case DELETEEVENT.LOADING:
            return state = Object.assign({}, state, { Loading : action.Loading})
        case DELETEEVENT.RESET:
            return state = Object.assign({}, state, { message : null , status : null})
        default:
            return state
    }
}


//getUpdateEventReducer
export const getUpdateEventReducer = (state = {message: null, status: null, error:null, Loading: false}, action = {}) => {
    switch (action.type) {
        case UPDATEEVENT.SUCCESS:
            return state = Object.assign({}, state, { message : action.message , status : 200})
        case UPDATEEVENT.ERROR:
            return state = Object.assign({}, state, { error : action.error, status: 400 })
        case UPDATEEVENT.LOADING:
            return state = Object.assign({}, state, { Loading : action.Loading})
        case UPDATEEVENT.RESET:
            return state = Object.assign({}, state, { message : null , status : null})
        default:
            return state
    }
}


//getUsersListReducer
export const getUsersListReducer = (state = {list : null ,error: "",Loading: false}, action = {}) => {
    switch (action.type) {
        case USERSLIST.SUCCESS:
            return state = Object.assign({}, state, {list : action.list})
        case USERSLIST.ERROR:
            return state = Object.assign({}, state, { error : action.error})
        case USERSLIST.LOADING:
            return state = Object.assign({}, state, { Loading : action.Loading})
        case USERSLIST.RESET:
            return state = Object.assign({}, state, { list : null})
        default:
            return state
    }
}

//createEventReducer
export const createEventReducer = (state = {message: null, status: null, error: null, Loading: false}, action = {}) => {
  switch (action.type) {
    case CREATEEVENT.SUCCESS:
      return state = Object.assign({}, state, { message : action.message , status : 200})
    case CREATEEVENT.ERROR:
      return state = Object.assign({}, state, { error : action.error , status : 400})
    case CREATEEVENT.LOADING:
      return state = Object.assign({}, state, { Loading : action.Loading})
    case CREATEEVENT.RESET:
      return state = Object.assign({}, state, { message : null , status : null})
    default:
      return state
  }
}

//updateEventUserReducer
export const updateEventUserReducer = (state = {message: null, status: null, error: null, Loading: false}, action = {}) => {
  switch (action.type) {
    case UPDATEEVENTUSER.SUCCESS:
      return state = Object.assign({}, state, { message : action.message , status : 200})
    case UPDATEEVENTUSER.ERROR:
      return state = Object.assign({}, state, { error : action.error , status : 400})
    case UPDATEEVENTUSER.LOADING:
      return state = Object.assign({}, state, { Loading : action.Loading})
    case UPDATEEVENTUSER.RESET:
      return state = Object.assign({}, state, { message : null , status : null})
    default:
      return state
  }
}