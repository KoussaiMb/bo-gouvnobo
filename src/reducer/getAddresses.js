import { ADDRESSES } from '../action/actionList';

export default function getAddresses(state = {list : null, error : null, isLoading : false}, action={}) {
  switch (action.type) {
    case ADDRESSES.SUCCESS:
        return state = Object.assign({}, state, { list: action.list})
    case ADDRESSES.ERROR:
        return state = Object.assign({}, state, { error: action.error})
    case ADDRESSES.LOADING:
        return state = Object.assign({}, state, { isLoading: action.isLoading})
    default:
      return state;
  }
};