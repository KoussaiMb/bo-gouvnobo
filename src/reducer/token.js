import { TOKEN } from '../action/actionList';

const initialState = {
  tokenData: null,
  error: "",
  isLoading: false,
};

export default function getTokenReducer(state = initialState, action) {
  switch (action.type) {
    case TOKEN.SUCCESS:
      return {
        ...state,
        tokenData: action.tokenData
      };
    case TOKEN.ERROR:
      return {
        ...state,
        error: action.error
      };
    case TOKEN.LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      };
    case TOKEN.RESET:
      return {
        ...state,
        isLoading: false,
        error: "",
        tokenData: null
      };
    default:
      return state;
  }
};