import { Materials } from '../action/actionList';

const initialState = {
  materialsData: null,
  error: "",
  isLoading: false,
};

export default function getMaterials(state = initialState, action) {
  switch (action.type) {
    case Materials.SUCCESS:
      return {
        ...state,
          materialsData: action.materialsData
      };
    case Materials.ERROR:
      return {
        ...state,
        error: action.error
      };
    case Materials.LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      };
    default:
      return state;
  }
};