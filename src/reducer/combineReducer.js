import {combineReducers} from 'redux';
import getOnGoingMissionsReducer from './getOnGoingMissions.js';
import getTodayMissions from './getTodayMissions.js';
import getAgendaEntry from './getAgendaEntry.js';
import loginReducer from './login';
import tokenReducer from './token';
import inspectionsReducer from './inspections';
import inspectionDetailsReducer from './inspectionDetails';
import getAllCustomers from './getAllCustomers';
import getMaintenancePlanning from './getMaintenancePlanning';
import getMaintenancePlanningv2 from './getMaintenancePlanningv2';
import getAllCustomersByCluster from './getAllCustomersByCluster';
import getLocalizationLockboxTrash from './getLocalizationLockboxTrash';
import getMaterials from './getMaterials';
import getMpCardex from './getMpCardex';
import getProducts from './getProducts';
import getAddressInfo from './getAddressInfo';
import {getProviderListReducer, getWeekMissionReducer, getSelectedProviderReducer, getTodayMissionsProviderReducer, getMonthMissionsProviderReducer, getSelectedEventReducer, getSideStatusReducer, getSelectedDateReducer, getEventsByTypeReducer, getDeleteEventModalReducer, getDeleteEventReducer, getInfoEventModalReducer, getUpdateEventReducer, getUsersListReducer, createEventReducer, updateEventUserReducer} from './providerReducer';
import getAddresses from './getAddresses';
import {createMissionReducer} from './provider/createMissionReducer';

export default combineReducers({
    getOnGoingMissionsReducer,
    getTodayMissions,
    getAgendaEntry,
    loginReducer,
    getProducts,
    inspectionsReducer,
    inspectionDetailsReducer,
    getAllCustomers,
    getMaintenancePlanning,
    getMaintenancePlanningv2,
    getAllCustomersByCluster,
    getLocalizationLockboxTrash,
    getAddressInfo,
    getMaterials,
    getMpCardex,
    tokenReducer,
    getProviderListReducer,
    getWeekMissionReducer,
    getSelectedProviderReducer,
    getTodayMissionsProviderReducer,
    getMonthMissionsProviderReducer,
    getSelectedEventReducer,
    getSideStatusReducer,
    getSelectedDateReducer,
    getEventsByTypeReducer,
    getDeleteEventModalReducer,
    getDeleteEventReducer,
    getInfoEventModalReducer,
    getUpdateEventReducer,
    getUsersListReducer,
    createEventReducer,
    getAddresses,
    updateEventUserReducer,
    createMissionReducer
});
