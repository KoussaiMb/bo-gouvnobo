import { ADDRESSINFO } from '../action/actionList';

const initialState = {
  addressData: null,
  error: "",
  isLoading: false,
};

export default function getAddressInfo(state = initialState, action) {
  switch (action.type) {
    case ADDRESSINFO.SUCCESS:
      return {
        ...state,
        addressData: action.addressData
      };
    case ADDRESSINFO.ERROR:
      return {
        ...state,
        error: action.error
      };
    case ADDRESSINFO.LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      };
    default:
      return state;
  }
};