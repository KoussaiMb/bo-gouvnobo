import { INSPDETAILS } from '../action/actionList';

const initialState = {
  inspDetailsData: null,
  error: "",
  isLoading: false,
};

export default function getInspDetailsReducer(state = initialState, action) {
  switch (action.type) {
    case INSPDETAILS.SUCCESS:
      return {
        ...state,
        inspDetailsData: action.inspDetailsData
      };
    case INSPDETAILS.ERROR:
      return {
        ...state,
        error: action.error
      };
    case INSPDETAILS.LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      };
    case INSPDETAILS.RESET:
      return {
        ...state,
        isLoading: false,
        error: "",
        inspDetailsData: null
      };
    default:
      return state;
  }
};