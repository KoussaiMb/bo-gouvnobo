import { ONGOINGMISSIONS } from '../action/actionList';

const initialState = {
  onGoingMissionsData: null,
  error: "",
  isLoading: false,
};

export default function getOnGoingMissionsReducer(state = initialState, action) {
  switch (action.type) {
    case ONGOINGMISSIONS.SUCCESS:
      return {
        ...state,
        onGoingMissionsData: action.onGoingMissionsData
      };
    case ONGOINGMISSIONS.ERROR:
      return {
        ...state,
        error: action.error
      };
    case ONGOINGMISSIONS.LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      };
    case ONGOINGMISSIONS.RESET:
      return {
        ...state,
        onGoingMissionsData: action.onGoingMissionsData
      };
    default:
      return state;
  }
};