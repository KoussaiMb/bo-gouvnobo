import { MAINTENANCEPLANNINGV2 } from '../action/actionList';

const initialState = {
    maintenanceData: null,
    error: "",
    isLoading: false,
};

export default function getMaintenancePlanningV2(state = initialState, action) {
    switch (action.type) {
        case MAINTENANCEPLANNINGV2.SUCCESS:
            return {
                ...state,
                maintenanceData: action.maintenanceData
            };
        case MAINTENANCEPLANNINGV2.ERROR:
            return {
                ...state,
                error: action.error
            };
        case MAINTENANCEPLANNINGV2.LOADING:
            return {
                ...state,
                isLoading: action.isLoading
            };
        case MAINTENANCEPLANNINGV2.RESET:
            return {
                ...state,
                maintenanceData: null
            };
        default:
            return state;
    }
};