import { AGENDAENTRY } from '../action/actionList';

const initialState = {
  agendaEntryData: null,
  error: "",
  isLoading: false
};

export default function getAgendaEntry(state = initialState, action) {
  switch (action.type) {
    case AGENDAENTRY.SUCCESS:
      return {
        ...state,
        agendaEntryData: action.agendaEntryData
      };
    case AGENDAENTRY.ERROR:
      return {
        ...state,
        error: action.error
      };
    case AGENDAENTRY.LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      };
    default:
      return state;
  }
};