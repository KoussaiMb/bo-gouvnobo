import { TODAYMISSIONS } from '../action/actionList';

const initialState = {
  todayMissionsData: null,
  error: "",
  isLoading: false
};

export default function getTodayMissions(state = initialState, action) {
  switch (action.type) {
    case TODAYMISSIONS.SUCCESS:
      return {
        ...state,
        todayMissionsData: action.todayMissionsData
      };
    case TODAYMISSIONS.ERROR:
      return {
        ...state,
        error: action.error
      };
    case TODAYMISSIONS.LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      };
    default:
      return state;
  }
};