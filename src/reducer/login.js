import { LOGIN } from '../action/actionList';

const initialState = {
  loginData: null,
  error: "",
  isLoading: false,
};

export default function getLoginReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN.SUCCESS:
      return {
        ...state,
        loginData: action.loginData
      };
    case LOGIN.ERROR:
      return {
        ...state,
        error: action.error
      };
    case LOGIN.LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      };
    case LOGIN.RESET:
      return {
          ...state,
        loginData: null,
        error: "",
        isLoading: false
      };
    default:
      return state;
  }
};