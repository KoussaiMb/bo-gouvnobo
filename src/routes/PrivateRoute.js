import React, { Component } from 'react';
import cookies from "js-cookie";
import {connect} from "react-redux";
import { Route, Redirect } from "react-router-dom";
import { CircularProgress, Grid } from "@material-ui/core";
import { checkToken, tokenReset } from "../action/token";
import {loginReset } from "../action/login";

const mapStateToProps = (state) => {
    return {
        tokenReducer: state.tokenReducer,
        loginReducer: state.loginReducer

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        checkToken: (token) => dispatch(checkToken(token)),
        resetToken: () => dispatch(tokenReset()),
        resetLogin: () => dispatch(loginReset())
    };
};

class PrivateRoute extends Component {

    state={
        isAuthenticated : false
    }
    componentWillMount() {
        const {loginData} = this.props.loginReducer
        if(loginData && loginData.status === 200){
            cookies.set('loadingToken', true)
            this.props.checkToken(loginData.apiToken)
            cookies.set('token',loginData.apiToken, { expires: 1 , secure : process.env.NODE_ENV === 'production'})
            this.props.resetLogin();
            return
        }

        if(cookies.get('token')){
            cookies.set('loadingToken', true)
            this.props.checkToken(cookies.get('token'))
        }
    }

    componentWillReceiveProps(nextProps){
        const {tokenData, error} = nextProps.tokenReducer
        if(tokenData && tokenData.status === 200 && cookies.get('loadingToken') === 'true'){
            cookies.set('loadingToken', false)
            this.props.resetToken();
            this.setState({
                isAuthenticated : true
            })
        }

        if(error && cookies.get('loadingToken') === 'true'){
            Object.keys(cookies.get()).map( cookie => {
                cookies.remove(cookie, {path : ''})
            })
        }
    }

    render() {
        if (cookies.get('loadingToken') === 'true'){
            return (
                <Grid container style={{height: "100vh"}} justify="center" alignItems="center">
                    <Grid item>
                        <CircularProgress color="primary" size={50} />
                    </Grid>
                </Grid>);
        }
        const { component: Component, ...props } = this.props,
    { isAuthenticated } = this.state
    return (
        <Route {...props} render={props => isAuthenticated ?  <Component {...props}/> : <Redirect to={{pathname: "/login"}}/> }/>
    )}

}

export default connect(mapStateToProps, mapDispatchToProps)(PrivateRoute);