import DashboardPage from "../views/Dashboard.js";
import ProviderPage from "../views/Provider.js";
import InspectionPage from "../views/Inspection.js";
import CustomerPage from "../views/Customer.js";
import {
  Dashboard,
  Person,
  Face,
  Assignment
} from "@material-ui/icons";

const dashboardRoutes = [
  {
    path: "/dashboard",
    sidebarName: "Tableau de bord",
    navbarName: "Tableau de bord",
    icon: Dashboard,
    component: DashboardPage
  },
  {
    path: "/provider",
    sidebarName: "Prestataires",
    navbarName: "Vos Prestataires",
    icon: Person,
    component: ProviderPage
  },
  {
    path: "/inspection",
    sidebarName: "Inspections",
    navbarName: "Vos Inspections",
    icon: Assignment,
    component: InspectionPage
  },
  {
    path: "/customer",
    sidebarName: "Clients",
    navbarName: "Vos Clients",
    icon: Face,
    component: CustomerPage
  },
  { redirect: true, path: "/", to: "/dashboard", navbarName: "Redirect" }
];

export default dashboardRoutes;
