import Dashboard from "../App.js";
import Login from "../views/Login.js"

const indexRoutes = [
  { path: "/login", component: Login },
  { path: "/", component: Dashboard }
];

export default indexRoutes;