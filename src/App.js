import React from "react";
import PropTypes from "prop-types";
import { Switch, Redirect } from "react-router-dom";
import { Sidebar, Header } from "./components";
import dashboardRoutes from "./routes/dashboard.js";
import appStyle from "./assets/jss/appStyle.js";
import logo from "./assets/img/nobo-logo-white.png";
import PrivateRoute from "./routes/PrivateRoute";
import {
    withStyles,
    createMuiTheme,
    MuiThemeProvider
    } from '@material-ui/core';

const switchRoutes = (
  <Switch>
    {dashboardRoutes.map((prop, key) => {
      if (prop.redirect)
        return <Redirect from={prop.path} to={prop.to} key={key} />;
      return <PrivateRoute path={prop.path} component={prop.component} key={key}/>;
    })}
  </Switch>
);

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#ffac96',
      main: '#ff978A',
      dark: '#e6604e',
      contrastText: '#fff'
    },
    secondary: {
      light: '#ff7961',
      main: '#e6604e',
      dark: '#ba000d',
      contrastText: '#000'
    }
  }
});

class App extends React.Component {
  state = {
    mobileOpen: false
  };
  handleDrawerToggle = () => {
    this.setState({ mobileOpen: !this.state.mobileOpen });
  };

  getRoute() {
    return this.props.location.pathname !== "/maps";
  }

  componentDidMount() {
  }
  componentDidUpdate() {
    //this.refs.mainPanel.scrollTop = 0;
  }
  render() {
    const { classes, ...rest } = this.props;
    return (
      <MuiThemeProvider theme={theme}>
      <div className={classes.wrapper}>
        <Sidebar
          routes={dashboardRoutes}
          logo={logo}
          handleDrawerToggle={this.handleDrawerToggle}
          open={this.state.mobileOpen}
          color="red"
          {...rest}
        />
        <div className={classes.mainPanel} ref="mainPanel">
          <Header
            routes={dashboardRoutes}
            handleDrawerToggle={this.handleDrawerToggle}
            {...rest}
          />
          {/* On the /maps route we want the map to be on full screen - this is not possible if the content and conatiner classes are present because they have some paddings which would make the map smaller */}
          {this.getRoute() ? (
            <div className={classes.content}>
              <div className={classes.container}>{switchRoutes}</div>
            </div>
          ) : (
            <div className={classes.map}>{switchRoutes}</div>
          )}
        </div>
      </div>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(appStyle)(App);
