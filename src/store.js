import { createStore, applyMiddleware } from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import combineReducer from './reducer/combineReducer';

const getMiddleware = () => {
    if(process.env.NODE_ENV === 'development')
        return applyMiddleware(thunk, logger)
    return applyMiddleware(thunk)
}

const store = createStore(combineReducer,{},  composeWithDevTools(getMiddleware()))

export default store;

