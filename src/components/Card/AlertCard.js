import React from 'react';
import PropTypes from 'prop-types';
import {
    withStyles,
    Card,
    CardContent,
    Typography
    } from '@material-ui/core';

const styles = {
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    marginBottom: 16,
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
};

function AlertCard(props) {
  const { classes } = props;

  return (
    <Card>
      <CardContent>
        <Typography>
          <span style={{fontWeight: 'bold', fontSize: '18px', color: "#e6604e"}}>/</span> Alertes
        </Typography>
        <Typography variant="headline" component="h2">
          /!\
        </Typography>
        <Typography component="p">
          ALERTE
        </Typography>
      </CardContent>
    </Card>
  );
}

AlertCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AlertCard);