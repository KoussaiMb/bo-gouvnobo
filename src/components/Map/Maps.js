/* global google */

import React, {Component } from "react";
import {connect} from 'react-redux';
import {compose} from "redux";
import cookies from 'js-cookie';
import {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
        } from "react-google-maps";
import {
    Card,
    Typography,
    CardContent,
    withStyles,
    Grid,
    CardActions,
    Button
    } from '@material-ui/core';
import {
  PersonPinCircle,
  Schedule,
  Home
} from "@material-ui/icons";
import mapsStyle from '../../assets/jss/mapsStyle'
import {getOnGoingMissions} from '../../action/getOnGoingMissions';
import { MarkerWithLabel } from "react-google-maps/lib/components/addons/MarkerWithLabel";

const mapStateToProps = (state) => {
  return {
    onGoingMissionsData: state.getOnGoingMissionsReducer
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getOnGoingMissions: (apiToken) =>
      dispatch(getOnGoingMissions(apiToken))
  };
};

const CustomSkinMap = withScriptjs(
  withGoogleMap(props => (
    <GoogleMap
      defaultZoom={13}
      defaultCenter={{ lat: 48.860912, lng: 2.348343 }}
      defaultOptions={{
        scrollwheel: true,
        zoomControl: true
      }}
    >
      {props.missions && props.missions.onGoingMissionsData &&
      props.missions.onGoingMissionsData.data &&
      props.missions.onGoingMissionsData.data.map((mission) => {
        let end = new Date(mission.end);
        let start = new Date(mission.start);
        return (
          <MarkerWithLabel labelAnchor={new google.maps.Point(0, 0)}
                           labelStyle={{backgroundColor: "#e6604e", fontSize: "10px",
                             padding: "5px", color: 'white', width: "7rem",
                             border: '2px solid #ff978A', borderRadius: '10px', opacity: 0.9}}
                           key={mission.id} position={{lat: parseFloat(mission.address.lat), lng: parseFloat(mission.address.lng)}}>
            <div>
              <Grid container spacing={8} alignItems="flex-end">
                <Grid item xs={3}>
                  <Home />
                </Grid>
                <Grid item xs={9}>
                  <p style={{fontWeight: "bold"}}>{mission.customer.firstname.substr(0, 1) + ('.')}
                    {mission.customer.lastname.length >= 9 ? mission.customer.lastname.substr(0, 9) + ('...') : mission.customer.lastname}</p>
                </Grid>
              </Grid>
              <Grid container spacing={8} alignItems="flex-end">
                <Grid item xs={3}>
                  <PersonPinCircle />
                </Grid>
                <Grid item xs={9}>
                  <p style={{fontWeight: "bold"}}>{mission.provider.user.firstname.substr(0, 1) + ('.')}
                    {mission.provider.user.lastname.length >= 9 ? mission.provider.user.lastname.substr(0, 9) + ('...') : mission.provider.user.lastname}</p>
                </Grid>
              </Grid>
              <Grid container spacing={8} alignItems="flex-end">
                <Grid item xs={3}>
                  <Schedule/>
                </Grid>
                <Grid item xs={9}>
                  <p style={{fontWeight: "bold"}}>{start.getHours() <= 9 ? '0' + start.getHours() : start.getHours()}:{start.getMinutes() <= 9 ? '0' + start.getMinutes() + ' - ' : start.getMinutes() + ' - '}
                    {end.getHours() <= 9 ? '0' + end.getHours() : end.getHours()}:{end.getMinutes() <= 9 ? '0' + end.getMinutes() : end.getMinutes()}</p>
                </Grid>
              </Grid>
            </div>
          </MarkerWithLabel>
        )
      })
      }
    </GoogleMap>
  ))
);

class Maps extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.getOnGoingMissions(cookies.get("token"));
  }

  render() {
    return (
      <Card>
        <CardContent>
          <Typography>
            <span style={{fontWeight: 'bold', fontSize: '18px', color: "#e6604e"}}>/</span> Missions en cours
          </Typography>
            <CustomSkinMap
              googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDkfDcFdPduSfiEkfCIkDMJQdEZzROkzqE"
              loadingElement={<div style={{height: `100%`}}/>}
              containerElement={<div style={{height: `60vh`}}/>}
              mapElement={<div style={{height: `100%`}}/>}
              missions={this.props.onGoingMissionsData}
            />
        </CardContent>
        <CardActions>
          <Button onClick={this.props.expandMap} size="small">{this.props.size === 6 ? "Agrandir" : "Réduire"}</Button>
        </CardActions>
      </Card>
    );
  }
}

export default compose(
  withStyles(mapsStyle, {
    name: 'mapsStyle',
  }),
  connect(mapStateToProps, mapDispatchToProps),
)(Maps);