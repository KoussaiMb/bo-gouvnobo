import React,{ Component } from 'react'
import {connect} from 'react-redux'
import {compose} from 'redux'
import { withStyles } from '@material-ui/core'
import Picker from './../stateless/Picker'
import {getSideStatus} from '../../action/providerActions'
import Options from './Options'
//TODO: remove getSideStatus && wwwwwwReducer

const styles = theme => ({
    root: {
        margin:'Opx',
        borderBottom:'solid 0.1px #ddd'
    }
})

const mapStateToProps = state => {
    return {
        open        : state.getSideStatusReducer
    }
}

class Side extends Component {
    state={
        open: true
    }

    componentWillReceiveProps(nextProps){
        const {open} = nextProps.open
            this.setState({
                open
            })
    }

    render(){
        const {classes} = this.props
        const {open}    = this.state
        if(open === true){
            return(
                <div id="sideCalendar" value={open}>
                    <div className={classes.root} ><Picker /></div>
                    {/* Not functional */}
                    {/* <div className={classes.root} ><Options /></div> */}
                </div>
            )
        }
        return <div style={{display : 'none'}} />

    }
}



export default  compose(withStyles(styles), connect(mapStateToProps,null))(Side)