import React, {Component} from 'react';
import {
    withStyles,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Radio,
    RadioGroup,
    FormControlLabel,
    FormControl
    } from '@material-ui/core';

const styles = theme => ({
    formControl: {
        margin: 0
    },
    group: {
        margin: `${theme.spacing.unit}px 0`
    }
})


class choiceEventModal extends Component {
    state = {
        value               : 'uniqueEvent',
        uniqueEvent         : false,
        allNextSubEvents    : false,
        allSubEvents        : false
    };

    componentWillReceiveProps(nextProps){
        const {choices} = nextProps
        this.setState({
            uniqueEvent         : false,
            allNextSubEvents    : false,
            allSubEvents        : false
        })
        if(choices && typeof(choices) === "object" && choices.length !== 0 ){
            choices.map(item =>
                    this.setState({
                        [item] : true,
                        value  : choices[0]
                    })
            )
        }
    }

    handleChange = event => {
        this.setState({ value: event.target.value });
    }

    handleSubmit = () => {
        this.props.onSubmit(this.state.value)
        this.setState({
            uniqueEvent         : false,
            allNextSubEvents    : false,
            allSubEvents        : false
        })
    }

    render() {
        const {classes, open, title} = this.props,
              {value, uniqueEvent, allNextSubEvents, allSubEvents} = this.state
        return (
                <Dialog
                open={open}
                onClose={this.props.onClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                maxWidth="xs"
                >
                    <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            <FormControl component="fieldset" className={classes.formControl}>
                                <RadioGroup
                                name="delete"
                                className={classes.group}
                                value={value}
                                onChange={this.handleChange}
                                >
                                {uniqueEvent         && <FormControlLabel value="uniqueEvent"      control={<Radio />} label="Cet événement"/>}
                                {allNextSubEvents    && <FormControlLabel value="allNextSubEvents" control={<Radio />} label="Cet événement et tous les suivants"/>}
                                {allSubEvents        && <FormControlLabel value="allSubEvents"     control={<Radio />} label="Tous les événements"/>}
                                </RadioGroup>
                            </FormControl>
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.props.onClose} >
                        ANNULER
                        </Button>
                        <Button onClick={this.handleSubmit} color="primary" autoFocus>
                        OK
                        </Button>
                    </DialogActions>
                </Dialog>
        );
    }
}

export default  withStyles(styles)(choiceEventModal)
