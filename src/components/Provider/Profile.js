import React,{Component} from 'react';
import {compose} from "redux"
import {connect} from 'react-redux'
import {
    withStyles,
    Paper,
    Input,
    InputLabel,
    InputAdornment,
    FormControl,
    Button
    } from '@material-ui/core';
import Icon from '../stateless/icon'
import PlacesAutocomplete, {
	geocodeByAddress,
	getLatLng,
} from 'react-places-autocomplete';

const styles = theme => ({
		//button
		button: {
				margin: theme.spacing.unit
		},
		rightIcon: {
				marginLeft: theme.spacing.unit
		},
		//BuildInput
		margin: {
				margin: theme.spacing.unit
		},
		maxWidth:{
			width: '90%',
			margin: '2vh'
		},
		minWidth:{
				width: '40%',
				margin: '2vh',
				marginRight: '8vh'
		},
		//page
		paper: {
		paddingTop: theme.spacing.unit * 2,
		//paddingBottom: theme.spacing.unit * 20,
		height: '60vh',
		...theme.mixins.gutters()
}
})

const BuildInput = props => {
		let icon = props.icon
		return (
				<FormControl className={props.classname} key={props.default+props.text+props.id}>
						<InputLabel htmlFor={props.id}>{props.text}</InputLabel>
						<Input
						id={props.id}
						defaultValue={props.default}
						onChange={(e) => props.onChange(e)}
						disabled={props.disabled}
						startAdornment={
								<InputAdornment position="start">
										<Icon icon={icon} />
								</InputAdornment>
								}
						/>
				</FormControl>
		)
}

const mapStateToProps = state => {
		return {
				selectedProvider    : state.getSelectedProviderReducer
		}
}

class Profile extends Component {
	constructor(props) {
		super(props);
		this.state = {
			id 								: null,
			firstname 				: null,
			lastname  				: null,
			formatted_address : "",
			address						: "",
			address_ext				: null,
			city 							: null,
			country 					: null,
			zipcode						: null,
			email  		  			: null,
			phone  	 	  			: null,
		};
	}

		setstate(provider){
			let long_address = provider.user.address[0].address
			+ ', '+ provider.user.address[0].zipcode
			+ ' ' + provider.user.address[0].city
			+ ',' + provider.user.address[0].country;
			geocodeByAddress(long_address)
			.then(results => {
				this.setState({
						id        				: provider.user.id,
						firstname 				: provider.user.firstname,
						lastname  				: provider.user.lastname,
						formatted_address : results[0].formatted_address,
						address 					: results[0].address_components[0].long_name + ' ' + results[0].address_components[1].long_name ,
						address_ext				: results[0].address_components[4].long_name,
						city 							: results[0].address_components[2].long_name,
						country 					: results[0].address_components[5].long_name,
						zipcode						: results[0].address_components[6].long_name,
						email     				: provider.user.email,
						phone     				: provider.user.phone
				})
			})
		}

		componentWillReceiveProps(nextProps){
				const { provider } = nextProps.selectedProvider
				if(provider.user.firstname !== this.state.firstname)
						this.setstate(provider)
		}

		componentWillMount(){
				const { provider } = this.props.selectedProvider
				if(provider && provider.user.firstname !== this.state.firstname)
						this.setstate(provider)
		}


		handleChange = formatted_address => {
		 this.setState({ formatted_address });
		};

		handleSelect = address => {
			geocodeByAddress(address)
				.then(results => {
					this.setState({
						formatted_address 	: results[0].formatted_address,
						address 						: results[0].address_components[0].long_name + ' ' + results[0].address_components[1].long_name ,
						address_ext					: results[0].address_components[4].long_name,
						city 								: results[0].address_components[2].long_name,
						country 						: results[0].address_components[5].long_name,
						zipcode							: results[0].address_components[6].long_name,
					})
					return (getLatLng(results[0]))
				})
				.then(latLng => console.log('Success', latLng))
				.catch(error => console.error('Error', error));
		};

		render () {
				const { classes } = this.props
				const { id, firstname, lastname, formatted_address, address, email, phone } = this.state
				return(
								<Paper className={classes.paper}>
										<BuildInput classname={classes.minWidth} id="firstname" text="First Name" default={firstname} onChange={(text) => this.state.firstname = text.target.value} icon="Face" />
										<BuildInput classname={classes.minWidth} id="lastname" text="Last Name" default={lastname} onChange={(text) => this.state.lastname = text.target.value} icon="Face" />
										<PlacesAutocomplete
											value={formatted_address}
											onChange={this.handleChange}
											onSelect={this.handleSelect}
										>
											{({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
												<FormControl className={classes.maxWidth}>
													<InputLabel htmlFor={"formatted_address"}>{"Address complete"}</InputLabel>
													<Input
													startAdornment={
														<InputAdornment position="start">
																<Icon icon={"Home"} />
														</InputAdornment>
													}
														{...getInputProps({
															placeholder: 'Search Places ...',
															className: 'location-search-input',
														})}
													/>
													<div className="autocomplete-dropdown-container">
														{loading && <div>Loading...</div>}
														{suggestions.map(suggestion => {
															const className = suggestion.active
																? 'suggestion-item--active'
																: 'suggestion-item';
															// inline style for demonstration purpose
															const style = suggestion.active
																? { backgroundColor: '#fafafa', cursor: 'pointer' }
																: { backgroundColor: '#ffffff', cursor: 'pointer' };
															return (
																<div
																	{...getSuggestionItemProps(suggestion, {
																		className,
																		style,
																	})}
																>
																	<span>{suggestion.description}</span>
																</div>
															);
														})}
													</div>
												</FormControl>
											)}
										</PlacesAutocomplete>
										<BuildInput classname={classes.margin, classes.maxWidth} id="email" text="Email" default={email} onChange={(text) => this.state.email = text.target.value} icon="Email" />
										<BuildInput classname={classes.margin, classes.maxWidth} id="phone" text="Phone" default={phone} onChange={(text) => this.state.phone = text.target.value} icon="Phone" />
										<BuildInput classname={classes.margin, classes.minWidth} id="subscribed_at" text="Subscribed at" default={"JJ/MM/AAAA"} icon="Phone" disabled={true}/>
										<BuildInput classname={classes.margin, classes.minWidth} id="validated_by" text="Validated by" default={"Admin"} icon="Phone" disabled={true}/>
										{this.props.selectedProvider.provider !== null &&
											<Button variant="contained" color="primary" className={classes.button}>
												Save
												<Icon icon="Save" classname={classes.rightIcon} />
											</Button>}
										{this.props.selectedProvider.provider !== null &&
											<Button variant="contained" color="secondary" className={classes.button}>
												Delete
												<Icon icon="Delete" classname={classes.rightIcon} />
											</Button>}
								</Paper>
				);
		}
}

export default compose(withStyles(styles), connect(mapStateToProps, null))(Profile)
//TODO: fix buttons