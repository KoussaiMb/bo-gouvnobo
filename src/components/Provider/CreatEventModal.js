import React, {Component} from 'react';
import {connect} from 'react-redux';
import { compose } from 'redux';
import cookies from "js-cookie"
import moment from "moment";

import {
    withStyles,
    TextField,
    Select,
    MenuItem,
    FormControl,
    InputLabel,
    ListItem,
    ListItemIcon,
    ListItemText,
    Button,
    Grid
    } from '@material-ui/core';
import { Assignment } from "@material-ui/icons";
import Snackbar from '../stateless/Snackbars';
import {getAllCustomers} from '../../action/getAllCustomers';
import {createEvent} from '../../action/providerActions';
import {getAddresses} from '../../action/getAddresses';
//TODO: split into several files

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  button: {
    margin: theme.spacing.unit
  },
  flex : {
    display: 'flex',
    flexWrap : 'nowrap'
  },
  formControl: {
    minWidth: 120,
    marginTop: "30px",
    marginBottom: "30px",
    marginRight: "30px"
  },
  card: {
    maxWidth: 345,
    minWidth: 345
  },
  CardActions:{
    flexDirection : 'row',
    justifyContent : 'flex-end'
  },
  cardTitle:{
    width: '100%',
    marginLeft: '60px'
  },
  cardContent: {
    flexDirection : 'column ',
    padding:0,
    paddingBottom: '10px'
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    marginTop: "30px",
    marginBottom: "30px"
  },
  textField: {
    width: 200
  },
  fab: {
    position: 'relative',
    bottom: theme.spacing.unit * 2.2,
    left: theme.spacing.unit * 1.5,
    color: '#fff'
  },
  popover: {
    marginLeft : theme.spacing.unit
  },
  nested: {
    paddingLeft: theme.spacing.unit * 4
  }
});

class CreateEventModal extends Component {
  constructor(props) {
    super(props);
    //this.day = this.getDayOfSelectedDate();
    //this.weekNb = this.weekOfMonth(moment(this.props.selectedDate));
    this.filterAddresses = [];
  }

    state = {
        title: '',
        comment: '',
        type: 'availability',
        address_id: null,
        start_rec: moment(this.props.selectedDate).format('YYYY-MM-DD'),
        end_rec: moment(this.props.selectedDate).format('YYYY-MM-DD'),
        start_time: moment().set('minute', 0).format('HH:mm'),
        end_time:   moment().set('minute', 0).add(120,'minutes').format('HH:mm'),
        duration: 0,
        locked: 0,
        pending:0,
        day: 0,
        nday: 0,
        repeat_type: 'punctual',
        repeat_rec: 1,
        users: [],
        address: '',
        user: '',
        userId: null,
        customersList : [],
        addressesList : [],
        error        : false
    }

  weekOfMonth(m) {
    let firstDay = moment().startOf('month').day();
    let numberOfDay = m.week() - moment(m).startOf('month').week() + 1;
    if (firstDay > m.day() && m.day() !== 0)
      numberOfDay -= 1;
    return numberOfDay;
  }

  componentDidMount() {
    this.props.getAddresses(cookies.get("token"));
    this.props.getAllCustomers(cookies.get("token"));
  }

    componentWillReceiveProps(nextProps){
        const {customers, addresses, selectedDate} = nextProps
        if(selectedDate){
            this.setState({
                day : selectedDate.day(),
                nday: this.weekOfMonth(selectedDate)
            })
        }
        if(customers.list){
            //console.log(customers.list)
            this.setState({
                customersList : customers.list
            })
        }
        if(addresses.list){
            //console.log(addresses.list)
            this.setState({
                addressesList : addresses.list
            })
        }
    }

  handleTypeChange(e) {
    this.setState({
      type: e.target.value
    });
  }

  handleAddressChange = ({target}) => {
      const {addressesList} = this.state
      let    find     = addressesList.find(item => {return item.address === target.value})
      this.setState({
          address   : target.value,
          address_id : find.id
      })
  }

    handleChange = ({target}) => {
        this.setState({
            [target.id] : target.value
        })
    }

  handleTitleChange({target}) {
    this.setState({
      title: target.value
    })
  }

  handleDateChange = ({target}) => {
    this.setState({
      [target.id]: target.value
    })
  }

  handleRepeatChange = ({target}) => {
    this.setState({
      [target.name]: target.value
    })
  }

  handleCustomerChange = ({target})=> {
       const {users, customersList} = this.state
       let    findOnC     = customersList.find(item => {return item.firstname + ' ' + item.lastname === target.value}),
              _users      = users,
              findOnU     = _users.find( _user => {return _user.user_id === findOnC.id})
       if(findOnU === undefined)
            _users.push({
                user_id     : findOnC.id,
                subtitle    : 'subtitle',
                subComment  : 'subComment',
                available   : 1
            })
       //else
            //_users.splice(_users.indexOf(find.id),1)

       this.setState({
           user   : target.value,
           users  : _users,
           userId : findOnC.id
       })
  }

  handleTimeChange = ({target}) => {
      this.setState({
      [target.id]: target.value
    })

  }

  submitEvent = () => {
      const {provider}  = this.props
      let eventToCreate = this.state,
          users         = eventToCreate.users
      users.push(
          {
              user_id : provider.user.id,
              subtitle: 'subtitle',
              subComment : 'subComment',
              available  : 1
          }
      )
      eventToCreate.start_time = eventToCreate.start_time.concat(':00')
      eventToCreate.end_time   = eventToCreate.end_time.concat(':00')
      delete eventToCreate.addressesList
      delete eventToCreate.customersList
      delete eventToCreate.address
      delete eventToCreate.userId
      delete eventToCreate.error
      delete eventToCreate.users
      delete eventToCreate.user
      this.props.onSubmit(eventToCreate)
      this.props.createEvent(cookies.get("token"),eventToCreate, users)
      this.props.onClose()
      cookies.set('createEvent', true)
  }

    closeNotify = () => {
        this.setState({
            error : false
        })
    }

  render() {
      const {customersList, addressesList, type, user, userId,  address, end_rec, start_time, end_time, repeat_type, error, nday} = this.state,
            {provider} = this.props
       if(addressesList && addressesList.length !== 0)
           this.filterAddresses = addressesList.filter( address => {
               return address.user === userId
            })
    const { classes, selectedDate} = this.props;
    return (
      <div style={{width: "50vw"}}>
        <ListItem>
          <ListItemIcon><Assignment/></ListItemIcon>
          <ListItemText primary={provider != null ?
            "Ajouter un évènement pour " + provider.user.firstname + " " + provider.user.lastname
            : "Ajouter un évènement"} />
        </ListItem>
        <TextField
          label="Titre"
          id="title"
          InputLabelProps={{
            shrink: true
          }}
          placeholder="Ajouter un titre"
          margin="normal"
          onChange={this.handleChange}
        />
        <Grid container style={{overflow : 'hidden'}}>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="type-simple">Type</InputLabel>
              <Select
                value={type}
                onChange={this.handleTypeChange.bind(this)}
                inputProps={{
                  name: 'type',
                  id: 'type-simple'
                }}
              >
                <MenuItem value={'availability'}>Disponibilité</MenuItem>
                <MenuItem value={'benefit'}>Prestation</MenuItem>
                <MenuItem value={'appointment'}>Rendez-vous</MenuItem>
                <MenuItem value={'firstAppointment'}>Premier rendez-vous</MenuItem>
              </Select>
            </FormControl>
            {type !== 'availability' &&
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="user-simple">Client</InputLabel>
                        <Select
                        value={user}
                        onChange={this.handleCustomerChange}
                        inputProps={{
                            name: 'user',
                            id: 'user-simple'
                        }}
                        >
                        { customersList && customersList.map(item => {
                            return (
                                <MenuItem key={item.id} value={item.firstname + ' ' + item.lastname}>{item.firstname + ' ' + item.lastname}</MenuItem>
                            );
                        })
                            }
                        </Select>
                    </FormControl>
                }
            {type !== 'availability' && user !== '' &&
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="address-simple">Adresse</InputLabel>
                        <Select
                            value={address}
                            onChange={this.handleAddressChange}
                            inputProps={{
                            name: 'address',
                            id: 'address-simple'
                            }}
                        >
                        { this.filterAddresses.map(item => {
                        return (
                            <MenuItem key={item.id} value={item.address}>{item.address}</MenuItem>
                        );
                        })
                        }
                        </Select>
                    </FormControl>
                    }
        </Grid>
        <form className={classes.container} noValidate>
          <TextField
            id="end_rec"
            type="date"
            label="Date de fin"
            value={end_rec}
            className={classes.textField}
            InputLabelProps={{
              shrink: true
            }}
            onChange={this.handleDateChange}
          />
        </form>
        <form className={classes.container} noValidate>
          <TextField
            id="start_time"
            type="time"
            label="Heure de début"
            value={start_time}
            className={classes.textField}
            InputLabelProps={{
              shrink: true
            }}
            onChange={this.handleTimeChange}
          />
        </form>
        <form className={classes.container} noValidate>
          <TextField
            id="end_time"
            type="time"
            label="Heure de fin"
            value={end_time}
            className={classes.textField}
            InputLabelProps={{
              shrink: true
            }}
            onChange={this.handleTimeChange}
          />
        </form>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="repeat-simple">Récurrence</InputLabel>
          <Select
            value={repeat_type}
            onChange={this.handleRepeatChange}
            inputProps={{
              name: 'repeat_type',
              id: 'repeat-simple'
            }}
          >
            <MenuItem value={'punctual'}>Une seule fois</MenuItem>
            <MenuItem value={'day'}>Tous les jours</MenuItem>
            <MenuItem value={'week'}>Toutes les semaines le {moment(selectedDate).format('dddd')}</MenuItem>
            <MenuItem value={'monthDay'}>Tous les mois le {nday > 1 ? nday + "ème ": nday + "er"} {moment(selectedDate).format('dddd')}</MenuItem>
            <MenuItem value={'monthDate'}>Tous les mois le {moment(selectedDate).format('Do')}</MenuItem>
            <MenuItem value={'year'}>Tous les ans le {moment(selectedDate).format('D MMMM')}</MenuItem>
            <MenuItem value={'other'} disabled="true">Autres options</MenuItem>
          </Select>
        </FormControl>
          <Grid container style={{overflow : 'hidden'}}>
              <Button variant="contained" className={classes.button} color="primary" onClick={this.submitEvent}>
                VALIDER
              </Button>
              <Button variant="contained" className={classes.button}  onClick={this.props.onClose}>
                ANNULER
              </Button>
          </Grid>
      {error &&  <Snackbar  open={Boolean(error)} variant="warning" message={error} onClose={this.closeNotify} verticalLocation="top" horizontalLocation="right" />}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    addresses: state.getAddresses,
    customers: state.getAllCustomers,
    createMessage: state.createEvent
  };
};

function mapDispatchToProps(dispatch) {
  return {
    getAddresses : (apiToken) => dispatch(getAddresses(apiToken)),
    getAllCustomers: (apiToken) => dispatch(getAllCustomers(apiToken)),
    createEvent: (apiToken, eventToCreate, users) =>
      dispatch(createEvent(apiToken, eventToCreate, users))
  };
}

export default  compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(CreateEventModal)
