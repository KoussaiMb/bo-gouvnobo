import React,{ Component } from 'react'
import PropTypes from 'prop-types';
import {connect} from 'react-redux'
import {compose} from "redux"
import {
    withStyles,
    Grid,
    IconButton
    } from '@material-ui/core';
import { DateRange, Menu } from '@material-ui/icons';
import {getSideStatus} from '../../action/providerActions'

const mapDispatchToProps = dispatch => {
    return {
        getSideStatus  :    open => dispatch(getSideStatus(open))
    }
}

const styles = theme => ({
    root: {
        flexGrow: 1,
        lineHeight:'65px'
    },
    flex:{
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems:'center'
    }
})

class Title extends Component {

    state ={
        open : true
    }

    handleClick (open){
        this.props.getSideStatus(open)
        this.setState({
            open
        })
    }

    render(){
        const {classes} = this.props
        const {open} = this.state
        return (
            <div id="sideTitle">
                <Grid container className={classes.root}>
                    <Grid key="humborgIcon" item lg={3} md={3} xs={3}>
                        <IconButton id="menu" style={{color:'red'}} aria-label="open" value={open} onClick={() => this.handleClick(!open)}>
                            <Menu style={{fontSize:'30'}}/>
                        </IconButton>
                    </Grid>
                    <Grid key="agendaIcon" className={classes.flex} item lg={2} md={2} xs={2}>
                        <DateRange  style={{fontSize:'30', color:'#4285f4'}}/>
                    </Grid >
                    <Grid key="agenda" item xs  style={{fontSize:'1.7em', color:'lightgray'}}>
                    Agenda
                    </Grid>
                </Grid>
            </div>
        )
    }
}

Title.propTypes = {
    classes: PropTypes.object.isRequired
};

export default compose(withStyles(styles), connect(null,mapDispatchToProps))(Title)