import React,{Component} from "react"
import cookies from "js-cookie"
import {connect} from 'react-redux'
import {compose} from 'redux'
import { Grid, withStyles } from '@material-ui/core'
import {getProviderList} from '../../action/providerActions'
import {getAllCustomers} from '../../action/getAllCustomers'
import SideProvider from './sideProvider'
import ContentProvider from './contentProvider'

const styles = theme => ({
    root: {
        flexGrow: 1
    }
})

const mapDispatchToProps = dispatch => {
    return {
        getAllCustomers         :    apiToken => dispatch(getAllCustomers(apiToken)),
        getProviderList         :    apiToken => dispatch(getProviderList(apiToken))

    }
}

class providerListContainer extends Component {
    componentWillMount(){
        this.props.getAllCustomers(cookies.get("token"))
        this.props.getProviderList(cookies.get("token"))
    }

    render() {
        const { classes } = this.props
        return (
            <div className={classes.root}>
                <Grid container spacing={8}>
                    <Grid item lg={2} md={12} xs>
                        <SideProvider />
                    </Grid>
                    <Grid item   xs>
                        <ContentProvider />
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default  compose(withStyles(styles), connect(null,mapDispatchToProps))(providerListContainer)