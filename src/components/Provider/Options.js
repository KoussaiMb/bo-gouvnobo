import React,{ Component } from 'react'
import PropTypes from 'prop-types';
import {
    withStyles,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    ListItemSecondaryAction,
    Checkbox,
    IconButton,
    Collapse,

    } from '@material-ui/core';
import {
    InsertInvitation,
    ExpandLess,
    ExpandMore,
    Cancel
    } from '@material-ui/icons';

const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper
    },
    nested: {
        paddingLeft: theme.spacing.unit * 4
    }
});

class Options extends Component {
    state = {
        open: true,
        checked: ['1ere rendez-vous','Disponibilité'],
        options: [
            '1ere rendez-vous',
            'Disponibilité',
            'Prestations',
            'Réunion',
            'Rappels',
        ]
    }

    handleClick = () => {
        this.setState(state => ({ open: !state.open }))
    }

    handleCheck = value => {
        const { checked } = this.state;
        const currentIndex = checked.indexOf(value)
        const newChecked = [...checked]

        if (currentIndex === -1) {
            newChecked.push(value)
        } else {
            newChecked.splice(currentIndex, 1)
        }

        this.setState({
            checked: newChecked
        })
    }

    dropeAgenda = value => {
        const { options } = this.state;
        const currentIndex = options.indexOf(value)
        const newOptions = [...options]
        newOptions.splice(currentIndex, 1)
        this.setState({
            options: newOptions
        })

    }

    render(){
        const {classes} = this.props
        const {open, options}    = this.state
        //console.log(checked)
        return (
            <div className={classes.root}>
                <List
                component="nav">
                    <ListItem button onClick={this.handleClick}>
                        <ListItemIcon>
                            <InsertInvitation />
                        </ListItemIcon>
                        <ListItemText inset primary="Mes Agendas" />
                        {open ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <List component="div" disablePadding>
                            {options.map( value => (
                                <ListItem
                                key={value}
                                role={undefined}
                                dense
                                button
                                onClick={() => this.handleCheck(value)}
                                className={classes.listItem}
                                >
                                    <Checkbox
                                    checked={this.state.checked.indexOf(value) !== -1}
                                    tabIndex={-1}
                                    disableRipple
                                    />
                                    <ListItemText primary={value} />
                                    <ListItemSecondaryAction>
                                        <IconButton aria-label="Suprimer" style={{color: 'red'}} onClick={() => this.dropeAgenda(value)}>
                                            <Cancel />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem>

                            ))}
                        </List>
                    </Collapse>
                </List>
            </div>
        )
    }
}

Options.propTypes = {
    classes: PropTypes.object.isRequired
}

export default withStyles(styles)(Options)

