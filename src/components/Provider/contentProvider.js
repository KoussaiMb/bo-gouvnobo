import React,{Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {compose} from "redux"
import {
    withStyles,
    AppBar,
    Tabs,
    Tab,
    Typography
    } from '@material-ui/core'
import {
    AccountCircle,
    Work,
    EventNote
    } from '@material-ui/icons'
import Profile from './Profile'
import Mission from './Missions'
import Snackbars from '../stateless/Snackbars'

function TabContainer(props) {
    return (
        <Typography component="div" style={{ padding: 10}}>{props.children}</Typography>
    )
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired
};

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper
    },
    nav: {
        overflow: 'auto',
        height : '80vh'
    }
});

class contentProvider extends Component {
    state = {
        value: 1
    };

    handleChange = (event, value) => {
        this.setState({ value });
    };

    render() {
        const { classes } = this.props
        const { value } = this.state
        return (
            <div className={classes.root}>
                <AppBar position="static" color="default">
                    <Tabs
                    fullWidth
                    value={value}
                    onChange={this.handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    centered
                    >
												{/* Not functional */}
                        {/* <Tab label="Profile" icon={<AccountCircle />}/> */}
                        <Tab label="Missions" icon={<Work />}/>
												{/* Not functional (Print nothing)*/}
                        {/* <Tab label="Inspections" icon={<EventNote />} /> */}
                    </Tabs>
                </AppBar>
                <div className={classes.nav}>
										{/* Not functional */}
										{/* {value === 0 && <TabContainer><Profile /></TabContainer>} */}
										{<TabContainer><Mission /></TabContainer>}
										{/* Not functional (Print nothing)*/}
                    {/* {value === 2 && <TabContainer><Snackbars /></TabContainer>} */}
                </div>

            </div>
        )
    }
}

contentProvider.propTypes = {
    classes: PropTypes.object.isRequired
};

export default compose(withStyles(styles), connect(null, null))(contentProvider)

