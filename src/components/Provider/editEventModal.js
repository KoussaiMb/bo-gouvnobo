import React from 'react';
import {compose} from 'redux'
import {connect} from 'react-redux'
import PropTypes from 'prop-types';
import cookies from "js-cookie";
import moment from 'moment'
import {
    withStyles,
    Button,
    Dialog,
    ListItemText,
    ListItem,
    List,
    ListItemSecondaryAction,
    IconButton,
    Typography,
    Slide,
    Grid,
    FormControl,
    TextField,
    Checkbox,
    FormControlLabel,
    MenuItem,
    Select,
    Tabs,
    Tab,
    ExpansionPanel,
    ExpansionPanelDetails,
    ExpansionPanelSummary
    } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';
import CancelIcon from '@material-ui/icons/Cancel';
import PlaceIcon from '@material-ui/icons/Place';
import WorkIcon from '@material-ui/icons/Work';
import ShortTextIcon from '@material-ui/icons/ShortText';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {getUpdateEvent, getUpdateEventUser, resetUpdateEventUserStatus} from '../../action/providerActions'
import Snackbar from '../stateless/Snackbars';
import ModalChoice from './choiceEventModal'
//TODO: split into several files

const styles = theme => ({
    root: {
        flexGrow: 1
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary
    },
    flex: {
        flex: 1
    },
    margin: {
        margin: theme.spacing.unit * 2
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    textFieldRoot: {
        padding: 0,
        'label + &': {
            marginTop: theme.spacing.unit * 3
        }
    },
    textFieldInput: {
        backgroundColor: '#f5f5f5',
        paddingLeft: '10px',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        '&:focus': {
            borderColor: '#80bdff',
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)'
        }
    },
    title : {
        width: '100%'
    },
    tabsRoot: {
        borderBottom: '1px solid #e8e8e8'
    },
    tabsIndicator: {
        backgroundColor: '#1890ff'
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: 72,
        fontWeight: theme.typography.fontWeightRegular,
        marginRight: theme.spacing.unit * 4,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        '&:hover': {
            color: '#40a9ff',
            opacity: 1
        },
        '&$tabSelected': {
            color: '#1890ff',
            fontWeight: theme.typography.fontWeightMedium
        },
        '&:focus': {
            color: '#40a9ff'
        }
    },
    typography: {
        padding: theme.spacing.unit * 3
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120
    },
    heightAndBorderRight:{
        borderRight: '#e0e0e0 1px solid',
        height : '75vh',
        overflow : 'auto'
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        //flexBasis: '33.33%',
        flexShrink: 0
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary
    },
    panel: {
        width: '100%'
    }
})

const mapDispatchToProps = dispatch => {
    return {
        getUpdateEvent        :    (apiToken, oldSubEvent,eventToUpdate, op, id)  => dispatch(getUpdateEvent(apiToken, oldSubEvent,eventToUpdate, op, id)),
        getUpdateEventUser    :    (apiToken, eventId,event_users)  => dispatch(getUpdateEventUser(apiToken, eventId,event_users)),
        resetUpdateEventUserStatus : () => dispatch(resetUpdateEventUserStatus())
    }
}

const mapStateToProps = state => {
    return {
        listCustomers   : state.getAllCustomers,
        listProvider    : state.getProviderListReducer,
        provider        : state.getSelectedProviderReducer,
        updateEventUser : state.updateEventUserReducer
    }
}

function contains(array, id){
    let exist = false
    let i = 0
    while((i < array.length) && !exist){
        if(array[i].id === id)
            exist = true
        i+=1
    }
    return exist
}

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

class editEventModal extends React.Component {
    constructor(props) {
        super(props);
        //this.day = this.getDayOfSelectedDate();
        //this.weekNb = this.weekOfMonth(moment(this.props.selectedDate));
        this.filteredProvider = [];
        this.filteredCustomer = [];
        this.filteredUsers = [];
    }

    state = {
        openSnackbar        : false,
        variantSnackbar     : '',
        messageSnackbar     : '',
        horizontalLocation  : '',
        verticalLocation    : '',
        available           : 0,
        tab                 : 0,
        tabInv              : 0,
        eventToUpdate       : {},
        oldEvent            : {},
        selectedProvider    : null,
        expanded            : null,
        openChoiceEventModal: false,
        which               : ['customer', 'provider'],
        choices             : ['uniqueEvent', 'allNextSubEvents', 'allSubEvents'],
        diffStartDate       : 0,
        diffStartEnd        : 0,
        diffStartEndTime    : 0
    }
    componentWillMount(){
        cookies.set('getUpdateEventUser', false)
        const {event} = this.props
            event.event_users.map( event_user => {
                this.filteredUsers.push(event_user.user)
            })
            this.setState({
                eventToUpdate    : event,
                oldEvent         : event,
                diffStartEnd     : moment(event.end).diff(moment(event.start),'days'),
                diffStartEndTime : (moment(`${event.start}T${event.endTime}`)).diff(moment(`${event.start}T${event.startTime}`),'minutes'),
            })
    }

    componentWillReceiveProps(nextProps){
        const {listCustomers, listProvider, provider, updateEventUser} = nextProps,
              {oldEvent, eventToUpdate, updateMode} = this.state
        if(updateEventUser && cookies.get('getUpdateEventUser') === 'true'){
            const {status} = updateEventUser
            if(status === 200 ){
                this.props.resetUpdateEventUserStatus()
                this.props.getUpdateEvent(cookies.get("token"), oldEvent, eventToUpdate, updateMode, eventToUpdate.id)
                cookies.set("getUpdateEvent", true)
                this.props.onSubmit()
                this.props.onClose()
            }
        }
        if(provider.provider){
            this.setState({
                selectedProvider : provider.provider
            })
        }

        if(listCustomers.list && cookies.get('getUpdateEventUser') !== 'true'){
            listCustomers.list.map( customer => {
                if(!contains(this.filteredUsers, customer.id) && !contains(this.filteredCustomer, customer.id))
                    this.filteredCustomer.push(customer)
            })
        }
        if(listProvider.providerList && cookies.get('getUpdateEventUser') !== 'true'){
            listProvider.providerList.data.map( provider => {
                if(!contains(this.filteredUsers, provider.user.id) && !contains(this.filteredProvider, provider.user.id))
                    this.filteredCustomer.push(provider.user)
            })
        }
    }

    handleSubmitUpdate = (updateMode) => {
        this.closeChoiceEventModal()
        const {diffStartDate} = this.state
        let eventToUpdate = this.state.eventToUpdate,
            oldEvent      = this.state.oldEvent
        eventToUpdate = Object.assign({}, eventToUpdate, {start_time : eventToUpdate.startTime, end_time : eventToUpdate.endTime, duration : moment(eventToUpdate.end).diff(moment(eventToUpdate.start), 'days')})
        if(updateMode === 'allNextSubEvents' && diffStartDate !== 0)
            eventToUpdate.newToken  = true
        if(updateMode === 'uniqueEvent')
        {
            eventToUpdate.start_rec = moment(eventToUpdate.start).format('YYYY-MM-DD')
            eventToUpdate.end_rec   = moment(eventToUpdate.end).format('YYYY-MM-DD')
        }
        else
        {
            eventToUpdate.start_rec = (moment(oldEvent.start).add(diffStartDate, 'days')).format('YYYY-MM-DD')
        }
        delete eventToUpdate.event_users
        delete eventToUpdate.resourceId
        delete eventToUpdate.subEvents
        delete eventToUpdate.textColor
        delete eventToUpdate.startTime
        delete eventToUpdate.anchorEl
        delete eventToUpdate.endTime
        delete eventToUpdate.address
        delete eventToUpdate.color
        delete oldEvent.event_users
        delete oldEvent.resourceId
        delete oldEvent.subEvents
        delete oldEvent.textColor
        delete oldEvent.startTime
        delete oldEvent.anchorEl
        delete oldEvent.endTime
        delete oldEvent.address
        this.setState({
            oldEvent,
            eventToUpdate,
            updateMode
        })
        let users = []
        this.filteredUsers.map( user => {
            let obj ={
                user_id : user.id,
                event_id: eventToUpdate.id,
                subtitle: '',
                subComment: '',
                available : 1
            }
            users.push(obj)
        })
        cookies.set('getUpdateEventUser', true)
        this.props.getUpdateEventUser(cookies.get("token"),eventToUpdate.id, users)
    }

    handleChange = ({target}) => {
        const {start, startTime} = this.state.eventToUpdate
        let diffStartEnd = this.state.diffStartEnd
        let diffStartEndTime= this.state.diffStartEndTime
        switch (target.id){
            case 'start' :
                let choices = this.state.choices
                if(moment(target.value).diff(moment(this.state.oldEvent.start), 'days') !== 0)
                    choices = ['uniqueEvent', 'allNextSubEvents']
                else
                    choices = ['uniqueEvent', 'allNextSubEvents', 'allSubEvents']
                this.setState({
                    eventToUpdate : Object.assign({}, this.state.eventToUpdate, {
                        start: target.value,
                        end  : moment(target.value).add(diffStartEnd, 'days')
                    }),
                    diffStartDate : moment(target.value).diff(moment(this.state.oldEvent.start), 'days'),
                    choices
                })

                break
            case 'end' :
                if(moment(target.value).isSameOrAfter(moment(start), 'day')){
                    this.setState({
                        eventToUpdate : Object.assign({}, this.state.eventToUpdate, {
                            end: target.value
                        }),
                        diffStartEnd : moment(target.value).diff(moment(this.state.eventToUpdate.start),'days')
                    })
                }
                else
                {
                    this.openSnackbar('La Date de fin ne peut etre inferirure a la Date de debut', 'warning')
                }
                break

            case 'startTime' :
                this.setState({
                    eventToUpdate : Object.assign({}, this.state.eventToUpdate, {
                        startTime : target.value,
                        endTime   :  (moment(`${start}T${target.value}`).add(diffStartEndTime, 'minutes')).format('HH:mm')
                    })
                })
                break
            case 'endTime' :
                if(moment(`${start}T${target.value}`).isSameOrAfter(moment(`${start}T${startTime}`), 'minute')){
                    this.setState({
                        eventToUpdate : Object.assign({}, this.state.eventToUpdate, {
                            endTime   : target.value
                        }),
                        diffStartEndTime : moment(`${start}T${target.value}`).diff(moment(`${start}T${startTime}`),'minutes')
                    })
                }
                else
                {
                    this.openSnackbar("L'heure de fin de l'evente ne peut etre inferirure a celle de debut", 'warning')
                }
                break

            default :
                this.setState({
                    eventToUpdate : Object.assign({}, this.state.eventToUpdate, {[target.id]: target.value})
                })
        }
    }

    handleChangeSelect = ({target}) => {
        this.setState({
            [target.name] : target.value
        })
    }



    openSnackbar = (messageSnackbar='HELLO ;)', variantSnackbar='error', verticalLocation='top', horizontalLocation='right') => {
        this.setState({
            openSnackbar : true,
            messageSnackbar ,
            variantSnackbar,
            verticalLocation,
            horizontalLocation
        })
    }

    handleChangePanel = panel => (event, expanded) => {
        this.setState({
            expanded: expanded ? panel : false
        });
    };

    closeSnackbar = () => {
        this.setState({
            openSnackbar : false
        })
    }

    handleChangeTabs = (event, value) => {
        this.setState({
            tab : value
        });
    }

    closeChoiceEventModal =  () => {
        this.setState({
            openChoiceEventModal : false
        })
    }

    handleClickSubmit = () => {
        if(this.state.eventToUpdate.repeat_type === 'punctual')
            this.handleSubmitUpdate('uniqueEvent')
        else
            this.setState({
                openChoiceEventModal : true
            })
    }

    handleAddUser = (user) => {
        switch (user.who){
            case 'customer':
                this.filteredCustomer.map( (customer, index) => {
                    if(user.id === customer.id){
                        this.filteredCustomer.splice(index, 1)
                    }
                })
                break
            case 'provider':
                this.filteredProvider.map( (provider, index) => {
                    if(user.id === provider.id){
                        this.filteredProvider.splice(index, 1)
                    }
                })
                break
            default :
                return
        }
        this.filteredUsers.push(user)
        this.setState({
            actionFilter : 'addUser'
        })
    }

    handleDropUser = (user) => {
        switch (user.who){
            case 'customer':
                this.filteredCustomer.push(user)
                break
            case 'provider':
                this.filteredProvider.push(user)
                break
            default :
                return
        }
        this.filteredUsers.map( (filteredUser, index) => {
            if(user.id === filteredUser.id){
                this.filteredUsers.splice(index, 1)
            }
        })
        this.setState({
            actionFilter : 'dropUser'
        })
    }

    render() {
        const {classes, open, onClose} = this.props,
              {openSnackbar, variantSnackbar, messageSnackbar,horizontalLocation, verticalLocation, eventToUpdate, tab, available, expanded, openChoiceEventModal, choices, selectedProvider}    = this.state,
              { title, allDay, start, startTime, end, endTime, address, comment, type} = eventToUpdate
        return (
                <Dialog
                fullScreen
                open={open}
                onClose={onClose}
                TransitionComponent={Transition}
                >
                    <Grid container style={{overflow : 'hidden'}}>
                            <Grid item xs={6} container style={{}}>
                                <Grid item xs={0.5}>
                                    <IconButton onClick={onClose} aria-label="Close"  style={{color : 'rgba(0,0,0,0.54)', marginTop:5, marginLeft: 10}}>
                                        <CloseIcon />
                                    </IconButton>
                                </Grid>
                                <Grid item xs={11} >
                                    <form className={classes.container +' '+ classes.margin} noValidate>
                                        <TextField
                                        type="text"
                                        value={title}
                                        id="title"
                                        onChange={this.handleChange}
                                        className={classes.title}
                                        InputProps={{
                                            disableUnderline: true,
                                            classes: {
                                                root: classes.textFieldRoot,
                                                input: classes.textFieldInput
                                            }
                                        }}
                                        />
                                    </form>
                                    <form className={classes.container} noValidate>
                                        <TextField
                                        type="date"
                                        value={start ? moment(start).format('YYYY-MM-DD') : ''}
                                        id="start"
                                        onChange={this.handleChange}
                                        className={classes.margin}
                                        style={{width: 145}}
                                        InputProps={{
                                            disableUnderline: true,
                                            classes: {
                                                root: classes.textFieldRoot,
                                                input: classes.textFieldInput
                                            }
                                        }}
                                        />
                                        {!allDay &&
                                            <TextField
                                            type="time"
                                            value={startTime ? startTime : ''}
                                            id="startTime"
                                            onChange={this.handleChange}
                                            className={classes.margin}
                                            style={{width: 85}}
                                            InputProps={{
                                                step: 300,
                                                disableUnderline: true,
                                                classes: {
                                                    root: classes.textFieldRoot,
                                                    input: classes.textFieldInput
                                                }
                                            }}
                                            />
                                        }
                                        <div className={classes.margin} style={{marginLeft: 0, marginRight: 0 }}>à</div>
                                        {!allDay &&
                                            <TextField
                                            type="time"
                                            value={endTime ? endTime : ''}
                                            id="endTime"
                                            onChange={this.handleChange}
                                            className={classes.margin}
                                            style={{width: 85}}
                                            InputProps={{
                                                step: 300,
                                                disableUnderline: true,
                                                classes: {
                                                    root: classes.textFieldRoot,
                                                    input: classes.textFieldInput
                                                }
                                            }}
                                            />
                                        }
                                        <TextField
                                        type="date"
                                        value={end ? moment(end).format('YYYY-MM-DD') : ''}
                                        id="end"
                                        onChange={this.handleChange}
                                        className={classes.margin}
                                        style={{width: 145}}
                                        InputProps={{
                                            disableUnderline: true,
                                            classes: {
                                                root: classes.textFieldRoot,
                                                input: classes.textFieldInput
                                            }
                                        }}
                                        />
                                    </form>
                                    <FormControl component="fieldset" className={classes.margin}>
                                        <FormControlLabel
                                        control={
                                            <Checkbox checked={allDay} id="allDay" onChange={this.handleChange} value={!allDay}/>
                                            }
                                        label="Toute la journée"
                                        />
                                    </FormControl>
                                </Grid>
                            </Grid>
                            <Grid item xs={6} container >
                                <Grid item xs >
                                    <Button color="primary" onClick={this.handleClickSubmit} className={classes.margin}>
                                    Enregistrer
                                    </Button>
                                </Grid>
                            </Grid>
                            <Grid item xs={6} container >
                                <div className={classes.root}>
                                    <Tabs
                                    value={tab}
                                    onChange={this.handleChangeTabs}
                                    classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator }}
                                    >
                                        <Tab
                                        disableRipple
                                        classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                        label="Détails de l'événement"
                                        />
                                        <Tab
                                        disableRipple
                                        classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                        label="Rechercher un horaire"
                                        />
                                    </Tabs>
                                    <Grid item xs={12} container className={classes.heightAndBorderRight} alignItems="flex-start" justify="flex-start">
                                    {tab === 0 &&
                                    <Grid item xs={12} container>
                                        { type !== 'availability' &&
                                        <Grid item xs={12} container>
                                            <Grid item xs={0.5} style={{display: 'flex', alignItems : 'center'}}>
                                                <PlaceIcon style={{marginLeft: 20, marginRight: 5}} />
                                            </Grid>
                                            <Grid item xs={11}>
                                                <form className={classes.container +' '+ classes.margin} noValidate>
                                                    <TextField
                                                    type="text"
                                                    value={address ? ''.concat(`${ address.address ? address.address+',' : ''}`,' ',`${ address.zipcode ? address.zipcode : ''}`,' ',`${ address.city ? address.city+',' : ''}`,' ', `${address.country ? address.country : ''}`) : ''}
                                                    id="location"
                                                    onChange={this.handleChange}
                                                    className={classes.title}
                                                    InputProps={{
                                                        disableUnderline: true,
                                                        classes: {
                                                            root: classes.textFieldRoot,
                                                            input: classes.textFieldInput
                                                        }
                                                    }}
                                                    />
                                                </form>
                                            </Grid>
                                        </Grid>
                                            }
                                        <Grid item xs={12} container>
                                            <Grid item xs={0.5} style={{display: 'flex', alignItems : 'center'}}>
                                                <WorkIcon style={{marginLeft: 20, marginRight: 5}} />
                                            </Grid>
                                            <Grid item xs={1} >
                                                <form className={classes.container +' '+ classes.margin} noValidate>
                                                    <FormControl className={classes.formControl}>
                                                        <Select
                                                        value={available}
                                                        onChange={this.handleChangeSelect}
                                                        inputProps={{
                                                            name: 'available',
                                                            id: 'availability'
                                                        }}
                                                        >
                                                            <MenuItem value={0}>Occupé</MenuItem>
                                                            <MenuItem value={1}>Disponible</MenuItem>
                                                        </Select>
                                                    </FormControl>
                                                </form>
                                            </Grid>
                                        </Grid>
                                        <Grid item xs={12} container>
                                            <Grid item xs={0.5} style={{display: 'flex'}}>
                                                <ShortTextIcon style={{marginLeft: 20, marginRight: 5, marginTop : 18}} />
                                            </Grid>
                                            <Grid item xs={11}>
                                                <form className={classes.container +' '+ classes.margin} noValidate>
                                                    <TextField
                                                    type="text"
                                                    value={comment ? comment : null}
                                                    placeholder={comment ? null : 'ajouter une description ;)'}
                                                    id="comment"
                                                    onChange={this.handleChange}
                                                    className={classes.title}
                                                    InputProps={{
                                                        disableUnderline: true,
                                                        multiline : true,
                                                        rows : 10,
                                                        rowsMax : 10,
                                                        classes: {
                                                            root: classes.textFieldRoot,
                                                            input: classes.textFieldInput
                                                        }
                                                    }}
                                                    />
                                                </form>
                                            </Grid>
                                        </Grid>
                                    </Grid >
                                        }
                                    {tab === 1 &&
                                    <Grid item xs={12} container>
                                    Rechercher un horaire
                                    </Grid>
                                        }
                                    </Grid>
                                </div>
                            </Grid>
                            <Grid item xs={3} container >
                                <div className={classes.root}>
                                    <Tabs
                                    value={0}
                                    classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator }}
                                    >
                                    </Tabs>
                                    <Grid item xs={12}  container className={classes.heightAndBorderRight} alignContent="flex-start">
                                        <ExpansionPanel expanded={true}  style={{width : '100%'}}>
                                            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                                <Typography className={classes.heading}>Liste Des invites</Typography>
                                            </ExpansionPanelSummary>
                                            <ExpansionPanelDetails style={{padding : 0, height: '61vh', overflow : 'auto'}}>
                                                <List component="div" style={{width : '100%'}}>
                                                {this.filteredUsers.map( user => (
                                                    <ListItem
                                                    key={user.id}
                                                    role={undefined}
                                                    dense
                                                    button
                                                    >
                                                        <ListItemText primary={user.firstname+' '+user.lastname} />
                                                        <ListItemSecondaryAction>
                                                            {selectedProvider && selectedProvider.user.id !== user.id &&
                                                                <IconButton aria-label="Suprimer" style={{color: 'red'}} onClick={() => this.handleDropUser(user)}>
                                                                    <CancelIcon />
                                                                </IconButton>
                                                            }

                                                        </ListItemSecondaryAction>
                                                    </ListItem>

                                                ))}
                                                </List>
                                            </ExpansionPanelDetails>
                                        </ExpansionPanel>
                                    </Grid>
                                </div>
                            </Grid>
                            <Grid item xs={3} container >
                                <div className={classes.root}>
                                    <Tabs
                                    classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator }}
                                    >
                                    </Tabs>
                                    <Grid item xs={12}  container className={classes.heightAndBorderRight} alignItems="flex-start" alignContent="flex-start">
                                        <ExpansionPanel expanded={expanded === 'panel2'} onChange={this.handleChangePanel('panel2')} style={{width : '100%'}}>
                                            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                                <Typography className={classes.heading}>Liste Des clients</Typography>
                                            </ExpansionPanelSummary>
                                            <ExpansionPanelDetails style={{padding : 0, height: '61vh', overflow : 'auto'}}>
                                                <List component="div" style={{width : '100%'}}>
                                                {this.filteredCustomer.map( customer => (
                                                    <ListItem
                                                    key={customer.id}
                                                    role={undefined}
                                                    dense
                                                    button
                                                    >
                                                        <ListItemText primary={customer.firstname+' '+customer.lastname} />
                                                        <ListItemSecondaryAction>
                                                            <IconButton aria-label="ajouter" style={{color: 'green'}} onClick={() => this.handleAddUser(customer)}>
                                                                <CancelIcon />
                                                            </IconButton>
                                                        </ListItemSecondaryAction>
                                                    </ListItem>

                                                ))}
                                                </List>
                                            </ExpansionPanelDetails>
                                        </ExpansionPanel>
                                        <ExpansionPanel expanded={expanded === 'panel3'} onChange={this.handleChangePanel('panel3')} style={{width : '100%'}}>
                                            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                                <Typography className={classes.heading}>Liste Des Prestataires</Typography>
                                            </ExpansionPanelSummary>
                                            <ExpansionPanelDetails style={{padding : 0, height: '61vh', overflow : 'auto'}}>
                                                <List component="div" style={{width : '100%'}}>
                                                {this.filteredProvider.map( provider => (
                                                    <ListItem
                                                    key={provider.id}
                                                    role={undefined}
                                                    dense
                                                    button
                                                    >
                                                        <ListItemText primary={provider.firstname+' '+provider.lastname} />
                                                        <ListItemSecondaryAction>
                                                            <IconButton aria-label="ajouter" style={{color: 'green'}} onClick={() => this.handleAddUser(provider)}>
                                                                <CancelIcon />
                                                            </IconButton>
                                                        </ListItemSecondaryAction>
                                                    </ListItem>

                                                ))}
                                                </List>
                                            </ExpansionPanelDetails>
                                        </ExpansionPanel>
                                    </Grid>
                                </div>
                            </Grid>
                        </Grid>
                    <ModalChoice open={openChoiceEventModal} title="mettre a jour" onClose={this.closeChoiceEventModal}  onSubmit={this.handleSubmitUpdate} choices={choices}/>
                    <Snackbar open={openSnackbar} variant={variantSnackbar} message={messageSnackbar} onClose={this.closeSnackbar} verticalLocation={verticalLocation} horizontalLocation={horizontalLocation} />
                </Dialog>
        );
    }
}

editEventModal.propTypes = {
    classes: PropTypes.object.isRequired
};

export default  compose(withStyles(styles), connect(mapStateToProps,mapDispatchToProps))(editEventModal)

