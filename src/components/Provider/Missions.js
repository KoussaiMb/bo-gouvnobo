import React,{Component} from 'react'
import {connect} from 'react-redux'
import {compose} from "redux"
import {
    withStyles,
    Grid
    } from '@material-ui/core'
import Calendar from './../stateless/Calendar'
const styles = theme => ({
    root: {
        flexGrow: 1
    }
})

class Missions extends Component {
    state = {
        open            : true
    }

    showModal(event){
        this.props.getSelectedEvent(event)
    }

    /*
     <Grid key="content" item lg={12} md={12} xs={12} style={{height:'76vh'}}>
     <Calendar />
     </Grid>
    * */

    render () {
        const { classes } = this.props
        return (

                    <Grid container className={classes.root}>
                        <Grid key="content" item lg={12} md={12} xs={12} style={{height:'76vh'}}>
                            <Calendar />
                        </Grid>
                    </Grid>
        );
    }
}


export default compose(withStyles(styles), connect(null,null))(Missions)