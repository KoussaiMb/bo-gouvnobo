import React,{Component} from 'react';
import PropTypes from 'prop-types';
import cookies from "js-cookie"
import {compose} from 'redux'
import {connect} from 'react-redux'
import moment from './../../utils/moment'
import { 
    withStyles,
    Card,
    CardActions,
    CardContent,
    Popover,
    Button,
    Icon,
    Typography,
    List,
    ListItem,
    ListItemText,
    ListItemIcon,
    Collapse,
    IconButton,
    } from '@material-ui/core';
import {
    Delete,
    Clear,
    AccessTime,
    Event,
    Place,
    ExpandLess,
    ExpandMore,
    Face,
    SupervisorAccount
    } from '@material-ui/icons';
import ModalChoice from './choiceEventModal'
import ModalEdit from './editEventModal'
import {getDeleteEventModal , getDeleteEvent} from '../../action/providerActions'
import {createMission} from '../../action/provider/createMission'
import ResponsiveDialog from './../stateless/ResponsiveDialog'
//TODO: split into several files

const styles = theme => ({
    flex : {
        display: 'flex',
        flexWrap : 'nowrap'
    },
    card: {
        maxWidth: 345,
        minWidth: 345
    },
    CardActions:{
        flexDirection : 'row',
        justifyContent : 'flex-end'
    },
    cardTitle:{
        width: '100%',
        marginLeft: '60px'
    },
    cardContent: {
        flexDirection : 'column ',
        padding:0,
        paddingBottom: '10px'
    },
    fab: {
        position: 'relative',
        bottom: theme.spacing.unit * 2.2,
        left: theme.spacing.unit * 2,
        color: '#fff'
    },
    popover: {
        //pointerEvents: 'none',
        marginLeft : theme.spacing.unit
    },
    nested: {
        paddingLeft: theme.spacing.unit * 4
    }
})

const mapStateToProps = state => {
    return {
        //selectedEvent       :   state.getSelectedEventReducer,
        //status              :   state.getInfoEventModalReducer
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getDeleteEvent        :    (apiToken, oldSubEvent, op, id)  => dispatch(getDeleteEvent(apiToken, oldSubEvent, op, id)),
        getDeleteEventModal   :    open  => dispatch(getDeleteEventModal(open)),
        createMission         :    (token, missionToCreate)  => dispatch(createMission(token, missionToCreate))
    }
}

class PopoverInfo extends Component {
    state={
        openCollapse          : false,
        openChoiceEventModal  : false,
        openEditEventModal    : false,
        openValidateEventModal: false,
        eventToDisplay        : {},
        provider              : {},
        choices               : ['uniqueEvent', 'allNextSubEvents', 'allSubEvents']
    }

    componentWillReceiveProps(nextProps){
        const {event , provider} = nextProps
        if(event)
            this.setState({
                eventToDisplay : event
            })
        if(provider)
            this.setState({
                provider
            })
    }


    handleClickCollapse = () => {
        this.setState({
            openCollapse : !this.state.openCollapse
        })
    }

    handleClickDelete = () => {
        let event = Object.assign({}, this.state.eventToDisplay)
        if(event.repeat_type === 'punctual')
            this.onSubmitDelete('uniqueEvent')
        else{
            let choices  = ['uniqueEvent', 'allNextSubEvents', 'allSubEvents']
            if(moment(event.start).isCustomAfter(event) || moment(event.start).isSame(moment(event.start_rec), 'day'))
                choices  = ['uniqueEvent', 'allSubEvents']
            this.setState({
                openChoiceEventModal : true,
                choices
            })
        }
    }

    handleClickEdit = () => {
        this.setState({
            openEditEventModal : true
        })
    }

    handleClickValidate = () => {
        this.setState({
            openValidateEventModal : true
        })
    }

    onSubmitDelete = (deleteMode) =>{
        this.CloseDeleteEventModal()
        this.props.onClose(deleteMode)
        const {eventToDisplay} = this.state
        let oldSubEvent = Object.assign({}, eventToDisplay, {
            start_rec   : eventToDisplay.start,
            end_rec     : eventToDisplay.end
        })
        delete oldSubEvent.event_users
        delete oldSubEvent.resourceId
        delete oldSubEvent.subEvents
        delete oldSubEvent.textColor
        delete oldSubEvent.anchorEl
        delete oldSubEvent.address
        delete oldSubEvent.color
        cookies.set("getDeleteEvent", true)
        this.props.getDeleteEvent(cookies.get("token"), oldSubEvent, deleteMode, eventToDisplay.id)
    }

    onSubmitUpdate = () =>{
        this.props.onClose()
        this.CloseEditEventModal()
    }

    onSubmitValidate = () => {
        this.onCloseValidateModal()
        this.props.onClose()
        const {eventToDisplay, provider} = this.state,
              missionToValidate = {
                  event_id     : eventToDisplay.id,
                  provider_id  : provider.id,
                  address_id   : eventToDisplay.address_id,
                  start        : moment(`${eventToDisplay.start}T${eventToDisplay.startTime}`).format('YYYY-MM-DDTHH:mm:ss'),
                  end          : moment(`${eventToDisplay.end}T${eventToDisplay.endTime}`).format('YYYY-MM-DDTHH:mm:ss')
              }
        eventToDisplay.event_users.map( eventUser => {
            if(eventUser.user.who === 'customer'){
                missionToValidate.user_id = eventUser.user.id
                return 0
            }
        })
        this.props.createMission(cookies.get('token'), missionToValidate)
        cookies.set('createMission', true)
    }

    CloseDeleteEventModal =  () => {
        this.setState({
            openChoiceEventModal : false
        })
    }

    CloseEditEventModal = () => {
        this.setState({
            openEditEventModal : false
        })
    }

    onCloseValidateModal = () => {
        this.setState({
            openValidateEventModal : false
        })
    }

    editable = (date) => {
        if(date)
            return moment(date._i).isAfter(moment().add(1, 'hours'), 'minute')
        return false
    }

    getMargin = (date) => {
        if(date)
            return this.editable(date) || moment(date._i).isBefore(moment(), 'minute')
        return false
    }

    render(){
        const {openCollapse, openChoiceEventModal, openEditEventModal, eventToDisplay, choices, openValidateEventModal} = this.state
        const {anchorEl, color, textColor, title, resourceId, resourceTitle, start, startTime, endTime, address, event_users, validated} = eventToDisplay
        const { classes, open } = this.props;
        return (
            <Popover
            className={classes.popover}
            open={open}
            anchorEl={anchorEl}
            onClose={this.props.onClose}
            anchorOrigin={{
                vertical: 'center',
                horizontal: 'right'
            }}
            transformOrigin={{
                vertical: 'center',
                horizontal: 'left'
            }}
            >
                <Card className={classes.card}>
                    <CardContent className={classes.cardContent} style={{backgroundColor: color ? color : '#039be5'}}>
                        <CardActions className={classes.flex +' '+ classes.CardActions}>
                            <IconButton  style={{color : textColor ? textColor : '#fff'}} aria-label="Delete" onClick={this.handleClickDelete}>
                                <Delete />
                            </IconButton>
                            <IconButton  style={{color : textColor ? textColor : '#fff'}} aria-label="Close" onClick={this.props.onClose}>
                                <Clear />
                            </IconButton>
                        </CardActions>
                        <Typography gutterBottom variant="headline" component="h2" className={classes.cardTitle} style={{color : textColor ? textColor : '#fff'}}>
                        {title}
                        </Typography>
                    </CardContent>
                     {this.editable(eventToDisplay._start) &&
                     <Button variant="fab" mini aria-label="Edit" className={classes.fab} style={{ backgroundColor : color, color : textColor ? textColor : '#fff'}} onClick={this.handleClickEdit}>
                         <Icon>edit_icon</Icon>
                     </Button>
                     }
                    {validated === false &&
                    <Button variant="fab" mini aria-label="validate" className={classes.fab} style={{ backgroundColor : color, color : textColor ? textColor : '#fff', marginLeft : '5px'}} onClick={this.handleClickValidate}>
                        <Icon>done</Icon>
                    </Button>}
                    <CardContent className={classes.cardContent} style={{marginTop: this.getMargin(eventToDisplay._start) ? -25 : 0, paddingBottom: 0}}>
                        <List dense={true} >
                            {resourceId &&
                                <ListItem>
                                    <Event />
                                    <ListItemText primary={resourceTitle} />
                                </ListItem>
                            }
                            {start      &&
                                <ListItem>
                                    <ListItemIcon><AccessTime /></ListItemIcon>
                                    <ListItemText primary={moment(start).format("dddd, DD MMMM")} secondary={startTime+' à '+endTime} />
                                </ListItem>
                            }
                            {address   &&
                                <ListItem>
                                    <ListItemIcon><Place /></ListItemIcon>
                                    <ListItemText primary={address.address} secondary={address.zipcode+','+address.city+'-'+ address.country}/>
                                </ListItem>
                            }
                            {event_users      &&
                                <React.Fragment>
                                    <ListItem button onClick={this.handleClickCollapse}>
                                        <ListItemIcon>
                                            <SupervisorAccount />
                                        </ListItemIcon>
                                        <ListItemText inset primary="Participants" />
                                        {openCollapse ? <ExpandLess /> : <ExpandMore />}
                                    </ListItem>
                                    <Collapse in={openCollapse} timeout="auto" unmountOnExit>
                                        <List component="div" disablePadding>
                                            {event_users.map( user => <ListItem  className={classes.nested}>
                                                                    <ListItemIcon>
                                                                        <Face />
                                                                    </ListItemIcon>
                                                                    <ListItemText inset primary={user.user.firstname +' '+ user.user.lastname} secondary={user.user.email}/>
                                                                </ListItem>
                                            )}
                                        </List>
                                    </Collapse>
                                </React.Fragment>
                            }
                        </List>
                    </CardContent>
                </Card>
                <ModalChoice open={openChoiceEventModal} title="Supprimer l'événement" onClose={this.CloseDeleteEventModal}  onSubmit={this.onSubmitDelete} event={eventToDisplay} choices={choices}/>
                <ModalEdit   open={openEditEventModal}   onClose={this.CloseEditEventModal}    onSubmit={this.onSubmitUpdate} event={eventToDisplay}/>
                <ResponsiveDialog
                    open={openValidateEventModal}
                    title={'VALIDATION DE MISSION'}
                    onClose={this.onCloseValidateModal}
                    message={'Voulez Vous Validez Cette Mission ?'}
                    Agree={
                            {
                                text : 'VALIDER',
                                onClick : this.onSubmitValidate
                            }
                    }
                    Disagree={
                            {
                                text : 'ANNULER',
                                onClick : this.onCloseValidateModal
                            }
                    }
                />
            </Popover>
        )
    }
}

PopoverInfo.propTypes = {
    classes: PropTypes.object.isRequired
}

export default  compose(withStyles(styles), connect(mapStateToProps,mapDispatchToProps))(PopoverInfo)