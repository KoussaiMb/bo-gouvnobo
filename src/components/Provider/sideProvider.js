import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux'
import cookies from "js-cookie";
import {compose} from "redux";
import {
    withStyles,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    AppBar,
    Tabs,
    FormControl,
    Input,
    InputLabel,

    } from '@material-ui/core';
import { Person, Search } from "@material-ui/icons";
import {getSelectedProvider, getWeekMissions, getProviderList, getTodayMissionsProvider, getMonthMissionsProvider} from '../../action/providerActions'

//TODO: remove getWeekMissions getTodayMissionsProvider && xxxxxxReducer

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper
    },
    content:{
        height:'80vh',
        overflow: 'auto'
    }
})

const mapStateToProps = state => {
    return {
        providerList    : state.getProviderListReducer
    }
}


const mapDispatchToProps = dispatch => {
    return {
        getProviderList             :    apiToken => dispatch(getProviderList(apiToken)),
        //getWeekMissions             :   (apiToken, providerId) => dispatch(getWeekMissions(apiToken, providerId)),
        getSelectedProvider         :    providerId => dispatch(getSelectedProvider( providerId)),
        //getTodayMissionsProvider    :   (apiToken, providerId) => dispatch(getTodayMissionsProvider(apiToken, providerId)),
        getMonthMissionsProvider    :   (apiToken, month) => dispatch(getMonthMissionsProvider(apiToken, month))
    }
}

class sideProvider extends React.Component {
    state = {
        data : [],
        fillData : false,
        searchLength : 0,
        currentProvider: {},
        search: ""
    }
    componentWillMount() {
        //this.props.getProviderList(cookies.get("token"));
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.providerList.providerList && !this.state.fillData){
            const {data} = nextProps.providerList.providerList
            this.setState({
                data,
                fillData : true
            })
        }
    }

    handleClick(provider) {
    //TODO:force the handlesearch
        //cookies.set('selectedProvider',provider)
        //this.setState({search: provider.user.firstname, resetInfo: true})
        //this.props.onClick(provider)
        //this.props.getWeekMission(cookies.get("token"),provider.id)
        //this.props.getWeekMissions(cookies.get('token'),provider.id)
        //this.props.getTodayMissionsProvider(cookies.get('token'),provider.id)
        this.props.getSelectedProvider(provider)
        cookies.set('getSelectedProvider', true)
        //this.props.getMonthMissionsProvider(cookies.get("token"), moment().month())
    }

    handleSearch = (search) => {
        this.setState({
            searchLength : search.target.value.length,
            search: search.target.value,
            data : this.props.providerList.providerList.data.filter( item => {
                let providertName = item.user.firstname + ' ' + item.user.lastname
                providertName = providertName.toLowerCase()
                let userInput = search.target.value.toLowerCase()
                return (providertName.includes(userInput))
            })
        })
    }

    render = () => {
        const { classes } = this.props
        const {data, search} = this.state
        return (
        <div className={classes.root}>
            <AppBar position="static" color="default">
                <Tabs indicatorColor="primary" textColor="primary">
                    <ListItem component="div">
                        <ListItemIcon>
                            <Search />
                        </ListItemIcon>
                        <FormControl>
                            <InputLabel htmlFor="search">Search</InputLabel>
                            <Input id="search" value={search} onChange={e => this.handleSearch(e)} />
                        </FormControl>
                    </ListItem>
                </Tabs>
            </AppBar>
            <div className={classes.content}>
                <List component="div">
                {data && data.map( provider => {
                    return(
                        <ListItem button onClick={() => this.handleClick(provider)} key={provider.id}>
                            <ListItemIcon>
                                <Person />
                            </ListItemIcon>
                            <ListItemText inset primary={provider.user.firstname+' '+provider.user.lastname} />
                        </ListItem>
                    )
                } )}
                </List>
            </div>
        </div>

        );
    }
}

sideProvider.propTypes = {
    classes: PropTypes.object.isRequired
};

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(sideProvider)