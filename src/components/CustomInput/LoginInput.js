import React, { Component } from 'react';
import {
    Grid,
    TextField
    } from '@material-ui/core';

export default class LoginInput extends Component {
  render() {
    return (
        <Grid container spacing={8} alignItems="flex-end">
          <Grid item>
            { this.props.iconComponent }
          </Grid>
          <Grid item>
            <TextField onChange={ev => this.props.onChangeCallback(ev, this.props.dataName)}
                       autoFocus={this.props.autofocus} type={this.props.fieldType}
                       label={this.props.label}/>
          </Grid>
        </Grid>);
  }
}