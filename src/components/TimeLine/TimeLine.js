import React, {Component} from "react";
import { connect } from "react-redux";
import moment from 'moment';
import cookies from 'js-cookie';
import {
    Card,
    CardContent,
    Typography,
    CardActions,
    Button,

    } from '@material-ui/core';
import Modal from 'react-responsive-modal';
import Timeline from 'react-visjs-timeline';
import MissionModal from './MissionModal';
import {getTodayMissions} from '../../action/getTodayMissions';
import {getMinutes, getHours, searchInArrayById, cutLongString} from '../../utils/utilistFunctions';
import '../../assets/css/timeline.css';

const timelineData = {
  options: {
    start: moment().hours('7').minutes('0'),
    end: moment().hours('19').minutes('0'),
    zoomable: false,
    moveable: false
  }
};

const mapStateToProps = (state) => {
  return {
    todayMissionsData: state.getTodayMissions
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getTodayMissions: (apiToken) =>
      dispatch(getTodayMissions(apiToken))
  };
};


class TimeLine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      agendaId: 0
    }
  }

  componentWillMount() {
    if (!this.props.todayMissionsData.todayMissionsData)
      this.props.getTodayMissions(cookies.get("token"));
  }

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  createTimeline() {
    timelineData.items = [];
    timelineData.groups = [];
    this.props.todayMissionsData.todayMissionsData.data.map((mission) => {
      timelineData.items.push({
        id: mission.id,
        content: mission.address.address,
        start: moment().hours(getHours(mission.start)).minutes(getMinutes(mission.start)),
        end: moment().hours(getHours(mission.end)).minutes(getMinutes(mission.end)),
        group: mission.provider.user_id,
        title: mission.start +  ' - ' + mission.end
      });
      if (!searchInArrayById(timelineData.groups, mission.provider.user_id)) {
        timelineData.groups.push({id: mission.provider.user_id,
          content: cutLongString(mission.provider.user.firstname, 14) + ' '
          + cutLongString(mission.provider.user.lastname, 12)});
      }
    });
    return (true);
  }

  handleTimelineClick(event) {
    if (event.item) {
      this.setState({
        agendaId: event.item
      });
      this.onOpenModal();
    }
  }

  render() {
    const { open } = this.state;
    return (
    <div>
      <Card>
        <CardContent>
          <Typography>
            <span style={{fontWeight: 'bold', fontSize: '18px', color: "#e6604e"}}>/</span> Aujourd'hui
          </Typography>
          { this.props.todayMissionsData && this.props.todayMissionsData.todayMissionsData
          && this.props.todayMissionsData.todayMissionsData.data
          && this.createTimeline()
          && <Timeline {...timelineData} clickHandler={this.handleTimelineClick.bind(this)}
          /> }
          { this.props.todayMissionsData.todayMissionsData && this.props.todayMissionsData.todayMissionsData.message === "No mission today" &&
          <p>Pas de mission aujourd'hui</p> }
        </CardContent>
        <CardActions>
          <Button onClick={this.props.expandTimeline} size="small">{this.props.size === 6 ? "Agrandir" : "Réduire"}</Button>
        </CardActions>
      </Card>
      <Modal open={open} onClose={this.onCloseModal} center>
        <MissionModal agendaId={this.state.agendaId}/>
      </Modal>
    </div>
    );
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(TimeLine);