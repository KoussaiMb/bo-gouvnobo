import React, {Component} from "react";
import { connect } from "react-redux";
import cookies from 'js-cookie';
import { Grid } from "@material-ui/core";
import { PersonPinCircle, Schedule, Home, Face } from "@material-ui/icons";
import {getAgendaEntry} from '../../action/getAgendaEntry';
import {cutSeconds} from '../../utils/utilistFunctions';

const mapStateToProps = (state) => {
  return {
    agendaEntryData: state.getAgendaEntry
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAgendaEntry: (apiToken, id) =>
      dispatch(getAgendaEntry(apiToken, id))
  };
};

class MissionModal extends Component {
  constructor(props) {
    super(props);
    console.log(this.props.agendaId);
  }

  componentWillMount() {
    this.props.getAgendaEntry(cookies.get("token"), this.props.agendaId);
  }

  render() {
    let data;
    if (this.props.agendaEntryData.agendaEntryData && this.props.agendaEntryData.agendaEntryData.data)
      data = this.props.agendaEntryData.agendaEntryData.data;
    console.log(this.props.agendaEntryData);
    return(
      <div>
        { data &&
        <Grid container spacing={0} style={{width: '60vw'}} alignItems="center">
          <Grid item xs={3} lg={1}>
            <Home style={{fontSize: "38px"}}/>
          </Grid>
          <Grid item xs={9} lg={11}>
            <p style={{fontSize: '20px'}}>{data.address.address}, {data.address.zipcode}</p>
          </Grid>
        </Grid>
        }
        { data &&
        <Grid container spacing={0} style={{width: '60vw'}} alignItems="center">
          <Grid item xs={3} lg={1}>
            <Face style={{fontSize: "38px"}}/>
          </Grid>
          <Grid item xs={9} lg={11}>
            <p style={{fontSize: '20px'}}>{data.customer.firstname} {data.customer.lastname}</p>
          </Grid>
        </Grid>
        }
        { data &&
        <Grid container spacing={0} style={{width: '60vw'}} alignItems="center">
          <Grid item xs={3} lg={1}>
            <PersonPinCircle style={{fontSize: "38px"}}/>
          </Grid>
          <Grid item xs={9} lg={11}>
            <p style={{fontSize: '20px'}}>{data.provider.user.firstname} {data.provider.user.lastname}</p>
          </Grid>
        </Grid>
        }
        { data &&
        <Grid container spacing={0} style={{width: '60vw'}} alignItems="center">
          <Grid item xs={3} lg={1}>
            <Schedule style={{fontSize: "38px"}}/>
          </Grid>
          <Grid item xs={9} lg={11}>
            <p style={{fontSize: '20px'}}>{cutSeconds(data.start)} - {cutSeconds(data.end)}</p>
          </Grid>
        </Grid>
        }
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MissionModal);