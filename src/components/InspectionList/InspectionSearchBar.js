import React, { Component } from "react";
import {connect} from "react-redux";
import cookies from "js-cookie";
import moment from "moment";
import {
    Grid,
    TextField,
    Typography,
    Button
    } from "@material-ui/core";
import {searchInspections} from "../../action/inspections";

const mapDispatchToProps = (dispatch) => {
  return {
    searchInspection: (apiToken, clientName, providerName, date) =>
        dispatch(searchInspections(apiToken, clientName, providerName, date))
  };
};

class InspectionSearchBar extends Component {
  constructor(props) {
    super(props);
    this.inspData = [];
    this.inspData['clientName'] = null;
    this.inspData['providerName'] = null;
    this.inspData['date'] = null;
  }

  handleSearch(event) {

    if (this.inspData['clientName'] || this.inspData['providerName'] || this.inspData['date'])
      this.props.searchInspection(cookies.get('token'), this.inspData['clientName'],
          this.inspData['providerName'], this.inspData['date']);
    event.preventDefault();
  }

  handleChange(event, field) {
    this.inspData[field] = event.target.value;
  }

  render() {
    return (
        <form onSubmit={(event) => this.handleSearch(event)}>
          <Grid container justify="center" alignItems="center" style={{paddingBottom: "2vh"}}>
            <Grid item>
              <Typography variant="title">
                Chercher une inspection
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={16} justify="center" alignItems="center">
            <Grid item>
              <TextField
                  onChange={event => this.handleChange(event, 'clientName')}
                  label="Nom du client"
                  type="text"
                  InputLabelProps={{
                    shrink: true,
                  }}/>
            </Grid>
            <Grid item>
              <TextField
                  onChange={event => this.handleChange(event, 'providerName')}
                  label="Nom du prestataire"
                  type="text"
                  InputLabelProps={{
                    shrink: true,
                  }}/>
            </Grid>
            <Grid item>
              <TextField
                  onChange={event => this.handleChange(event, 'date')}
                  label="Date"
                  type="date"
                  InputLabelProps={{
                    shrink: true,
                  }}/>
            </Grid>
            <Grid item>
              <Button type="submit" variant="contained" color="secondary"
                      style={{marginTop: "1rem", color: 'white'}}>
                Chercher
              </Button>
            </Grid>
          </Grid>
        </form>);
  }
}

export default connect(null, mapDispatchToProps)(InspectionSearchBar);