import React, { Component } from 'react';
import {connect} from "react-redux";
import cookies from "js-cookie";
import {
    List,
    ListItem,
    ListItemText,
    ListItemIcon,
    CircularProgress,
    Grid,
    Typography,
    Button
    } from '@material-ui/core'
import {Refresh} from "@material-ui/icons";
import InspectionItem from "./InspectionItem";
import {getInspections} from "../../action/inspections";
import {getInspDetails} from "../../action/inspectionDetails";

const mapStateToProps = (state) => {
  return {
    inspectionReducer: state.inspectionsReducer
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getData: (apiToken, page) => dispatch(getInspections(apiToken, page)),
    changeInspDetails: (apiToken, inspId) => dispatch(getInspDetails(apiToken, inspId))
  };
};

class InspectionList extends Component {
  constructor(props) {
    super(props);
    this.dataPage = 1;
  }

  componentWillMount() {
    this.props.getData(cookies.get('token'), this.dataPage);
  }

  handleInspSelect(id) {
    this.props.changeInspDetails(cookies.get('token'), id);
  }

  handleRefresh() {
    const inspectionsData = this.props.inspectionReducer.inspectionsData;
    if (inspectionsData && inspectionsData.totalPages <= this.dataPage)
      return;
    this.dataPage++;
    this.props.getData(cookies.get('token'), this.dataPage);
  }

  handleReset() {
    this.dataPage = 1;
    this.props.getData(cookies.get('token'), this.dataPage);
  }

  render() {
    const inspectionsData = this.props.inspectionReducer.inspectionsData;
     const reducer = this.props.inspectionReducer;
     if (reducer.error !== "")
       return (
           <Grid container justify="center" alignItems="center">
             <Typography variant="title">Aucune inspection n'a été trouvée</Typography>
           </Grid>);
    if (reducer.isLoading || inspectionsData === null || inspectionsData.data === null)
      return (
          <Grid container justify="center" alignItems="center">
            <CircularProgress color="primary" size={50} />
          </Grid>);
    return (
        <List component="nav" style={{maxHeigth: '20vh'}}>
          <ListItem button onClick={this.handleReset.bind(this)} style={{textAlign: "center"}}>
            <ListItemText primary="Réinitialiser"/>
          </ListItem>
          { inspectionsData.data.map(inspection => {
            return (inspection) ? <InspectionItem key={inspection.id} inspection={inspection}
                                                  onClick={this.handleInspSelect.bind(this)}/> : null;})
          }
          { inspectionsData.totalPages > this.dataPage &&
          <ListItem button onClick={this.handleRefresh.bind(this)} style={{textAlign: "center"}}>
            <ListItemIcon>
              <Refresh color="primary"/>
            </ListItemIcon>
            <ListItemText primary="Appuyer pour charger plus d'inspections"/>
          </ListItem> }
          { reducer.isLoading &&
          <ListItem>
              <CircularProgress color="primary" size={50} />
          </ListItem> }
        </List>);
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(InspectionList);
