import React, { Component } from 'react';
import moment from "moment";
import {
    ListItem,
    ListItemText
        } from '@material-ui/core'
import {
    Star,
    Event,
    AssignmentInd,
    Person
        } from '@material-ui/icons'

export default class InspectionItem extends Component {
  shouldComponentUpdate() {
    return false;
  }

  render() {
    const inspection = this.props.inspection;
    return (
        <ListItem button onClick={() => this.props.onClick(inspection.id)} divider>
          <Star color="secondary"/>
          <ListItemText primary={inspection.average_rate + '%'}/>
          <Event color="secondary"/>
          <ListItemText primary={moment(inspection.date).locale('fr').format("LLL")}/>
          <Person color="secondary"/>
          <ListItemText primary={inspection.mission.customer.firstname.charAt(0)
          + ". " + inspection.mission.customer.lastname}/>
          <AssignmentInd color="secondary"/>
          <ListItemText primary={inspection.mission.provider.user.firstname + " " + inspection.mission.provider.user.lastname}/>
        </ListItem>
    );
  }
}