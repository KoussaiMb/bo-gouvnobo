import React, { Component } from 'react';
import {connect} from "react-redux";
import moment from "moment";
import {
    Grid,
    Typography,
    Card,
    Fade
        } from '@material-ui/core'
import {
    Event,
    Grade
        } from '@material-ui/icons'
import RatesTable from "./RatesTable";

const mapStateToProps = (state) => {
  return {
    inspDetailsReducer: state.inspectionDetailsReducer
  };
};

class InspectionDetails extends Component {
  getStars(rate) {
    if (rate === 0)
      return ("0/3");
    let stars = [], nbStar;
    for (nbStar = 0; nbStar < rate; nbStar++)
      stars.push(<Grade key={nbStar} color="primary"/>);
    return stars;
  }

  render() {
    const reducer = this.props.inspDetailsReducer;
    if (reducer.inspDetailsData === null || reducer.error !== "")
      return (
          <Grid container direction="row" justify="center" alignItems="center">
            <Typography variant="display1">Aucune inspection sélectionnée</Typography>
          </Grid>);
    return (
        <Fade in>
          <Card style={{height: "100%", width: "100%"}}>
            <Grid container direction="column" spacing={16} style={{padding: "2vh"}}>
              <Grid container spacing={16}>
                <Grid item>
                  <Event color="primary"/>
                </Grid>
                <Grid item>
                  {moment(reducer.inspDetailsData.date).locale('fr').format('LLL')}
                </Grid>
                <Grid item>
                    {"Commentaire : " + ((reducer.inspDetailsData.comments !== "") ? reducer.inspDetailsData.comments : "Aucun") }
                </Grid>
                <Grid item>
                  {"Grooming : "}
                </Grid>
                <Grid item>
                  {this.getStars(reducer.inspDetailsData.grooming)}
                </Grid>
              </Grid>
              <RatesTable rates={reducer.inspDetailsData.rates}/>
            </Grid>
          </Card>
        </Fade>
    );
  }
}

export default connect(mapStateToProps, null)(InspectionDetails);