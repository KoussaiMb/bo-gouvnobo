import React, {Component} from "react";
import {
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,

    } from '@material-ui/core'
import {Grade} from "@material-ui/icons";

export default class RatesTable extends Component {
  getStars(rate) {
    if (rate === 0)
      return ("Non effectuée");
    let stars = [], nbStar;
    for (nbStar = 0; nbStar < rate; nbStar++)
      stars.push(<Grade key={nbStar} color="primary"/>);
    return stars;
  }

  render() {
    return (
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Tâche</TableCell>
              <TableCell numeric>Note</TableCell>
              <TableCell numeric>Lieu</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.props.rates.map(rate =>
                <TableRow key={rate.id}>
                  <TableCell component="th" scope="row">
                    {rate.task.description}
                  </TableCell>
                  <TableCell numeric>
                    {this.getStars(rate.rate)}
                  </TableCell>
                  <TableCell numeric>{rate.task.place.value}</TableCell>
                </TableRow>
            )}
          </TableBody>
        </Table>
    );
  }
}