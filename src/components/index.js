import Sidebar from "./Sidebar/Sidebar.js";
import Header from "./Header/Header.js";
import CustomInput from "./CustomInput/CustomInput.js";
import IconButton from './CustomButton/IconButton.js';
import Map from './Map/Maps.js';
import AlertCard from './Card/AlertCard.js';
import TimeLine from './TimeLine/TimeLine.js';
import InspectionList from './InspectionList/InspectionList.js';
import InspectionDetails from './InspectionDetails/InspectionDetails.js';
import InspectionSearchBar from './InspectionList/InspectionSearchBar.js';
import SideListCustomer from './ListCustomer/SideListCustomer.js';
import Provider from './Provider/';

export {
    Sidebar,
    Header,
    CustomInput,
    IconButton,
    Map,
    AlertCard,
    TimeLine,
    InspectionList,
    InspectionDetails,
    InspectionSearchBar,
    SideListCustomer,
    Provider
    };
