import React,{Component} from 'react'
import {compose} from 'redux'
import {connect} from 'react-redux'
import cookies from "js-cookie"
import moment from 'moment'
import $ from 'jquery'
import 'fullcalendar'

//TODO: remove getAllCustomers getSelectedEvent getInfoEventModal && xxxxxxReducer
import {
    withStyles,
    Paper
    } from '@material-ui/core'
import Fullscreen from "react-full-screen";
import Modal from 'react-responsive-modal';
import Side from '../Provider/sideCalendar'
import TitleSide from '../Provider/TitleSide'
import ModalInfo from '../Provider/infoEventModal'
import CreateModal from '../Provider/CreatEventModal'
import ModalChoice from '../Provider/choiceEventModal'
import Snackbar from '../stateless/Snackbars';
import {getAllCustomers} from '../../action/getAllCustomers';
import {resetCreateMissionStatus} from '../../action/provider/createMission'
import {getSelectedEvent, getSelectedDate, getEventsByType, getInfoEventModal, getResetDeleteEventStatus, getResetUpdateEventStatus, getResetCreateEventStatus, getUpdateEvent} from '../../action/providerActions'



const mapDispatchToProps = dispatch => {
    return {
        getSelectedDate         :    date  => dispatch(getSelectedDate(date)),
        getUpdateEvent          :    (apiToken, oldSubEvent,eventToUpdate, op, id)  => dispatch(getUpdateEvent(apiToken, oldSubEvent,eventToUpdate, op, id)),
        resetDeleteEventStatus  :    () => dispatch(getResetDeleteEventStatus()),
        resetUpdateEventStatus  :    () => dispatch(getResetUpdateEventStatus()),
        resetCreateEventStatus  :    () => dispatch(getResetCreateEventStatus()),
        resetCreateMissionStatus:    () => dispatch(resetCreateMissionStatus()),
        getEventsByType         :    (apiToken, startDate, endDate, types, users) => dispatch(getEventsByType(apiToken, startDate, endDate, types, users)),
        getAllEvents            :    (apiToken, startDate, endDate) => dispatch(getEventsByType(apiToken, startDate, endDate, '')),
        //getSelectedEvent      :    event => dispatch(getSelectedEvent(event)),
        //getAllCustomers       :    apiToken => dispatch(getAllCustomers(apiToken))
        //getInfoEventModal     :    open   => dispatch(getInfoEventModal(open))
    }
}

const mapStateToProps = state => {
    return {
        eventsByType       :   state.getEventsByTypeReducer,
        getDeleteEvent     :   state.getDeleteEventReducer,
        UpdatedEvent       :   state.getUpdateEventReducer,
        createEvent        :   state.createEventReducer,
        selectedProvider   :   state.getSelectedProviderReducer,
        provider           :   state.getSelectedProviderReducer,
        validatedMission   :   state.createMissionReducer
        //customers          :   state.getAllCustomers
    }
}

const styles = theme => ({
    paper: {
        position: 'relative',
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5]
    }
})

const populateEvents = (arraySubEvents, arrayEvents) => {
    let _arrayEvents    = arrayEvents
    if(typeof (arrayEvents) === 'undefined')
        _arrayEvents = []
    arraySubEvents.map( e => _arrayEvents.push(e))
    return _arrayEvents
}
const populateMapSource = (event, mapSource) => {
    let _mapSource = mapSource
    return Object.assign({}, _mapSource,{ [event.id] : event})
}


const populateListEvents = (event, type) => {
    return Object.assign({}, type, {
        events      : populateEvents(event.subEvents, type.events),
        mapSource   : populateMapSource(event, type.mapSource)
    })
}

class Calendar extends Component {
    state={
        openInfoModal           : false,
        openChoiceEventModal    : false,
        openCreateEventModal    : false,
        isFull                  : false,
        error                   : false,
        deleteMode              : null,
        selectedDate            : null,
        start                   : moment().day(0),
        end                     : moment().day(6),
        selectedEvent           : null,
        createdEvent            : null,
        status                  : true,
        provider                : null,
        listCustomer            : [],
        listProvider            : [],
        choices                 : ['uniqueEvent', 'allNextSubEvents', 'allSubEvents'],
        appointment             : {id : 'appointment'     , color  : '#3EFF00', textColor: '#fff'},
        benefit                 : {id : 'benefit'         , color  : '#FF9000', textColor: '#fff'},
        firstAppointment        : {id : 'firstAppointment', color  : '#1600FF', textColor: '#fff'},
        other                   : {id : 'other'           , color  : '#FF0900', textColor: '#fff'},
        availability            : {id : 'availability'    , color  : '#BB00FF', textColor: '#fff'}
        //test             : {id : 'test'            , color  : 'gray'   , textColor: '#fff', events:[{ title : 'halim', start : '2018-08-23', end : '2018-08-24'}]}
    }

    componentWillReceiveProps(nextProps){
        const {eventsByType, getDeleteEvent, UpdatedEvent, createEvent, validatedMission} = nextProps,
              {selectedEvent, createdEvent, start, end}  = this.state,
              {provider} = nextProps.provider
        if(eventsByType.events && cookies.get("getEventsByType") === 'true'){
            cookies.set("fetchSources", true)
            cookies.set("getEventsByType", false)
            const {events} = eventsByType
            let appointment     ={id : 'appointment'     , color  : '#3EFF00', textColor: '#fff'},
                benefit         ={id : 'benefit'         , color  : '#FF9000', textColor: '#fff'},
                firstAppointment={id : 'firstAppointment', color  : '#1600FF', textColor: '#fff'},
                other           ={id : 'other'           , color  : '#FF0900', textColor: '#fff'},
                availability    ={id : 'availability'    , color  : '#BB00FF', textColor: '#fff'}
            events.map( event => {
                switch (event.type){
                    case 'appointment'      :
                        appointment = populateListEvents(event, appointment)
                        this.setState({appointment})
                        break
                    case 'benefit'          :
                        benefit = populateListEvents(event, benefit)
                        this.setState({benefit})
                        break
                    case 'firstAppointment' :
                        firstAppointment = populateListEvents(event, firstAppointment)
                        this.setState({firstAppointment})
                        break
                    case 'other'            :
                        other = populateListEvents(event, other)
                        this.setState({other})
                        break
                    case 'availability'     :
                        availability = populateListEvents(event, availability)
                        this.setState({availability})
                        break
                    default :
                        break
                }
            })
        }

        if(getDeleteEvent.status && cookies.get("getDeleteEvent") === 'true'){
            console.log('getDeleteEvent')

            cookies.set("getDeleteEvent", false)
            const {status} = getDeleteEvent
            const {type}   = selectedEvent
            if(status === 200 ){
                this.props.resetDeleteEventStatus()
                this.setState({
                    [type] : Object.assign({}, this.state[type], {
                        events    : [],
                        mapSource : {}
                    })
                })
                this.props.getEventsByType(cookies.get("token"),moment(start).format('YYYY-MM-DD'), moment(end).format('YYYY-MM-DD'), `${type}`, `users=${provider.user.id}`)
                cookies.set("getEventsByType", true)
            }
        }

        if(UpdatedEvent.status && cookies.get("getUpdateEvent") === 'true'){
            cookies.set("getUpdateEvent", false)
            cookies.set('getUpdateEventUser', false)
            const {status} = UpdatedEvent
            const {type}   = selectedEvent
            if(status === 200 ){
                this.props.resetUpdateEventStatus()
                this.setState({
                    [type] : Object.assign({}, this.state[type], {
                        events    : [],
                        mapSource : {}
                    })
                })
                this.props.getEventsByType(cookies.get("token"),moment(start).format('YYYY-MM-DD'), moment(end).format('YYYY-MM-DD'), `types=${type}`, `users=${provider.user.id}`)
                cookies.set("getEventsByType", true)
            }
        }

        if(createEvent.status && cookies.get("createEvent") === 'true'){
            console.log('createEvent')
            cookies.set("createEvent", false)
            const {status} = createEvent
            const {type}   = createdEvent
            if(status === 200 ){
                this.props.resetCreateEventStatus()
                this.setState({
                    [type] : Object.assign({}, this.state[type], {events    : [], mapSource : {}})
                })
                this.props.getEventsByType(cookies.get("token"),moment(start).format('YYYY-MM-DD'), moment(end).format('YYYY-MM-DD'), `types=${type}`, `users=${provider.user.id}`)
                cookies.set("getEventsByType", true)
            }
        }

        if(provider && cookies.get('getSelectedProvider') === 'true'){
            cookies.set('getSelectedProvider', false)
            this.setState({
                provider
            })
            this.props.getEventsByType(cookies.get("token"),moment(start).format('YYYY-MM-DD'), moment(end).format('YYYY-MM-DD'),'', `users=${provider.user.id}`)
            cookies.set("getEventsByType", true)
        }

        if(validatedMission.status && cookies.get('createMission') === 'true'){
            cookies.set('createMission', false)
            const {status} = validatedMission
            const {type}   = selectedEvent
            if(status === 200 ){
                this.props.resetCreateMissionStatus()
                this.props.getEventsByType(cookies.get("token"),moment(start).format('YYYY-MM-DD'), moment(end).format('YYYY-MM-DD'), `types=${type}`, `users=${provider.user.id}`)
                cookies.set("getEventsByType", true)
            }
        }
    }

    onSubmitCreate = (createdEvent) => {
        this.setState({
            createdEvent
        })
    }

    handleSubmitUpdate = (updateMode) => {
        this.closeChoiceEventModal()
        let eventToUpdate    = Object.assign({}, this.state.eventToUpdate),
            oldSubEvent      = Object.assign({}, this.state.oldSubEvent)
        if(updateMode === 'uniqueEvent')
        {
            eventToUpdate.start_rec = eventToUpdate.start
            eventToUpdate.end_rec   = eventToUpdate.start
        }
        else if(updateMode === 'allNextSubEvents'){
            if(moment(oldSubEvent.start).diff(eventToUpdate.start) !== 0)
                eventToUpdate.newToken = true
            eventToUpdate.start_rec = eventToUpdate.start
            delete eventToUpdate.start
        }
        else{
            oldSubEvent = Object.assign({},{token : eventToUpdate.token})
            delete eventToUpdate.repeat_type
            delete eventToUpdate.repeat_rec
            delete eventToUpdate.start_rec
            delete eventToUpdate.end_rec
            delete eventToUpdate.comment
            delete eventToUpdate.pending
            delete eventToUpdate.locked
            delete eventToUpdate.start
            delete eventToUpdate.token
            delete eventToUpdate.title
        }
        cookies.set('getUpdateEvent', true)
        this.props.getUpdateEvent(cookies.get("token"),oldSubEvent, eventToUpdate, updateMode, eventToUpdate.id)
    }

    handleViewRender = (start, end) => {
        const {provider} = this.props.provider
        this.props.getSelectedDate(start)
        if(moment(start).isBefore(this.state.start) || moment(end).isAfter(this.state.end)){
            this.setState({
                start,
                end
            })
            if(provider){
                this.props.getEventsByType(cookies.get("token"),start, end, '', `users=${provider.user.id}`)
                cookies.set("getEventsByType", true)
            }
        }

    }

    handleEventClick = (eventObj, anchorEl) => {
        let selectedEvent   = this.state[eventObj.source.id].mapSource[eventObj.mapId]
        selectedEvent = Object.assign({}, selectedEvent, {
            _start      : eventObj.start,
            start       : moment(eventObj.start).format('YYYY-MM-DD'),
            startTime   : eventObj.start.format('HH:mm:ss'),
            end         : moment(eventObj.end).format('YYYY-MM-DD'),
            endTime     : eventObj.end.format('HH:mm:ss'),
            allDay      : false,
            color       : eventObj.source.color,
            textColor   : eventObj.source.textColor,
            validated   : eventObj.validated,
            resourceId  : null,
            anchorEl})
        delete selectedEvent.subEvents
        //console.log(moment('2018-07-02').diff('2018-08-03', 'days'))
        //this.props.getSelectedEvent(selectedEvent)
        //this.props.getInfoEventModal(true)
        this.setState({
            selectedEvent,
            openInfoModal : true
        })
    }

    handleDayClick= (start) => {
        const {provider} = this.props.provider
        if(provider)
            this.setState({
                openCreateEventModal: true,
                selectedDate: moment(start)
            })
        else
            this.setState({
                error : 'selectioner un provider sur la liste de gauche svp !'
            })
    }

    handleEventDropResize= (event, delta, revertFunc) => {
        this.setState({
            eventToUpdate   : Object.assign({}, this.state.oldSubEvent, {
                start_time  : event.start.format('HH:mm:ss'),
                end_time    : event.end.format('HH:mm:ss'),
                duration    : event.end.diff(event.start, 'days'),
                start       : event.start.format('YYYY-MM-DD')
            }),
            selectedEvent           : this.state.oldSubEvent,
            revertFunc
        })
        if(this.state.oldSubEvent.repeat_type === 'punctual' || !moment(event.start.format('YYYY-MM-DD')).isSame(this.state.oldSubEvent.start, 'day'))
            this.handleSubmitUpdate('uniqueEvent')
        else{
            let choices = ['uniqueEvent', 'allNextSubEvents', 'allSubEvents']
            if(moment(event.start.format('YYYY-MM-DD')).isSame(this.state.oldSubEvent.end_rec, 'day') || moment(event.start.format('YYYY-MM-DD')).isSame(this.state.oldSubEvent.start_rec, 'day'))
                choices = ['uniqueEvent', 'allSubEvents']
            this.setState({
                openChoiceEventModal    : true,
                choices
            })
        }
    }

    initialize = (event) => {
        let oldSubEvent = Object.assign({}, this.state[event.source.id].mapSource[event.mapId], {
            start : event.start.format('YYYY-MM-DD')
        })
        delete oldSubEvent.event_users
        delete oldSubEvent.subEvents
        delete oldSubEvent.address
        this.setState({
            oldSubEvent
        })
    }

    onCloseInfoModal = (deleteMode='') => {
        this.setState({
            deleteMode,
            openInfoModal : false
        })
    }

    onCloseCreateModal = () => {
      this.setState({
        openCreateEventModal : false
      })
    }

    closeChoiceEventModal =  () => {
        this.setState({
            openChoiceEventModal : false
        })
    }

    closeNotify = () => {
        this.setState({
            error : false
        })
    }

    fullScreen = (isFull) => {
        this.setState({
            isFull
        })
    }

    componentDidMount() {
        $('#calendar').fullCalendar({
            dayClick: (start) => {
                this.handleDayClick(start)
            },
            eventClick: (eventObj,eventEl) => {
                this.handleEventClick(eventObj, eventEl.currentTarget)
            },
            eventResizeStart : (event) => {
                this.initialize(event)
            },
            eventResize: ( event , delta , revertFunc ) => {
                this.handleEventDropResize(event, delta, revertFunc)
            },
            eventDragStart : (event) => {
                this.initialize(event)
            },
            eventDrop: ( event , delta , revertFunc ) => {
                this.handleEventDropResize(event, delta, revertFunc)
            },
            viewRender: (view) => {
                this.handleViewRender(view.start.format('YYYY-MM-DD'), view.end.subtract(1, 'days').format('YYYY-MM-DD'))
            },
            eventAfterRender : ( event, element, view ) =>  {},
            customButtons: {},
            events:[{ title : 'halim', start : '2018-08-10T12:00:00', end : '2018-08-11'}],
            header: {
                left: 'today prev next ',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            defaultDate:moment().format('YYYY-MM-DD'),
            defaultView: 'month',
            height: 'parent',
            navLinks:true,
            editable: true,
            weekNumbers:true,
            weekNumbersWithinDays: true
        })
        $('.fc-view-container').addClass("wrapCalendar")
        $('#sideCalendar').addClass("wrapCalendar")
        $('.wrapCalendar').wrapAll("<div id='wrapCalendar'></div>")
        $('.fc-header-toolbar').addClass("wrapTitle")
        $('#sideTitle').addClass("wrapTitle")
        $('.wrapTitle').wrapAll("<div id='wrapTitle'></div>")

    }


    render() {
        const calendare = $('#calendar')
        if(cookies.get("fetchSources") === 'true'){
            cookies.set("fetchSources", false)
            calendare.fullCalendar( 'removeEventSources' )
            calendare.fullCalendar( 'addEventSource', this.state.appointment)
            calendare.fullCalendar( 'addEventSource', this.state.benefit)
            calendare.fullCalendar( 'addEventSource', this.state.firstAppointment)
            calendare.fullCalendar( 'addEventSource', this.state.other)
            calendare.fullCalendar( 'addEventSource', this.state.availability)
            //calendare.fullCalendar( 'addEventSource', this.state.test)
        }
        const {classes} = this.props
        const { isFull, openInfoModal, selectedEvent, openCreateEventModal, error, provider, openChoiceEventModal, choices} = this.state
        return <Fullscreen
                enabled={isFull}
                onChange={isFull => this.fullScreen(isFull)}
                >
                    <Paper id="calendar" className={classes.paper}>
                        <Side />
                        <TitleSide />
                        <ModalInfo open={openInfoModal} event={selectedEvent} onClose={this.onCloseInfoModal} provider={provider}/>
                        <Modal showCloseIcon={true} open={openCreateEventModal} onClose={this.onCloseCreateModal} center>
                            <CreateModal selectedDate={this.state.selectedDate} onClose={this.onCloseCreateModal} onSubmit={this.onSubmitCreate} provider={provider}/>
                        </Modal>
                        {error &&  <Snackbar  open={Boolean(error)} variant="warning" message={error} onClose={this.closeNotify} verticalLocation="top" horizontalLocation="right" />}
                        <ModalChoice open={openChoiceEventModal} title="mettre a jour" onClose={this.closeChoiceEventModal}  onSubmit={this.handleSubmitUpdate}  choices={choices}/>
                    </Paper>
        </Fullscreen>
    }
}

export default  compose(withStyles(styles), connect(mapStateToProps,mapDispatchToProps))(Calendar)