import React from 'react';
import PropTypes from 'prop-types';
import {
    withStyles,
    InputLabel,
    MenuItem,
    FormControl,
    Select,
    Button
    } from '@material-ui/core';

const styles = theme => ({
    button: {
        display: 'block',
        marginTop: theme.spacing.unit * 2
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120
    }
});

class ControlledOpenSelect extends React.Component {
    state = {
        age: '',
        open: true
    };

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    handleOpen = () => {
        this.setState({ open: true });
    };

    render() {
        const { classes } = this.props;

        return (
            <form autoComplete="off">
                <Button className={classes.button} onClick={this.handleOpen}>
                Open the select
                </Button>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="demo-controlled-open-select">Age</InputLabel>
                    <Select
                    open={this.state.open}
                    onClose={this.handleClose}
                    onOpen={this.handleOpen}
                    value={this.state.age}
                    onChange={this.handleChange}
                    inputProps={{
                        name: 'age',
                        id: 'demo-controlled-open-select'
                    }}
                    >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        <MenuItem value={10}>List Month</MenuItem>
                        <MenuItem value={20}>List Day</MenuItem>
                        <MenuItem value={30}>List Week</MenuItem>
                    </Select>
                </FormControl>
            </form>
        );
    }
}

ControlledOpenSelect.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ControlledOpenSelect);