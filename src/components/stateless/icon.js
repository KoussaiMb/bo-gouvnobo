import React from 'react'
import {
    AccountCircle,
    Phone,
    Email,
    Face,
    Home,
    Save,
    Place,
    Person,
    Delete,
    AccessTime,
    SentimentSatisfied
    } from '@material-ui/icons';

const icon = props => {
    const classname = props.classname ? props.classname:""
        switch (props.icon){
            case "Face"             : return <Face className={classname}/>
            case "Person"           : return <Person className={classname}/>
            case "Home"             : return <Home className={classname}/>
            case "Email"            : return <Email className={classname}/>
            case "Phone"            : return <Phone className={classname}/>
            case "Save"             : return <Save className={classname}/>
            case "Delete"           : return <Delete className={classname}/>
            case "Place"            : return <Place className={classname}/>
            case "AccessTime"       : return <AccessTime className={classname}/>
            case "AccountCircle"    : return <AccountCircle className={classname}/>
            default                 : return <SentimentSatisfied className={classname}/>
        }
}

export default icon