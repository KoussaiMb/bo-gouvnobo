import React from 'react';
import PropTypes from 'prop-types';
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    withMobileDialog
    } from '@material-ui/core';

class ResponsiveDialog extends React.Component {
    render() {
        const { open, onClose, title, fullScreen, message, Disagree, Agree  } = this.props;

        return (
                <Dialog
                fullScreen={fullScreen ? fullScreen : false}
                open={open}
                onClose={onClose}
                aria-labelledby="responsive-dialog-title"
                >
                    <DialogTitle id="responsive-dialog-title">{title}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                        {message}
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={Disagree.onClick} color="primary">
                        {Disagree.text ? Disagree.text : 'ANNULER'}
                        </Button>
                        <Button onClick={Agree.onClick} color="primary" autoFocus>
                        {Agree.text ? Agree.text : 'OK'}
                        </Button>
                    </DialogActions>
                </Dialog>
        );
    }
}

ResponsiveDialog.propTypes = {
    fullScreen: PropTypes.bool.isRequired
};

export default withMobileDialog()(ResponsiveDialog);