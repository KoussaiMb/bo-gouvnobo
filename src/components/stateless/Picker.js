import React, { PureComponent } from 'react';
import {connect} from 'react-redux';
import 'moment/locale/fr';
import moment from 'moment';
import BasePicker from 'material-ui-pickers/_shared/BasePicker';
import Calendar from 'material-ui-pickers/DatePicker/components/Calendar';
import MuiPickersUtilsProvider from 'material-ui-pickers/utils/MuiPickersUtilsProvider';
import MomentUtils from 'material-ui-pickers/utils/moment-utils';
import {getSelectedDate} from '../../action/providerActions';

moment.locale('fr');

const mapDispatchToProps = dispatch => {
    return {
        getSelectedDate  :    date => dispatch(getSelectedDate(date))
    }
}

const mapStateToProps = state => {
    return {
        SelectedDate     :   state.getSelectedDateReducer
    }
}

class Picker extends PureComponent {
    state = {
        selectedDate: new Date()
    }

    componentWillReceiveProps(nextProps){
        const {date}    =   nextProps.SelectedDate
        this.setState({
            selectedDate : date
        })
    }

    handleDateChange = (date) => {
        this.setState({ selectedDate: date });
        this.props.getSelectedDate(moment(date).format('YYYY-MM-DD'));
    }

    render() {
        const { selectedDate } = this.state;
        return (
            <MuiPickersUtilsProvider utils={MomentUtils} locale="fr" moment={moment}>
                <BasePicker value={selectedDate} onChange={this.handleDateChange}>
                    {
                        ({date,handleAccept,handleChange,handleClear,handleDismiss,handleSetTodayDate,handleTextFieldChange,pick12hOr24hFormat}) => (
                            <div className="picker">
                                <Calendar date={date} onChange={handleChange} />
                            </div>

                        )
                        }
                </BasePicker>
            </MuiPickersUtilsProvider>
        );
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Picker);
