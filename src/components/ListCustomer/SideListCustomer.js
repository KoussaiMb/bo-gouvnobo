import React, {Component} from 'react';
import {connect} from 'react-redux';
import cookies from 'js-cookie';
import {
    List,
    ListItem,
    ListItemText,
    Grid,
    TextField,
    CardContent,
    Card,
    Typography,
    ListItemIcon
    } from '@material-ui/core/List';
import {Face} from "@material-ui/icons";
import CustomerGeneralInfo from "./CustomerGeneralInfo";
import {getAllCustomersByCluster} from '../../action/getAllCustomersByCluster';

const mapStateToProps = (state) => {
    return {
        customersData: state.getAllCustomersByCluster
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getAllCustomersByCluster: (apiToken) =>
            dispatch(getAllCustomersByCluster(apiToken))
    };
};

class SideListCustomer extends Component {
    constructor(props) {
        super(props);
        this.data = []; // contain the result of API call
        this.fillData = false; // to just fill this.data one time with the result of the API call, then the search filter it
        this.searchLength = 0; // to compare with the last size of the search input to know if the user delete
        this.state = {
            currentCustomer: {},
            search: ""
        }
    }

    componentWillMount() {
        this.props.getAllCustomersByCluster(cookies.get("token"));
    }

    handleClick(customer) {
        this.setState({currentCustomer: customer, resetInfo: true});
    }

    handleSearch(search) {
        if (this.searchLength > search.target.value.length) {
            this.data = this.props.customersData.list;
        }
        console.log(this.data);
        this.searchLength = search.target.value.length;
        this.data = this.data.filter(item => {
            let clientName = item.firstname + ' ' + item.lastname;
            clientName = clientName.toLowerCase();
            let userInput = search.target.value.toLowerCase();
            console.log(userInput);
            return (clientName.includes(userInput));
        });

        console.log(this.data);
        this.setState({
            search: search.target.value
        });
    }

    render() {

        if (this.props.customersData && this.props.customersData.list && !this.fillData) {
            this.data = this.props.customersData.list;
            this.fillData = true;
        }
        return (
            <Grid container spacing={16} alignItems="flex-start">
                <Grid item xs={10} md={3} xl={2}>
                    <Card style={{}}>
                        <CardContent>
                            <ListItem>
                                <ListItemIcon>
                                    <Face style={{color: '#e6604e'}}/>
                                </ListItemIcon>
                                <Typography variant="title">Clients</Typography>
                            </ListItem>
                            <TextField
                                style={{width: '100%'}}
                                id="customer"
                                label="🔎"
                                value={this.state.search}
                                margin="normal"
                                onChange={this.handleSearch.bind(this)}
                            />
                            <List component="nav" style={{maxHeight: "72vh", overflow: 'auto'}}>
                                { this.props.customersData.list  && this.props.customersData.list.map((customer) => {
                                    return (
                                        <ListItem button key={customer.id} onClick={() =>this.handleClick(customer)}>
                                            <ListItemText primary={customer.firstname + ' ' + customer.lastname} />
                                        </ListItem>
                                    );
                                })
                                }
                            </List>
                        </CardContent>
                    </Card>
                </Grid>
                {this.state.currentCustomer &&
                <Grid item xs={12} md={9} xl={10}>
                    <CustomerGeneralInfo customer={this.state.currentCustomer}/>
                </Grid> }
            </Grid>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SideListCustomer);