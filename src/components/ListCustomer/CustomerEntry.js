import React, {Component} from 'react';
import {
    Typography,
    ListItem,
    ListItemIcon
    } from '@material-ui/core/Typography'

class CustomerEntry extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
    <ListItem>
      <ListItemIcon>
        {this.props.icon}
      </ListItemIcon>
      <Typography variant="subheading">{this.props.text}</Typography>
    </ListItem>
    );
  }
}

export default CustomerEntry;