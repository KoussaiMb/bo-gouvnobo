import React, {Component} from 'react';
import {
    Card,
    CardContent,
    Typography,
    ListItemIcon,
    ListItem,
    ExpansionPanel,
    ExpansionPanelSummary,
    ExpansionPanelDetails
    } from "@material-ui/core";
import { Book, NavigateNext, ExpandMore } from "@material-ui/icons";

class PlanningTypeCard extends Component {
    render() {
        const {data} = this.props
        return (
            <div>
                <Card style={{marginTop: "24px"}}>
                    <CardContent style={{paddingTop: '2px', paddingBottom: '2px'}}>
                        <ListItem>
                            <ListItemIcon>
                                <Book style={{color: '#e6604e'}}/>
                            </ListItemIcon>
                            <Typography variant="title">Type d'entretien</Typography>
                        </ListItem>
                    </CardContent>
                </Card>
                <Card style={{maxHeight: '36vh', overflow: 'auto'}}>
                    <CardContent>
                        {data.mp_template.mp_units.map(unit =>
                                    <ExpansionPanel>
                                        <ExpansionPanelSummary expandIcon={<ExpandMore />}>
                                            <Typography>{unit.list.field}</Typography>
                                        </ExpansionPanelSummary>
                                        <ExpansionPanelDetails>
                                            <Typography> {unit.mp_unit_tasks.map(task =>
                                                <Typography><NavigateNext />{task.list.field}</Typography>
                                            )}
                                            </Typography>
                                        </ExpansionPanelDetails>
                                    </ExpansionPanel>
                                )
                            }
                        {Object.keys(this.props.data.mp_template.mp_units).length === 0 &&
                        <Typography  variant="subheading">Liste des PE Type est vide</Typography>
                        }
                    </CardContent>
                </Card>
            </div>
        );
    }
}

export default PlanningTypeCard;
