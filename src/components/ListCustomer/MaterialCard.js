import React, {Component} from 'react';
import {
    Card,
    CardContent,
    Typography,
    Divider,
    ListItemIcon,
    List,
    ListItem,
    ListItemText,
    Avatar
    } from "@material-ui/core";
import {Place} from "@material-ui/icons";

class MaterialCard extends Component {
  render() {
    return (
      <div>
        <Card>
          <CardContent style={{paddingTop: '2px', paddingBottom: '2px'}}>
            <ListItem>
              <ListItemIcon>
                <Place style={{color: '#e6604e'}}/>
              </ListItemIcon>
              <Typography variant="title">Matériaux nobles</Typography>
            </ListItem>
          </CardContent>
        </Card>
        <Card style={{maxHeight: '29vh', overflow: 'auto'}}>
            <CardContent>
                {this.props.data && this.props.data.map((material) => {
                    return (
                        <List>
                            <ListItem>
                                <Avatar>
                                    <img src={material.list_reference.url}/>
                                </Avatar>
                                <ListItemText primary={material.list_reference.value }/>
                                <ListItemText primary={material.list.field} secondary={"Commentaire : " + ((material.comment !== "") ? material.comment : "Aucun") }/>
                            </ListItem>
                            <li>
                                <Divider inset />
                            </li>
                        </List>
                    );
                })
                }
                { !this.props.data[0] &&
                <Typography variant="subheading">
                    La liste des matériaux est vide...
                </Typography>}
            </CardContent>
        </Card>
      </div>
    );
  }
}

export default MaterialCard;