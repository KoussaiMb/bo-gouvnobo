import React, {Component} from 'react';
import {
    Card,
    CardContent,
    Typography,
    Grid,
    ListItemIcon,
    ListItem,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Slide,
    TextField
    } from "@material-ui/core";
import {Delete, EventNote, Edit} from "@material-ui/icons";

function Transition(props) {
    return <Slide direction="up" {...props} />;
}
class LocalizationTrashCard extends Component {
    state = {
        open: false,
    };
    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };
    render() {

        return (
            <div>
                <Card style={{marginTop: "24px"}}>
                <CardContent style={{paddingTop: '2px', paddingBottom: '2px'}}>
                    <Grid container spacing={24} alignItems="flex-start">
                        <Grid item xs={9} lg={11}>
                            <ListItem>
                                <ListItemIcon>
                                    <Delete style={{color: '#e6604e'}}/>
                                </ListItemIcon>
                                <Typography variant="title">Local Poubelle</Typography>
                            </ListItem>
                        </Grid>
                        <Grid item xs={3} lg={1}>
                            <ListItem>
                                <ListItemIcon>
                                    <Edit onClick={this.handleClickOpen}/>
                                </ListItemIcon>
                            </ListItem>
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
                <Card style={{maxHeight: '36vh', overflow: 'auto'}}>
                    <CardContent>
                        { this.props && this.props.data.map((trash) => {trash.trash_localization !== ""  }) &&
                        this.props.data.map((trash)=>{
                            return(
                                <div>
                                    <Typography variant="subheading" gutterBottom>
                                        {((trash.trash_localization !== "") ? trash.trash_localization : "Pas défini ") }
                                    </Typography>
                                    <Dialog
                                        open={this.state.open}
                                        TransitionComponent={Transition}
                                        keepMounted
                                        onClose={this.handleClose}
                                        aria-labelledby="alert-dialog-slide-title"
                                        aria-describedby="alert-dialog-slide-description">
                                        <DialogTitle id="alert-dialog-slide-title">
                                            Modification
                                        </DialogTitle>
                                        <DialogContent>
                                            <DialogContentText id="alert-dialog-slide-description">
                                                <TextField
                                                    id="trash_localization"
                                                    label="Lieu"
                                                    value={((trash.trash_localization !== "") ? trash.trash_localization : "") }
                                                    margin="normal"
                                                />
                                            </DialogContentText>
                                        </DialogContent>
                                        <DialogActions>
                                            <Button onClick={this.handleClose} color="green">
                                                Valider
                                            </Button>
                                            <Button onClick={this.handleClose}>
                                                Fermer
                                            </Button>
                                        </DialogActions>
                                    </Dialog>
                                </div>
                            );
                        })
                        }{ !this.props.data[0] &&
                    <Typography variant="subheading">
                        Pas défini
                    </Typography>}
                    </CardContent>
                </Card>
            </div>
        );
    }
}

export default LocalizationTrashCard;
