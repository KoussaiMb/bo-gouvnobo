import React, {Component} from 'react';
import {
    Card,
    CardContent,
    Typography,
    Divider,
    ListItemIcon,
    ListItem
    } from "@material-ui/core";
import {Book} from "@material-ui/icons";

class PlanningCard extends Component {
    render() {
        return (
            <div>
                <Card style={{marginTop: "24px"}}>
                    <CardContent style={{paddingTop: '2px', paddingBottom: '2px'}}>
                        <ListItem>
                            <ListItemIcon>
                                <Book style={{color: '#e6604e'}}/>
                            </ListItemIcon>
                            <Typography variant="title">Planning</Typography>
                        </ListItem>
                    </CardContent>
                </Card>
                <Card style={{maxHeight: '36vh', overflow: 'auto'}}>
                    <CardContent>
                        {Object.keys(this.props.data).length !== 0 &&
                        <div>
                            <Typography variant="subheading" gutterBottom>
                                {this.props.data.mp_template.name}</Typography>
                            <Typography variant="subheading" gutterBottom>
                                DE {this.props.data.mp_template.min_area} m² à {this.props.data.mp_template.max_area} m² </Typography>
                            {this.props.data.mp_template.childs === 1 && <Typography variant="subheading" gutterBottom>Avec Enfants</Typography>}
                            {this.props.data.mp_template.ironing === 1 && <Typography variant="subheading" gutterBottom>Avec Repassage</Typography>}
                            <Typography variant="subheading" gutterBottom>
                                {this.props.data.mp_template.list.field}</Typography>
                            <Typography variant="subheading" gutterBottom>
                                {"Commentaire : " + ((this.props.data.comment !== "") ? this.props.data.comment : "Aucun") }</Typography>
                        </div>
                        } {Object.keys(this.props.data).length === 0 &&
                    <Typography  variant="subheading">Liste des PE Type est vide</Typography>                    }
                    </CardContent>
                </Card>
            </div>
        );
    }
}

export default PlanningCard;
