import React, {Component} from 'react';
import moment from "moment";
import {
    Card,
    CardContent,
    Typography,
    Grid,
    Select,
    MenuItem,
    FormControl,
    InputLabel,
    ListItem,
    ListItemIcon
    } from '@material-ui/core';
import { Person, Phone, PhoneInTalk, Email, ChildCare } from "@material-ui/icons";
import CustomerEntry from './CustomerEntry';
import CustomerAddressInfo from './CustomerAddressInfo';
import {getInArrayById} from '../../utils/utilistFunctions';
import '../../assets/css/customer.css';

class CustomerGeneralInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            addressId: 0,
            addressInfo: null,
            showCardex: false
        };
        this.state = {
            addressIdOne: 0,
            addressInfoOne: null
        };
    }

    handleChange = event => {
        this.setState({
            addressId: event.target.value,
            addressInfo: getInArrayById(this.props.customer.address, event.target.value),
            addressInfo:  event.target.value,
            showCardex: true,
        });
    };


    render() {
        const {customer} = this.props;

        return (
            <div>
                <Card>
                    { customer && this.props.customer.firstname && this.props.customer.customer &&
                     <CardContent>
                        <Grid container spacing={24} alignItems="stretch">
                            <Grid item lg={6}>
                                <ListItem>
                                    <ListItemIcon>
                                        <Person style={{color: '#e6604e'}}/>
                                    </ListItemIcon>
                                    <Typography variant="title">{this.props.customer.firstname} {this.props.customer.lastname}</Typography>
                                </ListItem>
                                <CustomerEntry text={this.props.customer.phone} icon={<Phone style={{color: '#e6604e'}}/>} />
                                {this.props.customer.customer.last_call_at &&
                                <CustomerEntry text={'Le ' + moment(this.props.customer.customer.last_call_at).day()
                                + '/' + moment(this.props.customer.customer.last_call_at).month() + '/' + moment(this.props.customer.customer.last_call_at).year() + ' à '
                                + moment(this.props.customer.customer.last_call_at).hours() + ':' + moment(this.props.customer.customer.last_call_at).minutes() } icon={<PhoneInTalk style={{color: '#e6604e'}}/>} /> }
                                <CustomerEntry text={this.props.customer.email} icon={<Email style={{color: '#e6604e'}}/>}/>
                                { this.props.customer.childNb &&
                                <CustomerEntry text={this.props.customer.childNb} icon={<ChildCare style={{color: '#e6604e'}}/>}/> }
                            </Grid>
                            { Object.keys(this.props.customer && this.props.customer.address).length === 1 &&
                            <Grid item xs={12} lg={6} style={{alignSelf: 'center', alignItems: 'center', marginTop: '-15px'}}>
                                <FormControl style={{minWidth: "20vw"}}>
                                    <Typography variant="Subhead" htmlFor="addressName">Adresse</Typography>
                                    <Typography value={this.state.addressId} inputProps={{
                                        name: 'addressName',
                                        id: 'addressId'
                                    }}>
                                        { this.props.customer.address && this.props.customer.address.map((address) => {
                                            return(
                                                <Typography variant="Title" style={{marginTop: '8px'}} key={address.id} value={address.id}>{address.address}</Typography>
                                            );
                                        })
                                        }
                                    </Typography>
                                </FormControl>
                            </Grid> }
                            { Object.keys(this.props.customer && this.props.customer.address).length > 1 &&
                            <Grid item xs={12} lg={6} style={{alignSelf: 'center', alignItems: 'center'}}>
                             <FormControl style={{minWidth: "20vw"}}>
                                 <InputLabel htmlFor="addressName" style={{marginTop : '-25px'}}>Adresse</InputLabel>
                                    <Select value={this.state.addressId} onChange={this.handleChange} inputProps={{
                                         name: 'addressName',
                                         id: 'addressId',
                              }}>
                                  { this.props.customer.address && this.props.customer.address.map((address) => {
                                      return(

                                             <MenuItem key={address.id} value={address.id}>{address.address}</MenuItem>
                                            );
                                     })
                                   }
                                     </Select>
                            </FormControl>
                            </Grid> }
                            { Object.keys(this.props.customer && this.props.customer.address).length === 0 &&
                            <Grid item xs={12} lg={6} style={{alignSelf: 'center', alignItems: 'center'}}>
                                <FormControl style={{minWidth: "30vw", marginTop: '-90px'}}>
                                    <Typography variant="Subhead" htmlFor="addressName" style={{marginTop: '-37px'}}>Il n'y pas d'adresse définie pour ce client</Typography>
                                </FormControl>
                            </Grid>
                            }
                        </Grid>
                    </CardContent> }
                </Card>
                {  ! this.props.customer.firstname && <Typography style={{textAlign: 'center', transform: `translateY(200%)`}} variant="display1">Aucun client sélectionné</Typography>}
                { this.props.customer.address && Object.keys(this.props.customer.address).length === 1 &&
                    this.props.customer.address.map((addresss) =>{
                          this.state.addressIdOne= addresss.id
                          this.state.addressInfoOne= addresss.addresss
                }) &&
                <CustomerAddressInfo address={this.state.addressIdOne} currentCustomerAddresses={this.state.addressInfoOne}/>}
                { this.state.showCardex &&
                <CustomerAddressInfo address={this.state.addressInfo} currentCustomerAddresses={this.props.customer.address}/> }
            </div>
        );
    }
}

export default CustomerGeneralInfo;
