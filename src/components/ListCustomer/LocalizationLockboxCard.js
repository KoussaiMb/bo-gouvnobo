import React, {Component} from 'react';
import {
    Card,
    CardContent,
    Typography,
    ListItemIcon,
    ListItem
    } from "@material-ui/core";
import {LockOpen} from "@material-ui/icons";

class LocalizationLockboxCard extends Component {
    render() {

        return (
            <div>
                <Card style={{marginTop: "24px"}}>
                    <CardContent style={{paddingTop: '2px', paddingBottom: '2px'}}>
                        <ListItem>
                            <ListItemIcon>
                                <LockOpen style={{color: '#e6604e'}}/>
                            </ListItemIcon>
                            <Typography variant="title">Lockbox</Typography>
                        </ListItem>
                    </CardContent>
                </Card>
                <Card style={{maxHeight: '36vh', overflow: 'auto'}}>
                    <CardContent>
                        { this.props.data.map((lockbox) => {lockbox.lockbox_localization !== null  &&  lockbox.lockbox_code !== null }) &&
                        this.props.data.map((lockbox)=>{
                            return(
                                <div>
                                    <Typography variant="subheading" gutterBottom>
                                        {lockbox.lockbox_code} </Typography>
                                    <Typography variant="subheading" gutterBottom>
                                        {((lockbox.lockbox_localization !== null) ? lockbox.lockbox_localization : "Pas défini ") }
                                    </Typography>
                                </div>
                            );
                        })
                        } { !this.props.data[0] &&
                    <Typography variant="subheading">
                        Pas défini
                    </Typography>}
                    </CardContent>
                </Card>
            </div>
        );
    }
}

export default LocalizationLockboxCard;
