import React, {Component} from 'react';
import cookies from 'js-cookie';
import {connect} from 'react-redux';
import {
    Typography,
    Grid
    } from "@material-ui/core";
import {searchInArrayById} from '../../utils/utilistFunctions';
import {getMaintenancePlanningv2,ResetgetMaintenancePlanningv2} from '../../action/getMaintenancePlanningv2';
import {getMpCardex,ResetgetMpCardex} from '../../action/getMpCardex';
import PlanningCard from './PlanningCard';
import PlanningTypeCard from './PlanningTypeCard';
import CardexCard from './CardexCard';
import AddressCard from './AddressCard';
import ProductCard from "./ProductCard";
import MaterialCard from "./MaterialCard";
import LocalizationLockboxCard from "./LocalizationLockboxCard";
import LocalizationTrashCard from "./LocalizationTrashCard";
import {getProducts} from "../../action/getProducts";
import {getMaterials} from "../../action/getMaterials";
import {getAddressInfo} from "../../action/getAddressInfo";
import {getLocalizationLockboxTrash} from "../../action/getLocalizationLockboxTrash";

const mapStateToProps = (state) => {
    return {
        productsData: state.getProducts,
        materialsData: state.getMaterials,
        mainData: state.getMaintenancePlanningv2,
        addressData: state.getAddressInfo,
        cardexData : state.getMpCardex,
        LocalizationLockboxTrash : state.getLocalizationLockboxTrash
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getProducts: (apiToken, id) => dispatch(getProducts(apiToken, id)),
        getMaterials: (apiToken, id) => dispatch(getMaterials(apiToken, id)),
        getMaintenancePlanningv2: (apiToken, id) => dispatch(getMaintenancePlanningv2(apiToken, id)),
        ResetgetMaintenancePlanningv2: () => dispatch(ResetgetMaintenancePlanningv2()),
        ResetgetMpCardex: () => dispatch(ResetgetMpCardex()),
        getAddressInfo: (apiToken, id) => dispatch(getAddressInfo(apiToken, id)),
        getMpCardex: (apiToken, id) => dispatch(getMpCardex(apiToken, id)),
        getLocalizationLockboxTrash: (apiToken, id) => dispatch(getLocalizationLockboxTrash(apiToken, id)),

    };
};

class CustomerAddressInfo extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.props.getProducts(cookies.get("token"), this.props.address);
        this.props.getMaterials(cookies.get("token"), this.props.address);
        this.props.getMaintenancePlanningv2(cookies.get("token"), this.props.address);
        cookies.set('getMaintenancePlanningv2', true)
        this.props.getAddressInfo(cookies.get("token"), this.props.address);
        this.props.getLocalizationLockboxTrash(cookies.get("token"), this.props.address);
    }

    componentDidUpdate(prevProps) {
        if ((this.props.address && prevProps.address && this.props.address !== prevProps.address)) {
            this.props.ResetgetMaintenancePlanningv2();
            this.props.getProducts(cookies.get("token"), this.props.address);
            this.props.getMaterials(cookies.get("token"), this.props.address);
            this.props.getMaintenancePlanningv2(cookies.get("token"), this.props.address);
            cookies.set('getMaintenancePlanningv2', true)
            this.props.getAddressInfo(cookies.get("token"), this.props.address);
            this.props.getLocalizationLockboxTrash(cookies.get("token"), this.props.address);
        }
    }

    componentWillReceiveProps(nextProps){
        const {mainData} = nextProps
            if(mainData.maintenanceData && cookies.get('getMaintenancePlanningv2') === 'true' ){
            cookies.set('getMaintenancePlanningv2', false)
            if(mainData.maintenanceData.mp_template){
                if(mainData.maintenanceData.mp_template.mp_units[0]){
                    this.props.getMpCardex(cookies.get("token"), mainData.maintenanceData.mp_template.mp_units[0].l_maintenanceType_id);
                    this.props.ResetgetMpCardex();
                }
            }
            this.setState({

            })
        }
    }

    render() {
        const { address } = this.props

        return (
            <div style={{marginTop: '24px'}}>
                { address  &&  this.props.mainData.maintenanceData && this.props.cardexData.maintenanceData &&
                this.props.productsData.productsData && this.props.materialsData &&
                this.props.productsData.productsData.data && this.props.addressData.addressData && this.props.addressData.addressData.data ?

                <div>
                    {Object.keys(this.props.mainData.maintenanceData).length !== 0 ?
                        <Grid container spacing={24} alignItems="flex-start">
                            <Grid item xs={12} md={6} lg={6}>
                                <PlanningCard  data={this.props.mainData.maintenanceData} />
                            </Grid>
                            <Grid item xs={12} md={6} lg={6}>
                                <PlanningTypeCard addressId={address.id} data={this.props.mainData.maintenanceData} />
                            </Grid>
                            <Grid item xs={12} md={6} lg={6}>
                                <CardexCard data={this.props.cardexData.maintenanceData} />
                            </Grid>
                            <Grid item xs={12} md={6} lg={6}>
                                <AddressCard data={this.props.addressData.addressData.data}/>
                            </Grid>
                            <Grid item xs={12} md={6} lg={6}>
                                <ProductCard addressId={address.id} data={this.props.productsData.productsData.data}/>
                            </Grid>
                            <Grid item xs={12} md={6} lg={6}>
                                <MaterialCard addressId={address.id} data={this.props.materialsData.materialsData}/>
                            </Grid>
                            <Grid item xs={12} md={6} lg={6}>
                                <LocalizationLockboxCard addressId={address.id} data={this.props.LocalizationLockboxTrash.LocalizationLockboxTrashData}/>
                            </Grid>
                            <Grid item xs={12} md={6} lg={6}>
                                <LocalizationTrashCard addressId={address.id} data={this.props.LocalizationLockboxTrash.LocalizationLockboxTrashData}/>
                            </Grid>
                        </Grid>
                        :
                        <Typography style={{textAlign: 'center', transform: `translateY(80%)`}} variant="display1">Liste des PE vide</Typography>
                    }
                </div>
                    :
                    <Typography style={{textAlign: 'center', transform: `translateY(80%)`}} variant="display1">Liste des PE vide</Typography>
                }
            </div>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CustomerAddressInfo);
