import React, {Component} from 'react';
import {
    Card,
    CardContent,
    Typography,
    Divider,
    ListItem,
    ListItemIcon,

    } from "@material-ui/core";
import {Place} from "@material-ui/icons";
import PlanningTypeCard from "./PlanningTypeCard";

class ProductCard extends Component {
  render() {
    let i = 0;
    return (
      <div>
        <Card>
          <CardContent style={{paddingTop: '2px', paddingBottom: '2px'}}>
            <ListItem>
              <ListItemIcon>
                <Place style={{color: '#e6604e'}}/>
              </ListItemIcon>
              <Typography variant="title">Localisation des produits</Typography>
            </ListItem>
          </CardContent>
        </Card>
        <Card style={{maxHeight: '29vh', overflow: 'auto'}}>
          <CardContent>
            { this.props.data.map((task) => {
              i += 1;
              return (
                <div key={task.id}>
                  { i !== 1 && <Divider style={{marginTop: '15px', marginBottom: '15px'}}/>}
                  <Typography variant="subheading" gutterBottom><span style={{fontWeight: 'bold', fontSize: '18px', color: "#e6604e"}}>/ </span>
                    {task.product.value}</Typography>
                  <p>
                    {task.description}
                  </p>
                </div>
              );
            })
            }
            { !this.props.data[0] &&
            <Typography variant="subheading">
              La liste des produits est vide...
            </Typography>}
          </CardContent>
        </Card>
      </div>
    );
  }
}

export default ProductCard;