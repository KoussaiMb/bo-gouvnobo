import React, {Component} from 'react';
import cookies from "js-cookie";
import {
    CardContent,
    Card,
    Typography,
    ListItemIcon,
    ListItem,
    ExpansionPanel,
    ExpansionPanelSummary,
    ExpansionPanelDetails,
    Grid
        } from '@material-ui/core'
import { EventNote, NavigateNext, ExpandMore } from "@material-ui/icons";


const populateMpCardex = (array, value) => {
    let _array = array
    if(typeof(_array) === 'undefined')
        _array = []
    _array.push(value)
    return _array
}

class CardexCard extends Component {
    constructor (props){
        super(props)
        this.data = []
    }
    state = {
        data : new Map(),
    }
    componentWillMount(){
        let data = new Map()
        this.props.data.map(item => {
            item.mp_cardex_tasks.map(mp_cardex_task => {
                data.set(mp_cardex_task.list_reference.value, populateMpCardex(data.get(mp_cardex_task.list_reference.value), mp_cardex_task.list.field))
            })
        })
        this.setState({
            data
        })
    }
    render() {

        this.state.data.forEach((item, index) => {
            let Obj= {
                key : index,
                value : item
            }
            this.data.push(Obj)
        })

        return (
            <div>
                <Card>
                    <CardContent style={{paddingTop: '2px', paddingBottom: '2px'}}>
                        <Grid container spacing={24} alignItems="flex-start">
                            <Grid item xs={9} lg={11}>
                                <ListItem>
                                    <ListItemIcon>
                                        <EventNote   style={{color: '#e6604e'}}/>
                                    </ListItemIcon>
                                    <Typography variant="title">Cardex</Typography>
                                </ListItem>
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>
                <Card style={{maxHeight: '36vh', overflow: 'auto'}}>
                    <CardContent>
                        {this.data.map(item => {
                            return (
                                <ExpansionPanel>
                                    <ExpansionPanelSummary expandIcon={<ExpandMore />}>
                                        <Typography>{item.key}</Typography>
                                    </ExpansionPanelSummary>
                                    <ExpansionPanelDetails>
                                        <Typography>  {item.value.map(task => {
                                            return( <Typography><NavigateNext />{task}</Typography>)
                                        })}
                                        </Typography>
                                    </ExpansionPanelDetails>
                                </ExpansionPanel>
                            )
                        })
                        }{Object.keys(this.data).length === 0 &&
                    <Typography  variant="subheading">Liste des Cardex est vide</Typography>
                    }
                    </CardContent>
                </Card>
            </div>
        );
    }
}
export default CardexCard;
