import React, {Component} from 'react';
import {
    Card,
    CardContent,
    Typography,
    ListItemIcon,
    ListItem,
    Icon,
    Divider,
    Grid
    } from "@material-ui/core";
import {
    EventNote,
    Edit,
    AccountBalance,
    Home,
    LockOpen,
    Business,
    Place,
    RoomService
    } from "@material-ui/icons";

class AddressCard extends Component {
    render() {
        return (
            <div>
                <Card>
                    <CardContent style={{paddingTop: '2px', paddingBottom: '2px'}}>
                        <Grid container spacing={24} alignItems="flex-start">
                            <Grid item xs={9} lg={11}>
                                <ListItem>
                                    <ListItemIcon>
                                        <Home  style={{color: '#e6604e'}}/>
                                    </ListItemIcon>
                                    <Typography variant="title">Accès bâtiment</Typography>
                                </ListItem>
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>
                <Card style={{maxHeight: '36vh', overflow: 'auto'}}>
                    <CardContent>
                        { this.props.data.digicode &&
                        <div>
                            <ListItem>
                                <ListItemIcon>
                                    <LockOpen style={{color: '#e6604e'}}/>
                                </ListItemIcon>
                                <Typography variant="subheading">{this.props.data.digicode}</Typography>
                            </ListItem>
                            <Divider/>
                        </div>}
                        { this.props.data.batiment &&
                        <div>
                            <ListItem>
                                <ListItemIcon>
                                    <AccountBalance style={{color: '#e6604e'}}/>
                                </ListItemIcon>
                                <Typography variant="subheading">{this.props.data.batiment}</Typography>
                            </ListItem>
                            <Divider/>
                        </div>}
                        { this.props.data.door &&
                        <div>
                            <ListItem>
                                <ListItemIcon>
                                    <Icon style={{color: "#e6604e"}}>meeting_room</Icon>
                                </ListItemIcon>
                                <Typography variant="subheading">{this.props.data.door}</Typography>
                            </ListItem>
                            <Divider/>
                        </div>}
                        { this.props.data.floor &&
                        <div>
                            <ListItem>
                                <ListItemIcon>
                                    <Business style={{color: '#e6604e'}}/>
                                </ListItemIcon>
                                <Typography variant="subheading">{this.props.data.floor}</Typography>
                            </ListItem>
                            <Divider/>
                        </div>}
                        { this.props.data.city &&
                        <div>
                            <ListItem>
                                <ListItemIcon>
                                    <Place style={{color: '#e6604e'}}/>
                                </ListItemIcon>
                                <Typography variant="subheading">{this.props.data.city}</Typography>
                            </ListItem>
                            <Divider/>
                        </div>}
                        { this.props.data.doorBell &&
                        <ListItem>
                            <ListItemIcon>
                                <RoomService style={{color: '#e6604e'}}/>
                            </ListItemIcon>
                            <Typography variant="subheading">{this.props.data.doorBell}</Typography>
                        </ListItem> }
                        { this.props.data.description &&
                        <ListItem> Remarque :
                            <Typography variant="subheading">{this.props.data.description}</Typography>
                        </ListItem> }
                    </CardContent>
                </Card>
            </div>
        );
    }
}

export default AddressCard;
