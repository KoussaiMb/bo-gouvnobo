//SYSTEM
import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux'
import cookies from "js-cookie";
import {compose} from "redux";
import moment from 'moment'
import {
    withStyles,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    AppBar,
    Tabs,
    Tab,
    FormControl,
    Input,
    InputLabel
    } from '@material-ui/core';
import { Person, Search } from "@material-ui/icons";
//UTILS
import {getSelectedProvider, getUsersList} from '../../action/providerActions'


const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper
    },
    content:{
        height:'80vh',
        overflow: 'auto'
    }
})

const mapStateToProps = state => {
    return {
        usersList    : state.getUsersListReducer
    }
}


const mapDispatchToProps = dispatch => {
    return {
        getUsersList                :    (apiToken, who) => dispatch(getUsersList(apiToken, who)),
        getSelectedProvider         :    providerId => dispatch(getSelectedProvider( providerId))
    }
}

class usersList extends React.Component {

    state = {
        searchLength : 0,
        fillData : false,
        search: "",
        data : [],
        _data : [],
        currentProvider: {}
    }
    componentWillMount() {
        const {which} = this.props
        let who = ''
        if(which.length !== 0){
            if(typeof  which === 'string')
                who = 'who='+which
            else {
                which.map( item => {
                    who = who.concat('who=',item,'&')
                })
                who.slice(0,who.lastIndexOf('&'))
            }
        }
        this.props.getUsersList(cookies.get("token"), '')
    }

    componentWillReceiveProps(nextProps){
        const {usersList} = nextProps
        if(usersList && usersList && !this.state.fillData){
            this.setState({
                data        : usersList,
                _data       : usersList,
                fillData    : true
            })
        }
    }

    handleClick(e) {
        //this.props.getSelectedProvider(provider)
        console.log(e)
    }

    handleSearch = ({target}) => {
        const {value} = target
        this.setState({
            searchLength : value.length,
            search       : value,
            _data        : this.state.data.filter( item => {
                let itemName = item.firstname + ' ' + item.lastname
                itemName = itemName.toLowerCase()
                let userInput = value.toLowerCase()
                return (itemName.includes(userInput))
            })
        })
    }

    render = () => {
        const { classes } = this.props
        const {_data, search} = this.state
        return (
            <div className={classes.root}>
                <div className={classes.content}>
                    <List component="div">
                        {_data && _data.map( item => {
                            return(
                                <ListItem button onClick={() => this.handleClick(item)} key={item.id+'_'+item.firstname+'_'+item.lastname}>
                                    <ListItemIcon>
                                        <Person />
                                    </ListItemIcon>
                                    <ListItemText inset primary={item.firstname+' '+item.lastname} />
                                </ListItem>
                            )
                        } )}
                    </List>
                </div>
            </div>
        );
    }
}

usersList.propTypes = {
    classes: PropTypes.object.isRequired
};

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(usersList)


/*
 <AppBar position="static" color="default">
 <Tabs indicatorColor="primary" textColor="primary">
 <ListItem component="div">
 <ListItemIcon>
 <Search />
 </ListItemIcon>
 <FormControl>
 <InputLabel htmlFor="search">Search</InputLabel>
 <Input id="search" value={search} onChange={e => this.handleSearch(e)} />
 </FormControl>
 </ListItem>
 </Tabs>
 </AppBar>
* */