module.exports = {
    apps:
        [
            {
                name: 'gateway-auth',
                script: '/home/api-nobo-v2/api-nobo-v2/app.js',
                cwd: '/home/api-nobo-v2/api-nobo-v2',
                max_restarts: 2,
                env: {
                    "NODE_ENV": "production",
                    "GATEWAY_PORT": 3000,
                    "GOVERNESS_PORT": 3001,
                    "PROVIDER_PORT": 3002,
                    "GUEST_PORT": 3003,
                    "GOVERNESS_API_URL": "http://localhost:3001",
                    "PROVIDER_API_URL": "http://localhost:3002",
                    "GUEST_API_URL": "http://localhost:3003"
                }
            },
            {
                name: 'provider-api',
                script: '/home/api-nobo-v2/api-provider-app/app.js',
                cwd: '/home/api-nobo-v2/api-provider-app',
                max_restarts: 2,
                env: {
                    "NODE_ENV": "production",
                    "PROVIDER_PORT": 3002
                }
            },
            {
                name: 'guest-api',
                script: '/home/api-nobo-v2/api-guest/app.js',
                cwd: '/home/api-nobo-v2/api-guest',
                max_restarts: 2,
                env: {
                    "SERVICE_MAIL": "reception@nobo.life",
                    "CONTACT_MAIL": "contact@nobo.life",
                    "MAILJET_PUBLIC": "bc3b6f891f16af2bcf0d9c1f93a75d24",
                    "MAILJET_PRIVATE": "a717f3eaae394a2653f05c0c4b2f163f",
                    "NODE_ENV": "production",
                    "GUEST_PORT": 3003
                }
            },
            {
                name: 'governess-api',
                script: '/home/api-nobo-v2/api-governess-app/app.js',
                cwd: '/home/api-nobo-v2/api-governess-app',
                max_restarts: 2,
                env: {
                    "NODE_ENV": "production",
                    "GOVERNESS_PORT": 3001
                }
            }
        ]
};